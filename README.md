# TSU.InTime
## Установка и запуск
- Установить локально [XcodeGen](https://github.com/yonaskolb/XcodeGen), [R.Swift](https://github.com/mac-cain13/R.swift) и [SwiftLint](https://github.com/realm/SwiftLint).
- После получения новой версии из Git или измнения ветки, в терминале в корне проекта запустить следующую команду. Будет создан файл для генерируемых ресурсов и файл проекта.
    ```shell
    sh setup.command
    ```
- Открыть файл проекта и запустить как обычно.
