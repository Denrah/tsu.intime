//
//  TimetablePageViewModel.swift
//  TSUInTime
//

import CoreGraphics

private extension Constants {
  static let lastLessonBeforeLunchNumber = 3
}

protocol TimetablePageViewModelDelegate: AnyObject {
  func timetablePageViewModel(_ viewModel: TimetablePageViewModel, didSelect lesson: Lesson)
}

class TimetablePageViewModel: TableViewModel {
  typealias Dependencies = HasCrashReportService
  
  enum LessonsBreakType {
    case common, beforeLunch, afterLunch
  }
  
  // MARK: - Properties
  
  weak var delegate: TimetablePageViewModelDelegate?
  
  let emptyStateViewModel: EmptyStateViewModel

  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasLessons: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] = []
  
  private let timetableDay: TimetableDay
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(timetableDay: TimetableDay, dependencies: Dependencies) {
    self.timetableDay = timetableDay
    self.dependencies = dependencies
    self.emptyStateViewModel = EmptyStateViewModel(image: R.image.emptyDayImage(),
                                                   imageSize: CGSize(width: 256, height: 200),
                                                   title: R.string.timetable.emptyDayPlaceholderTitle(),
                                                   subtitle: R.string.timetable.emptyDayPlaceholderSubtitle())
    self.makeItemViewModels()
  }
  
  // MARK: - Private methods
  
  private func makeItemViewModels() {
    itemViewModels.removeAll()
    
    var sameTimeLessons: [Lesson] = []
    
    timetableDay.timeSlots.forEach { entity in
      switch entity {
      case .lesson(let lesson):
        if sameTimeLessons.isEmpty || sameTimeLessons.last?.startTime == lesson.startTime {
          sameTimeLessons.append(lesson)
        } else {
          appendCardContainerViewModel(lessons: sameTimeLessons)
          sameTimeLessons.removeAll()
        }
      case .lessonsBreak(let timetableBreak):
        appendCardContainerViewModel(lessons: sameTimeLessons)
        sameTimeLessons.removeAll()
        itemViewModels.append(TimetableSpaceViewModel(timetableBreak: timetableBreak))
      case .staticBreak(let timetableBreak):
        appendCardContainerViewModel(lessons: sameTimeLessons)
        sameTimeLessons.removeAll()
        itemViewModels.append(TimetableLunchViewModel(timetableBreak: timetableBreak))
      }
    }
    
    appendCardContainerViewModel(lessons: sameTimeLessons)
  }
  
  private func appendCardContainerViewModel(lessons: [Lesson]) {
    guard !lessons.isEmpty else { return }
    let cardsContainerViewModel = LessonCardsContainerViewModel(lessons: lessons)
    cardsContainerViewModel.delegate = self
    itemViewModels.append(cardsContainerViewModel)
  }
}

// MARK: - LessonCardsContainerViewModelDelegate

extension TimetablePageViewModel: LessonCardsContainerViewModelDelegate {
  func lessonCardsContainerViewModel(_ viewModel: LessonCardsContainerViewModel, didSelect lesson: Lesson) {
    delegate?.timetablePageViewModel(self, didSelect: lesson)
  }
}
