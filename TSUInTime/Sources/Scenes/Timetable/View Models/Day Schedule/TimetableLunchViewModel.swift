//
//  TimetableLunchViewModel.swift
//  TSUInTime
//

import Foundation

class TimetableLunchViewModel: TableCellViewModel {
  var tableReuseIdentifier: String {
    return DayScheduleLunchCell.reuseIdentifier
  }

  let title: String?
  let subtitle: String

  init(timetableBreak: TimetableBreak) {
    title = R.string.plurals.minutes(count: timetableBreak.breakInMinutes)
    subtitle = R.string.timetable.timePeriod(timetableBreak.startTime.formattedTime(),
                                             timetableBreak.endTime.formattedTime())
  }
}
