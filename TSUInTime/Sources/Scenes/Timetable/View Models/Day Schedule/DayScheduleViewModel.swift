//
//  DayScheduleViewModel.swift
//  TSUInTime
//

import Foundation

protocol DayScheduleViewModelDelegate: AnyObject {
  func dayScheduleViewModel(_ dayScheduleViewModel: DayScheduleViewModel, didScrollToPage page: Int)
  func dayScheduleViewModel(_ dayScheduleViewModel: DayScheduleViewModel,
                            didRequestToShowDetailsFor lesson: Lesson)
}

final class DayScheduleViewModel {
  typealias Dependencies = HasCrashReportService
  
  weak var delegate: DayScheduleViewModelDelegate?
  var timetablePageViewModels: [TimetablePageViewModel] = []
  var hasLessons: Bool {
    timetablePageViewModels.contains { $0.hasLessons }
  }
  
  private let dependencies: Dependencies
  
  init(timetableDays: [TimetableDay], dependencies: Dependencies) {
    self.dependencies = dependencies
    timetablePageViewModels = timetableDays
      .sorted { ($0.date ?? Date()) < ($1.date ?? Date()) }
      .map { timetableDay in
        let viewModel = TimetablePageViewModel(timetableDay: timetableDay, dependencies: dependencies)
        viewModel.delegate = self
        return viewModel
      }
  }
  
  func didScrollToPage(_ pageIndex: Int) {
    delegate?.dayScheduleViewModel(self, didScrollToPage: pageIndex)
  }
}

extension DayScheduleViewModel: TimetablePageViewModelDelegate {
  func timetablePageViewModel(_ viewModel: TimetablePageViewModel, didSelect lesson: Lesson) {
    delegate?.dayScheduleViewModel(self, didRequestToShowDetailsFor: lesson)
  }
}
