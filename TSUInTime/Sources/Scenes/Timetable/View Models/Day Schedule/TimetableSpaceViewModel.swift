//
//  TimetableSpaceViewModel.swift
//  TSUInTime
//

import CoreGraphics

enum TimetableSpaceType {
  case `break`, lessons
  
  var height: CGFloat {
    switch self {
    case .break:
      return 32
    case .lessons:
      return 80
    }
  }
}

class TimetableSpaceViewModel {
  let title: String?
  
  var height: CGFloat {
    return type.height
  }
  
  private let type: TimetableSpaceType
  
  init(timetableBreak: TimetableBreak) {
    if timetableBreak.lessonsCount == 0 {
      type = .break
      title = R.string.plurals.minutes(count: timetableBreak.breakInMinutes)
    } else {
      type = .lessons
      title = R.string.timetable.emptyLessonsBreakTitle(timetableBreak.startTime.formattedTime(),
                                                        timetableBreak.endTime.formattedTime(),
                                                        R.string.plurals.lessons(count: timetableBreak.lessonsCount))
    }
  }
}

// MARK: - TableCellViewModel

extension TimetableSpaceViewModel: TableCellViewModel {
  var tableReuseIdentifier: String {
    return DayScheduleSpaceCell.reuseIdentifier
  }
}
