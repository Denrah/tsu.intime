//
//  CalendarBarDateViewModel.swift
//  TSUInTime
//

import Foundation

protocol CalendarBarDateViewModelDelegate: AnyObject {
  func сalendarBarDateViewModel(_ viewModel: CalendarBarDateViewModel, didSelect date: Date)
}

class CalendarBarDateViewModel {
  // MARK: - Properties

  weak var delegate: CalendarBarDateViewModelDelegate?

  var onDidUpdate: (() -> Void)?

  var dayOfWeek: String {
    return DateFormatter.dayOfWeek.string(from: date)
  }
  
  var dayOfMonth: String {
    return DateFormatter.dayOfMonth.string(from: date)
  }
  
  let date: Date
  
  private(set) var isSelected: Bool

  // MARK: - Init
  
  init(date: Date, isSelected: Bool) {
    self.date = date
    self.isSelected = isSelected
  }

  // MARK: - Public methods

  func didTapView() {
    delegate?.сalendarBarDateViewModel(self, didSelect: date)
  }

  func setSelectedState(isSelected: Bool) {
    self.isSelected = isSelected
    onDidUpdate?()
  }
}
