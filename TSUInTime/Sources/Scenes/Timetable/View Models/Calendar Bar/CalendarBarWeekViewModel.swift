//
//  CalendarBarWeekViewModel.swift
//  TSUInTime
//

import Foundation

protocol CalendarBarWeekViewModelDelegate: AnyObject {
  func сalendarBarWeekViewModel(_ viewModel: CalendarBarWeekViewModel, didSelect date: Date)
}

class CalendarBarWeekViewModel {
  // MARK: - Properties
  weak var delegate: CalendarBarWeekViewModelDelegate?
  
  var onDidUpdate: (() -> Void)?
  
  var displayString: String? {
    guard let firstDate = startDateOfWeek,
          let secondDate = endDateOfWeek else {
      return nil
    }
    let firstDateString = DateFormatter.dayMonthDisplay.string(from: firstDate)
    let secondDateString = DateFormatter.dayMonthDisplay.string(from: secondDate)
    return R.string.timetable.timePeriod(firstDateString, secondDateString)
  }
  
  private(set) var startDateOfWeek: Date?
  
  private var endDateOfWeek: Date? {
    guard let startDateOfWeek = startDateOfWeek,
          let endDateOfWeek = calendar.date(byAdding: .day, value: Constants.daysInWeek - 1, to: startDateOfWeek) else {
      return nil
    }
    return endDateOfWeek
  }
  
  private let calendar = Date.calendarISO
  
  // MARK: - Init
  init(selectedDate: Date = Date()) {
    startDateOfWeek = selectedDate.startOfWeek()
    onDidUpdate?()
  }
  
  // MARK: - Public methods
  func switchToPreviousWeek() {
    guard let startDateOfWeek = startDateOfWeek,
          let newWeekStartDate = calendar.date(byAdding: .day, value: -Constants.daysInWeek, to: startDateOfWeek) else { return }
    self.startDateOfWeek = newWeekStartDate
    delegate?.сalendarBarWeekViewModel(self, didSelect: newWeekStartDate)
  }
  
  func switchToNextWeek() {
    guard let startDateOfWeek = startDateOfWeek,
          let newWeekStartDate = calendar.date(byAdding: .day, value: Constants.daysInWeek, to: startDateOfWeek) else { return }
    self.startDateOfWeek = newWeekStartDate
    delegate?.сalendarBarWeekViewModel(self, didSelect: newWeekStartDate)
  }
  
  func setStartOfWeek(with date: Date) {
    startDateOfWeek = date.startOfWeek()
    onDidUpdate?()
  }
  
}
