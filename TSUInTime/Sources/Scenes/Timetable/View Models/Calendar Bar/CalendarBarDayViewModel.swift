//
//  CalendarBarViewModel.swift
//  TSUInTime
//

import Foundation

protocol CalendarBarDayViewModelDelegate: AnyObject {
  func сalendarBarDayViewModel(_ viewModel: CalendarBarDayViewModel, didSelect date: Date, isSameWeek: Bool)
}

class CalendarBarDayViewModel {
  // MARK: - Properties
  weak var delegate: CalendarBarDayViewModelDelegate?
  
  var onDidUpdate: (() -> Void)?
  
  var endDateOfWeek: Date? {
    dateItemViewModels.last?.date
  }
  
  private(set) var dateItemViewModels: [CalendarBarDateViewModel] = []
  private(set) var startDateOfWeek: Date?
  private(set) var selectedDate: Date {
    didSet {
      updateSelectedDate()
    }
  }
  private let calendar = Date.calendarISO

  // MARK: - Init
  init(selectedDate: Date = Date()) {
    self.selectedDate = selectedDate
    startDateOfWeek = selectedDate.startOfWeek()
    updateWeek()
  }

  // MARK: - Public methods
  func switchToPreviousWeek() {
    guard let startDateOfWeek = startDateOfWeek,
          let newWeekStartDate = calendar.date(byAdding: .day, value: -Constants.daysInWeek, to: startDateOfWeek) else { return }
    self.startDateOfWeek = newWeekStartDate
    selectedDate = newWeekStartDate
    updateWeek()
    delegate?.сalendarBarDayViewModel(self, didSelect: selectedDate, isSameWeek: false)
  }

  func switchToNextWeek() {
    guard let startDateOfWeek = startDateOfWeek,
          let newWeekStartDate = calendar.date(byAdding: .day, value: Constants.daysInWeek, to: startDateOfWeek) else { return }
    self.startDateOfWeek = newWeekStartDate
    selectedDate = newWeekStartDate
    updateWeek()
    delegate?.сalendarBarDayViewModel(self, didSelect: selectedDate, isSameWeek: false)
  }
  
  func setSelectedDate(_ date: Date) {
    selectedDate = date
    startDateOfWeek = selectedDate.startOfWeek()
    updateWeek()
  }
  
  func selectDateAtIndex(_ index: Int) {
    guard let dateItemViewModel = dateItemViewModels.element(at: index) else { return }
    selectedDate = dateItemViewModel.date
  }
  
  // MARK: - Private methods
  private func updateSelectedDate() {
    dateItemViewModels.forEach { $0.setSelectedState(isSelected: false) }
    dateItemViewModels.first { calendar.isDate($0.date, inSameDayAs: selectedDate) }?.setSelectedState(isSelected: true)
  }

  private func updateWeek() {
    guard let startDateOfWeek = startDateOfWeek else { return }

    dateItemViewModels.removeAll()

    for offset in 0..<Constants.daysInWeek {
      let date = calendar.date(byAdding: .day, value: offset, to: startDateOfWeek) ?? Date()
      let dateItemViewModel = CalendarBarDateViewModel(date: date,
                                                       isSelected: calendar.isDate(date, inSameDayAs: selectedDate))
      dateItemViewModel.delegate = self
      dateItemViewModels.append(dateItemViewModel)
    }

    onDidUpdate?()
  }
}

// MARK: - CalendarBarDateViewModelDelegate

extension CalendarBarDayViewModel: CalendarBarDateViewModelDelegate {
  func сalendarBarDateViewModel(_ viewModel: CalendarBarDateViewModel, didSelect date: Date) {
    selectedDate = date
    delegate?.сalendarBarDayViewModel(self, didSelect: date, isSameWeek: true)
  }
}
