//
//  LessonCardViewModel.swift
//  TSUInTime
//

import UIKit

private extension Constants {
  static let groupsSeparator = ", "
}

protocol LessonCardViewModelDelegate: AnyObject {
  func lessonCardViewModel(_ viewModel: LessonCardViewModel, didSelect lesson: Lesson)
}

class LessonCardViewModel {
  enum MaskedCorners {
    case top, bottom, all, none
    
    var cornerMask: CACornerMask {
      switch self {
      case .top:
        return [.layerMinXMinYCorner, .layerMaxXMinYCorner]
      case .bottom:
        return [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
      case .all:
        return [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
      case .none:
        return []
      }
    }
  }
  
  // MARK: - Properties
  
  weak var delegate: LessonCardViewModelDelegate?
  
  var title: String {
    lesson.title
  }
  
  var type: LessonType {
    lesson.type
  }
  
  var professor: String {
    lesson.professor.name
  }
  
  var auditory: String {
    lesson.auditory.name
  }
  
  var group: String {
    lesson.groups.map(\.name).joined(separator: Constants.groupsSeparator)
  }
  
  var time: String {
    R.string.timetable.timePeriod(lesson.startTime.formattedTime(),
                                  lesson.endTime.formattedTime())
  }
  
  var lessonNumber: String {
    return R.string.timetable.lessonNumberTitle(lesson.lessonNumber)
  }
  
  var cornerMask: CACornerMask {
    maskedCorners.cornerMask
  }
  
  private let lesson: Lesson
  private let maskedCorners: MaskedCorners
  
  // MARK: - Init
  
  init(lesson: Lesson, maskedCorners: MaskedCorners) {
    self.lesson = lesson
    self.maskedCorners = maskedCorners
  }
  
  // MARK: - Public methods
  
  func didTapView() {
    delegate?.lessonCardViewModel(self, didSelect: lesson)
  }
}
