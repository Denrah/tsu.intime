//
//  LessonCardsContainerViewModel.swift
//  TSUInTime
//

import Foundation

protocol LessonCardsContainerViewModelDelegate: AnyObject {
  func lessonCardsContainerViewModel(_ viewModel: LessonCardsContainerViewModel,
                                     didSelect lesson: Lesson)
}

class LessonCardsContainerViewModel {
  weak var delegate: LessonCardsContainerViewModelDelegate?
  
  var cardViewModels: [LessonCardViewModel] {
    lessons.enumerated().map { index, lesson in
      let maskedCorners: LessonCardViewModel.MaskedCorners
      if lessons.count == 1 {
        maskedCorners = .all
      } else if index == 0 {
        maskedCorners = .top
      } else if index == lessons.count - 1 {
        maskedCorners = .bottom
      } else {
        maskedCorners = .none
      }
      
      let viewModel = LessonCardViewModel(lesson: lesson, maskedCorners: maskedCorners)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private let lessons: [Lesson]
  
  init(lessons: [Lesson]) {
    self.lessons = lessons
  }
}

// MARK: - TableCellViewModel

extension LessonCardsContainerViewModel: TableCellViewModel {
  var tableReuseIdentifier: String {
    LessonCardsContainerCell.reuseIdentifier
  }
}

// MARK: - LessonCardViewModelDelegate

extension LessonCardsContainerViewModel: LessonCardViewModelDelegate {
  func lessonCardViewModel(_ viewModel: LessonCardViewModel, didSelect lesson: Lesson) {
    delegate?.lessonCardsContainerViewModel(self, didSelect: lesson)
  }
}
