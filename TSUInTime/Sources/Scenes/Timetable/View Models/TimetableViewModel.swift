//
//  TimetableViewModel.swift
//  TSUInTime
//

import UIKit

protocol TimetableViewModelDelegate: AnyObject {
  func timetableViewModelDidRequestToShowFavorites(_ viewModel: TimetableViewModel)
  func timetableViewModelDidRequestToShowMenu(_ viewModel: TimetableViewModel)
  func timetableViewModelDidRequestToReturnToMainScreen(_ viewModel: TimetableViewModel)
  func timetableViewModel(_ viewModel: TimetableViewModel, didRequestToSetTitleTo title: String?, date: String?)
  func timetableViewModel(_ viewModel: TimetableViewModel, didRequestToShowDetailsFor lesson: Lesson)
}

class TimetableViewModel: DataLoadingViewModel {
  typealias Dependencies = HasDataStore & HasScheduleNetworkService
  & HasCrashReportService & HasFavoritesStorageService
  
  // MARK: - Properties
  
  weak var delegate: TimetableViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  var onDidRequestToSelectDate: ((_ selectedDate: Date, _ selectedWeek: Int) -> Void)?
  var onDidRequestToJumpToPage: ((_ pageNumber: Int) -> Void)?
  var onDidAddToFavorites: (() -> Void)?
  var onDidRemoveFromFavorites: (() -> Void)?
  var onDidRequestToUpdateBottomBarItem: ((_ fromType: TimetableBottomBarItemType,
                                           _ toType: TimetableBottomBarItemType) -> Void)?
  var onDidSwitchTimetableDisplayType: ((_ timetableDisplayType: TimetableDisplayType) -> Void)?
  
  var hasTimetable: Bool {
    switch timetableDisplayType {
    case .day:
      return timetableDayViewModel?.hasLessons ?? false
    case .week:
      return timetableWeekViewModel?.hasLessons ?? false
    }
  }
  
  var bottomBarConfiguration: TimetableBottomBarConfiguration {
    timetableInfo.bottomBarConfiguration
  }
  
  var startDate: Date? {
    switch timetableDisplayType {
    case .week:
      return calendarBarWeekViewModel.startDateOfWeek
    case .day:
      return calendarBarDayViewModel.startDateOfWeek
    }
  }
  
  private var selectedDate: Date {
    didSet {
      calendarBarDayViewModel.setSelectedDate(selectedDate)
      calendarBarWeekViewModel.setStartOfWeek(with: selectedDate)
    }
  }
  
  private(set) var calendarBarDayViewModel = CalendarBarDayViewModel()
  private(set) var calendarBarWeekViewModel = CalendarBarWeekViewModel()
  private(set) var timetableDayViewModel: DayScheduleViewModel?
  private(set) var timetableWeekViewModel: WeekScheduleViewModel?
  private(set) var emptyStateViewModel: EmptyStateViewModel?
  
  private(set) var timetableDisplayType: TimetableDisplayType {
    get {
      guard let type = dependencies.dataStore.preferredDisplayType else {
        dependencies.dataStore.preferredDisplayType = .day
        return .day
      }
      return type
    }
    set {
      dependencies.dataStore.preferredDisplayType = newValue
    }
  }
  
  private var timetableInfo: TimetableInfo
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(timetableInfo: TimetableInfo, dependencies: Dependencies) {
    self.timetableInfo = timetableInfo
    self.dependencies = dependencies
    selectedDate = calendarBarDayViewModel.selectedDate
    calendarBarDayViewModel.delegate = self
    calendarBarWeekViewModel.delegate = self
  }
  
  // MARK: - Public methods
  
  func update(with timetableInfo: TimetableInfo) {
    self.timetableInfo = timetableInfo
    updateData()
  }
  
  func update() {
    updateData()
  }
  
  func viewIsReady() {
    updateData()
  }
  
  func didSelectBottomBarItem(itemType: TimetableBottomBarItemType) {
    switch itemType {
    case .favorites:
      delegate?.timetableViewModelDidRequestToShowFavorites(self)
    case .date:
      onDidRequestToSelectDate?(selectedDate,
                                AcademicPeriodsHelper.numberOfAcademicWeek(for: selectedDate))
    case .today:
      selectedDate = Date()
      loadData()
    case .menu:
      delegate?.timetableViewModelDidRequestToShowMenu(self)
    case .home:
      delegate?.timetableViewModelDidRequestToReturnToMainScreen(self)
    case .addToFavorites, .deleteFromFavorites:
      toggleFavorites()
    case .day, .week:
      switchPreferredTimetableDisplayType()
    }
  }
  
  func didSelectDate(_ date: Date) {
    selectedDate = date
    loadData()
  }
  
  func didSelectWeek(_ week: Int) {
    if let date = AcademicPeriodsHelper.startOfAcademicWeek(week: week) {
      selectedDate = date
      loadData()
    }
  }
  
  // MARK: - Private methods
  
  private func updateData() {
    if dependencies.favoritesStorageService.isTimetableInFavorites(id: timetableInfo.timetableID) {
      onDidRequestToUpdateBottomBarItem?(.addToFavorites, .deleteFromFavorites)
    } else {
      onDidRequestToUpdateBottomBarItem?(.deleteFromFavorites, .addToFavorites)
    }
    
    switch timetableDisplayType {
    case .day:
      onDidRequestToUpdateBottomBarItem?(.day, .week)
    case .week:
      onDidRequestToUpdateBottomBarItem?(.week, .day)
    }
    loadData()
  }
  
  private func loadData() {
    guard let startDate = startDate,
          let endDate = Calendar.current.date(byAdding: .day, value: 6, to: startDate) else { return }
    updateTitle(for: selectedDate)
    onDidStartRequest?()
    switch timetableDisplayType {
    case .day:
      loadDayDisplayTimetable(startDate: startDate, endDate: endDate)
    case .week:
      loadWeekDisplayTimetable(startDate: startDate, endDate: endDate)
    }
    
  }
  
  private func loadDayDisplayTimetable(startDate: Date, endDate: Date) {
    timetableWeekViewModel = nil
    dependencies.scheduleNetworkService.getDaysTimetable(type: timetableInfo.timetableType,
                                                         id: timetableInfo.timetableID,
                                                         startDate: startDate, endDate: endDate).done { response in
      self.handleDayDisplayTimetable(response)
    }.catch { error in
      self.onDidFinishRequest?()
      self.onDidReceiveError?(error)
    }
  }
  
  private func loadWeekDisplayTimetable(startDate: Date, endDate: Date) {
    timetableDayViewModel = nil
    dependencies.scheduleNetworkService.getWeekTimetable(type: timetableInfo.timetableType,
                                                         id: timetableInfo.timetableID,
                                                         startDate: startDate, endDate: endDate).done { response in
      self.handleWeekDisplayTimetable(response)
    }.catch { error in
      self.onDidFinishRequest?()
      self.onDidReceiveError?(error)
    }
  }
  
  private func handleDayDisplayTimetable(_ response: [TimetableDay]) {
    let timetableDayViewModel = DayScheduleViewModel(timetableDays: response,
                                                     dependencies: dependencies)
    self.timetableDayViewModel = timetableDayViewModel
    timetableDayViewModel.delegate = self
    if timetableDayViewModel.timetablePageViewModels.allSatisfy({ !$0.hasLessons }) {
      generateEmptyStateView()
    }
    
    onDidLoadData?()
    onDidRequestToJumpToPage?(calendarBarDayViewModel.selectedDate.dayOfWeek())
    onDidFinishRequest?()
  }
  
  private func handleWeekDisplayTimetable(_ response: TimetableWeek) {
    let timetableWeekViewModel = WeekScheduleViewModel(timetableGridElements: response.grid,
                                                       timetableSlots: response.timeSlots)
    self.timetableWeekViewModel = timetableWeekViewModel
    timetableWeekViewModel.delegate = self
    
    if !hasTimetable {
      generateEmptyStateView()
    }
    
    onDidLoadData?()
    onDidFinishRequest?()
  }
  
  private func generateEmptyStateView() {
    let image: UIImage?
    let imageSize: CGSize?
    let title: String?
    let subtitle: String?
    let currentDate = Date()
    
    if (calendarBarDayViewModel.endDateOfWeek ?? currentDate) < currentDate {
      image = R.image.emptyDayImage()
      imageSize = CGSize(width: 256, height: 200)
      title = R.string.timetable.emptyPastWeekPlaceholderTitle()
      subtitle = R.string.timetable.emptyPastWeekPlaceholderSubtitle()
    } else {
      image = R.image.emptyWeekImage()
      imageSize = CGSize(width: 256, height: 240)
      title = R.string.timetable.emptyFutureWeekPlaceholderTitle()
      subtitle = R.string.timetable.emptyFutureWeekPlaceholderSubtitle()
    }
    
    emptyStateViewModel = EmptyStateViewModel(image: image, imageSize: imageSize,
                                              title: title, subtitle: subtitle)
  }
  
  private func updateTitle(for date: Date) {
    let dateString = DateFormatter.fullMonthYear.string(from: date).capitalized
    let numberOfWeek = AcademicPeriodsHelper.numberOfAcademicWeek(for: date)
    delegate?.timetableViewModel(self, didRequestToSetTitleTo: timetableInfo.timetableTitle,
                                 date: R.string.timetable.screenDateTitle(dateString, numberOfWeek))
  }
  
  private func toggleFavorites() {
    let isInFavorites = dependencies.favoritesStorageService.isTimetableInFavorites(id: timetableInfo.timetableID)
    
    do {
      if isInFavorites {
        try dependencies.favoritesStorageService.deleteTimetable(id: timetableInfo.timetableID)
        onDidRequestToUpdateBottomBarItem?(.deleteFromFavorites, .addToFavorites)
        onDidRemoveFromFavorites?()
      } else {
        try dependencies.favoritesStorageService.saveTimetable(timetableInfo: timetableInfo)
        onDidRequestToUpdateBottomBarItem?(.addToFavorites, .deleteFromFavorites)
        onDidAddToFavorites?()
      }
    } catch let error {
      onDidReceiveError?(error)
    }
  }
  
  private func switchPreferredTimetableDisplayType() {
    switch timetableDisplayType {
    case .day:
      timetableDisplayType = .week
    case .week:
      timetableDisplayType = .day
    }
    onDidSwitchTimetableDisplayType?(timetableDisplayType)
  }
}

// MARK: - CalendarBarDayViewModelDelegate

extension TimetableViewModel: CalendarBarDayViewModelDelegate {
  func сalendarBarDayViewModel(_ viewModel: CalendarBarDayViewModel, didSelect date: Date, isSameWeek: Bool) {
    if isSameWeek {
      onDidRequestToJumpToPage?(date.dayOfWeek())
    } else {
      selectedDate = date
      loadData()
    }
  }
}

// MARK: - CalendarBarWeekViewModelDelegate

extension TimetableViewModel: CalendarBarWeekViewModelDelegate {
  func сalendarBarWeekViewModel(_ viewModel: CalendarBarWeekViewModel, didSelect date: Date) {
    selectedDate = date
    loadData()
  }
}

// MARK: - DayScheduleViewModelDelegate

extension TimetableViewModel: DayScheduleViewModelDelegate {
  func dayScheduleViewModel(_ dayScheduleViewModel: DayScheduleViewModel, didRequestToShowDetailsFor lesson: Lesson) {
    delegate?.timetableViewModel(self, didRequestToShowDetailsFor: lesson)
  }
  
  func dayScheduleViewModel(_ dayScheduleViewModel: DayScheduleViewModel, didScrollToPage page: Int) {
    calendarBarDayViewModel.selectDateAtIndex(page)
    updateTitle(for: calendarBarDayViewModel.selectedDate)
  }
}

// MARK: - WeekScheduleViewModelDelegate

extension TimetableViewModel: WeekScheduleViewModelDelegate {
  func timetableWeekViewModel(_ viewModel: WeekScheduleViewModel, didSelect lesson: Lesson) {
    delegate?.timetableViewModel(self, didRequestToShowDetailsFor: lesson)
  }
}
