//
//  WeekScheduleItemHeaderViewModel.swift
//  TSUInTime
//

import Foundation

final class WeekScheduleLessonTimeViewModel {
  let startTime: String
  let endTime: String
  
  init(startTime: String, endTime: String) {
    self.startTime = startTime
    self.endTime = endTime
  }
}
