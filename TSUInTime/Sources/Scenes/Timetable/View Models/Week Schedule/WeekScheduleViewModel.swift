//
//  TimetableWeekViewModel.swift
//  TSUInTime
//

import Foundation

protocol WeekScheduleViewModelDelegate: AnyObject {
  func timetableWeekViewModel(_ viewModel: WeekScheduleViewModel, didSelect lesson: Lesson)
}

final class WeekScheduleViewModel {
  // MARK: - Properties
  weak var delegate: WeekScheduleViewModelDelegate?
  
  var hasLessons: Bool {
    let elements = matrix.flatMap { $0 }
    for element in elements {
      switch element {
      case .lessons(lessonViewModels: let lessonViewModels):
        if !lessonViewModels.isEmpty {
          return true
        }
      default:
        continue
      }
    }
    return false
  }
  
  private(set) var matrix: [[WeekScheduleCellType]] = [[]]
  
  // MARK: - Init
  init(timetableGridElements: [TimetableGridElement], timetableSlots: [TimetableSlot]) {
    generateData(timetableGridElements: timetableGridElements, timetableSlots: timetableSlots)
  }
  
  // MARK: - Public Methods
  func selectLesson(_ lesson: Lesson) {
    delegate?.timetableWeekViewModel(self, didSelect: lesson)
  }
  
  // MARK: - Private Methods
  private func generateData(timetableGridElements: [TimetableGridElement], timetableSlots: [TimetableSlot]) {
    matrix = [[WeekScheduleCellType]](repeating: [WeekScheduleCellType](repeating: .empty,
                                                                        count: timetableGridElements.count + 1),
                                      count: timetableSlots.count + 1)
    setupLessonTimes(timetableSlots: timetableSlots)
    let sortedGridElements = timetableGridElements.sorted { $0.date ?? Date() < $1.date ?? Date() }
    sortedGridElements.enumerated().forEach { index, element in
      guard let date = element.date else { return }
      setupWeekday(date: date, index: index)
      setupLessons(lessons: element.lessons, index: index)
    }
  }
  
  private func setupLessonTimes(timetableSlots: [TimetableSlot]) {
    timetableSlots.enumerated().forEach { index, element in
      let startTime = DateFormatter.shortHoursMinutes.string(from: Date(timeIntervalSince1970: element.startTime))
      let endTime = DateFormatter.shortHoursMinutes.string(from: Date(timeIntervalSince1970: element.endTime))
      let weekdayViewModel = WeekScheduleLessonTimeViewModel(startTime: startTime, endTime: endTime)
      guard areIndexesInBounds(firstIndex: index + 1, secondIndex: 0) else { return }
      matrix[index + 1][0] = .lessonTime(lessonTimeViewModel: weekdayViewModel)
    }
  }
  
  private func setupWeekday(date: Date, index: Int) {
    let dateViewModel = WeekScheduleWeekdayViewModel(weekday: DateFormatter.dayOfWeek.string(from: date),
                                                     dateString: DateFormatter.dayMonthDisplay.string(from: date))
    guard areIndexesInBounds(firstIndex: 0, secondIndex: index + 1) else { return }
    matrix[0][index + 1] = .weekday(weekdayViewModel: dateViewModel)
  }
  
  private func setupLessons(lessons: [Lesson], index: Int) {
    var dictionary: [Int: [Lesson]] = [:]
    for lesson in lessons {
      if dictionary[lesson.lessonNumber] == nil {
        dictionary[lesson.lessonNumber] = []
      }
      dictionary[lesson.lessonNumber]?.append(lesson)
    }
    
    for (key, value) in dictionary {
      let lessonViewModels: [WeekScheduleLessonViewModel] = value.map { lesson in
        let viewModel = WeekScheduleLessonViewModel(lesson: lesson)
        viewModel.delegate = self
        return viewModel
      }
      guard areIndexesInBounds(firstIndex: key, secondIndex: index + 1) else { continue }
      matrix[key][index + 1] = .lessons(lessonViewModels: lessonViewModels)
    }
  }
  
  private func areIndexesInBounds(firstIndex: Int, secondIndex: Int) -> Bool {
    guard firstIndex < matrix.count,
          secondIndex < matrix[firstIndex].count else { return false }
    return true
  }
  
}

// MARK: - WeekScheduleLessonViewModelDelegate
extension WeekScheduleViewModel: WeekScheduleLessonViewModelDelegate {
  func weekScheduleLessonViewModel(_ viewModel: WeekScheduleLessonViewModel,
                                   didSelect lesson: Lesson) {
    selectLesson(lesson)
  }
}
