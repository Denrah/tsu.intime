//
//  WeekScheduleWeekdayItemViewModel.swift
//  TSUInTime
//

import Foundation

final class WeekScheduleWeekdayViewModel {
  let weekday: String
  let dateString: String
  
  init(weekday: String, dateString: String) {
    self.weekday = weekday.uppercased()
    self.dateString = dateString
  }
  
}
