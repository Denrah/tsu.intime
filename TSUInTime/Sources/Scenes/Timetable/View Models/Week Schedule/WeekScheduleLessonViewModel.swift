//
//  WeekScheduleLessonViewModel.swift
//  TSUInTime
//

import UIKit

protocol WeekScheduleLessonViewModelDelegate: AnyObject {
  func weekScheduleLessonViewModel(_ viewModel: WeekScheduleLessonViewModel,
                                   didSelect lesson: Lesson)
}

final class WeekScheduleLessonViewModel {
  weak var delegate: WeekScheduleLessonViewModelDelegate?
  let lessonTitle: String
  let lessonGroups: String
  let lessonColor: UIColor
  private let lesson: Lesson
  
  init(lesson: Lesson) {
    self.lesson = lesson
    lessonTitle = lesson.title
    lessonGroups = lesson.groups.map { $0.name }.joined(separator: ", ")
    lessonColor = lesson.type.color
  }
  
  func select() {
    delegate?.weekScheduleLessonViewModel(self, didSelect: lesson)
  }
  
}
