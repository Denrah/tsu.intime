//
//  TimetableInfo.swift
//  TSUInTime
//

import Foundation

struct TimetableInfo: Codable {
  let timetableName: String?
  let timetableType: TimetableType
  let timetableID: String
  let bottomBarConfiguration: TimetableBottomBarConfiguration

  var timetableTitle: String? {
    timetableType.title(for: timetableName)
  }
  
  init(timetableName: String?, timetableType: TimetableType, timetableID: String,
       bottomBarConfiguration: TimetableBottomBarConfiguration) {
    self.timetableName = timetableName
    self.timetableType = timetableType
    self.timetableID = timetableID
    self.bottomBarConfiguration = bottomBarConfiguration
  }
  
  init(userInfo: UserInfo) {
    self.init(timetableName: userInfo.timetableName, timetableType: userInfo.timetableType,
              timetableID: userInfo.timetableID, bottomBarConfiguration: .mainTimetable)
  }
}
