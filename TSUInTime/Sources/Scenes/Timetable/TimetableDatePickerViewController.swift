//
//  TimetableDatePickerViewController.swift
//  TSUInTime
//

import UIKit
import SnapKit

private extension Constants {
  static let numberOfWeeks = 45
}

class TimetableDatePickerViewController: UIViewController {
  // MARK: - Types
  
  enum PickerType: Int, CaseIterable {
    case date, week
    
    var title: String {
      switch self {
      case .date:
        return R.string.timetable.datePickerDateOption()
      case .week:
        return R.string.timetable.datePickerWeekOption()
      }
    }
  }
  
  // MARK: - Properties
  
  var onDidSelectDate: ((_ date: Date) -> Void)?
  var onDidSelectWeek: ((_ week: Int) -> Void)?
  
  var defaultDate = Date() {
    didSet {
      datePicker.setDate(defaultDate, animated: false)
    }
  }
  
  var defaultWeek = 1 {
    didSet {
      guard defaultWeek > 1, defaultWeek <= Constants.numberOfWeeks else { return }
      weekPickerView.selectRow(defaultWeek - 1, inComponent: 0, animated: false)
      selectedWeek = defaultWeek
    }
  }
  
  private let overlayView = UIView()
  private let containerView = UIView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let cancelButton = UIButton(type: .system)
  private let segmentedControl = UISegmentedControl(items: PickerType.allCases.map(\.title))
  private let datePicker = UIDatePicker()
  private let weekPickerView = UIPickerView()
  private let selectButton = CommonButton(style: .default)
  private let bottomSpaceView = UIView()
  
  private var selectedWeek = 1
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    datePicker.setDate(defaultDate, animated: false)
    if defaultWeek > 1, defaultWeek <= Constants.numberOfWeeks {
      weekPickerView.selectRow(defaultWeek - 1, inComponent: 0, animated: false)
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    animateAppearance()
  }
  
  // MARK: - Actions
  
  @objc private func closePicker() {
    animateDisappearance()
  }
  
  @objc private func selectDate() {
    let pickerType = PickerType(rawValue: segmentedControl.selectedSegmentIndex) ?? .date
    
    switch pickerType {
    case .date:
      onDidSelectDate?(datePicker.date)
    case .week:
      onDidSelectWeek?(selectedWeek)
    }
    
    closePicker()
  }
  
  @objc private func didChangeSegmentedControlValue() {
    let pickerType = PickerType(rawValue: segmentedControl.selectedSegmentIndex) ?? .date
    
    switch pickerType {
    case .date:
      datePicker.isHidden = false
      weekPickerView.isHidden = true
    case .week:
      datePicker.isHidden = true
      weekPickerView.isHidden = false
    }
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupOverlayView()
    setupContainerView()
    setupTitleLabel()
    setupCancelButton()
    setupSegmentedControl()
    setupDatePicker()
    setupWeekPickerView()
    setupSelectButton()
    setupBottomSpaceView()
  }
  
  private func setupOverlayView() {
    view.addSubview(overlayView)
    overlayView.backgroundColor = .zeroBlack
    overlayView.alpha = 0
    overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closePicker)))
    overlayView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupContainerView() {
    view.addSubview(containerView)
    containerView.backgroundColor = .baseWhite
    containerView.layer.cornerRadius = 16
    containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    makeClosedStateConstraints()
  }
  
  private func setupTitleLabel() {
    containerView.addSubview(titleLabel)
    titleLabel.text = R.string.timetable.datePickerTitle()
    titleLabel.textColor = .baseBlack
    titleLabel.snp.makeConstraints { make in
      make.top.leading.equalToSuperview().inset(16)
    }
  }
  
  private func setupCancelButton() {
    containerView.addSubview(cancelButton)
    cancelButton.setTitle(R.string.common.cancel(), for: .normal)
    cancelButton.setTitleColor(.accent, for: .normal)
    cancelButton.addTarget(self, action: #selector(closePicker), for: .touchUpInside)
    cancelButton.snp.makeConstraints { make in
      make.trailing.equalToSuperview().inset(16)
      make.centerY.equalTo(titleLabel)
    }
  }
  
  private func setupSegmentedControl() {
    containerView.addSubview(segmentedControl)
    segmentedControl.selectedSegmentIndex = 0
    segmentedControl.setTitleTextAttributes([.font: UIFont.body ?? .systemFont(ofSize: 16)], for: .normal)
    segmentedControl.setTitleTextAttributes([.font: UIFont.body ?? .systemFont(ofSize: 16)], for: .selected)
    segmentedControl.addTarget(self, action: #selector(didChangeSegmentedControlValue), for: .valueChanged)
    segmentedControl.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupDatePicker() {
    containerView.addSubview(datePicker)
    if #available(iOS 13.4, *) {
      datePicker.preferredDatePickerStyle = .wheels
    }
    datePicker.datePickerMode = .date
    datePicker.snp.makeConstraints { make in
      make.top.equalTo(segmentedControl.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupWeekPickerView() {
    containerView.addSubview(weekPickerView)
    weekPickerView.dataSource = self
    weekPickerView.delegate = self
    weekPickerView.isHidden = true
    weekPickerView.snp.makeConstraints { make in
      make.top.leading.trailing.equalTo(datePicker)
    }
  }
  
  private func setupSelectButton() {
    containerView.addSubview(selectButton)
    selectButton.setTitle(R.string.timetable.datePickerSelectButtonTitle(), for: .normal)
    selectButton.addTarget(self, action: #selector(selectDate), for: .touchUpInside)
    selectButton.snp.makeConstraints { make in
      make.top.equalTo(datePicker.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.equalToSuperview().inset(16).priority(750)
      make.bottom.greaterThanOrEqualTo(view.snp.bottom).offset(-40)
    }
  }
  
  private func setupBottomSpaceView() {
    view.addSubview(bottomSpaceView)
    bottomSpaceView.backgroundColor = .baseWhite
    bottomSpaceView.snp.makeConstraints { make in
      make.top.equalTo(containerView.snp.bottom)
      make.leading.trailing.equalToSuperview()
      make.height.equalTo(view.snp.height)
    }
  }
  
  // MARK: - Private methods
  
  private func animateAppearance() {
    makeOpenedStateConstraints()
    
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
      self.overlayView.alpha = 0.5
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  private func animateDisappearance() {
    makeClosedStateConstraints()
    
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
      self.overlayView.alpha = 0
      self.view.layoutIfNeeded()
    } completion: { _ in
      self.dismiss(animated: false)
    }
  }
  
  private func makeOpenedStateConstraints() {
    containerView.snp.remakeConstraints { make in
      make.leading.trailing.equalToSuperview()
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
    }
  }
  
  private func makeClosedStateConstraints() {
    containerView.snp.remakeConstraints { make in
      make.leading.trailing.equalToSuperview()
      make.top.equalTo(view.snp.bottom)
    }
  }
}

// MARK: - UIPickerViewDataSource, UIPickerViewDelegate

extension TimetableDatePickerViewController: UIPickerViewDataSource, UIPickerViewDelegate {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return Constants.numberOfWeeks
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return R.string.timetable.datePickerWeekPickerOptionTitle(row + 1)
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    selectedWeek = row + 1
  }
}
