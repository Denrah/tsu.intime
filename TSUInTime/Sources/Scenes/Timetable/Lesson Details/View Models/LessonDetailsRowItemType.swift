//
//  LessonDetailsRowItemType.swift
//  TSUInTime
//

import UIKit

private extension Constants {
  static let separator = ", "
}

enum LessonDetailsRowItemType {
  case time(lesson: Lesson), professor(professor: Professor),
       auditory(auditory: Auditory), groups(groups: [Group])
  
  var icon: UIImage? {
    switch self {
    case .time:
      return R.image.timeIcon()
    case .professor:
      return R.image.professorIcon()
    case .auditory:
      return R.image.auditoryIcon()
    case .groups:
      return R.image.groupIcon()
    }
  }
  
  var title: String? {
    switch self {
    case .time(let lesson):
      return R.string.timetable.lessonDetailsLessonTime(lesson.startTime.formattedTime(),
                                                        lesson.endTime.formattedTime(),
                                                        lesson.lessonNumber)
    case .professor(let professor):
      return professor.name
    case .auditory(let auditory):
      return [auditory.name, auditory.building?.address].compactMap { $0 }.filter { !$0.isEmpty }
      .joined(separator: Constants.separator)
    case .groups(let groups):
      return groups.map(\.name).joined(separator: Constants.separator)
    }
  }
}
