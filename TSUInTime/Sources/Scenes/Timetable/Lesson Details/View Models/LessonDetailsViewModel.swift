//
//  LessonDetailsViewModel.swift
//  TSUInTime
//

import UIKit

protocol LessonDetailsViewModelDelegate: AnyObject {
  func lessonDetailsViewModel(_ viewModel: LessonDetailsViewModel, didRequestToShowTimetableFor timetableInfo: TimetableInfo)
}

class LessonDetailsViewModel {
  // MARK: - Properties

  weak var delegate: LessonDetailsViewModelDelegate?
  
  var title: String {
    lesson.title
  }
  
  var type: LessonType {
    lesson.type
  }
  
  private(set) var rowItemViewModels: [LessonDetailsRowItemViewModel] = []
  
  private let lesson: Lesson
  
  private lazy var rowTypes: [LessonDetailsRowItemType] = [.time(lesson: lesson),
                                                           .professor(professor: lesson.professor),
                                                           .auditory(auditory: lesson.auditory),
                                                           .groups(groups: lesson.groups)]
  
  // MARK: - Init
  
  init(lesson: Lesson) {
    self.lesson = lesson
    makeRowItemViewModels()
  }
  
  // MARK: - Private methods
  
  private func makeRowItemViewModels() {
    rowItemViewModels = rowTypes.enumerated().map { index, type in
      return makeRowItemViewModel(for: type, index: index)
    }
  }
  
  private func makeRowItemViewModel(for type: LessonDetailsRowItemType,
                                    index: Int) -> LessonDetailsRowItemViewModel {
    let shouldHandleTap: Bool
    
    switch type {
    case .time, .groups:
      shouldHandleTap = false
    case .professor(let professor):
      shouldHandleTap = !professor.id.isEmpty
    case .auditory(let auditory):
      shouldHandleTap = !auditory.id.isEmpty
    }
    
    let viewModel = LessonDetailsRowItemViewModel(type: type, shouldHandleTap: shouldHandleTap,
                                                  shouldHideDashedLine: index == (rowTypes.count - 1))
    viewModel.delegate = self
    return viewModel
  }
}

// MARK: - LessonDetailsRowItemViewModelDelegate

extension LessonDetailsViewModel: LessonDetailsRowItemViewModelDelegate {
  func lessonDetailsRowItemViewModel(_ viewModel: LessonDetailsRowItemViewModel,
                                     didSelect type: LessonDetailsRowItemType) {
    switch type {
    case .professor(let professor):
      let timetableInfo = TimetableInfo(timetableName: professor.name, timetableType: .professor,
                                        timetableID: professor.id,
                                        bottomBarConfiguration: .additionalTimetable)
      delegate?.lessonDetailsViewModel(self, didRequestToShowTimetableFor: timetableInfo)
    case .auditory(let auditory):
      guard !auditory.id.isEmpty else { return }
      let timetableInfo = TimetableInfo(timetableName: auditory.name, timetableType: .auditory,
                                        timetableID: auditory.id,
                                        bottomBarConfiguration: .additionalTimetable)
      delegate?.lessonDetailsViewModel(self, didRequestToShowTimetableFor: timetableInfo)
    default:
      return
    }
  }
}
