//
//  LessonDetailsRowItemViewModel.swift
//  TSUInTime
//

import UIKit

enum LessonDetailsRowAdditionalViewType {
  case map(viewModel: LessonDetailsMapViewModel)
}

protocol LessonDetailsRowItemViewModelDelegate: AnyObject {
  func lessonDetailsRowItemViewModel(_ viewModel: LessonDetailsRowItemViewModel,
                                     didSelect type: LessonDetailsRowItemType)
}

class LessonDetailsRowItemViewModel {
  // MARK: - Properties
  
  weak var delegate: LessonDetailsRowItemViewModelDelegate?
  
  var hidesDisclosureIndicator: Bool {
    !shouldHandleTap
  }
  
  var icon: UIImage? {
    type.icon
  }
  
  var title: String? {
    type.title
  }
  
  let shouldHideDashedLine: Bool
  let additionalViewType: LessonDetailsRowAdditionalViewType?
  
  private let type: LessonDetailsRowItemType
  private let shouldHandleTap: Bool
  
  // MARK: - Init
  
  init(type: LessonDetailsRowItemType, shouldHandleTap: Bool, shouldHideDashedLine: Bool) {
    self.type = type
    self.shouldHandleTap = shouldHandleTap
    self.shouldHideDashedLine = shouldHideDashedLine
    
    switch type {
    case .auditory(let auditory):
      guard !(auditory.building?.address.isEmptyOrNil ?? true) else {
        additionalViewType = nil
        break 
      }
      additionalViewType = .map(viewModel: LessonDetailsMapViewModel(auditory: auditory))
    default:
      additionalViewType = nil
    }
  }
  
  // MARK: - Public methods
  
  func didTapView() {
    delegate?.lessonDetailsRowItemViewModel(self, didSelect: type)
  }
}
