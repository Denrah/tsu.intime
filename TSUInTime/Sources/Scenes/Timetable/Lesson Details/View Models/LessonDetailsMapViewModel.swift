//
//  LessonDetailsMapViewModel.swift
//  TSUInTime
//

import Foundation
import MapKit

private extension Constants {
  static let locationDistance: CLLocationDistance = 300
}

class LessonDetailsMapViewModel {
  let coordinate: CLLocationCoordinate2D
  let region: MKCoordinateRegion
  let markerViewModel = MapMarkerViewModel(type: .universityBuilding, size: .small)
  
  private let auditory: Auditory
  
  init(auditory: Auditory) {
    let center = auditory.building?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
    self.auditory = auditory
    self.coordinate = center
    self.region = MKCoordinateRegion(center: center,
                                     latitudinalMeters: Constants.locationDistance,
                                     longitudinalMeters: Constants.locationDistance)
  }
  
  func didTapView() {
    let options = [
      MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: coordinate),
      MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)
    ]
    let placemark = MKPlacemark(coordinate: coordinate)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = auditory.building?.address
    mapItem.openInMaps(launchOptions: options)
  }
}
