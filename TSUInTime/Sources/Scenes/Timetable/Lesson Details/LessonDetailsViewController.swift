//
//  LessonDetailsViewController.swift
//  TSUInTime
//

import UIKit

class LessonDetailsViewController: BaseViewController {
  // MARK: - Properties
  
  private let scrollView = UIScrollView()
  private let stackView = UIStackView()
  private let titleLabel = Label(textStyle: .header2)
  private let typeBadgeView = LessonTypeBadgeView()
  
  private let viewModel: LessonDetailsViewModel
  
  // MARK: - Init
  
  init(viewModel: LessonDetailsViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Overrides
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupScrollView()
    setupStackView()
    setupTitleLabel()
    setupTypeBadgeView()
    setupRowItemViews()
  }
  
  private func setupScrollView() {
    view.addSubview(scrollView)
    scrollView.showsVerticalScrollIndicator = false
    scrollView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    scrollView.addSubview(stackView)
    stackView.axis = .vertical
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(16)
      make.width.equalTo(view.snp.width).offset(-32)
    }
  }
  
  private func setupTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.text = viewModel.title
    titleLabel.numberOfLines = 0
    stackView.setCustomSpacing(8, after: titleLabel)
  }
  
  private func setupTypeBadgeView() {
    let containerView = UIView()
    stackView.addArrangedSubview(containerView)
    
    containerView.addSubview(typeBadgeView)
    typeBadgeView.configure(with: viewModel.type)
    typeBadgeView.snp.makeConstraints { make in
      make.top.leading.bottom.equalToSuperview()
      make.trailing.lessThanOrEqualToSuperview()
    }
    
    stackView.setCustomSpacing(40, after: containerView)
  }
  
  private func setupRowItemViews() {
    viewModel.rowItemViewModels.forEach { rowItemViewModel in
      let rowItemView = LessonDetailsRowItemView(viewModel: rowItemViewModel)
      if let type = rowItemViewModel.additionalViewType {
        rowItemView.addAdditionalView(makeRowAdditionalView(type: type))
      }
      stackView.addArrangedSubview(rowItemView)
    }
  }
  
  private func makeRowAdditionalView(type: LessonDetailsRowAdditionalViewType) -> UIView {
    switch type {
    case .map(let viewModel):
      return LessonDetailsMapView(viewModel: viewModel)
    }
  }
}
