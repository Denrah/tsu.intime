//
//  LessonDetailsMapView.swift
//  TSUInTime
//

import UIKit
import MapKit

class LessonDetailsMapView: UIView {
  // MARK: - Properties
  
  private let mapView = MKMapView()
  
  private let viewModel: LessonDetailsMapViewModel
  
  // MARK: - Init
  
  init(viewModel: LessonDetailsMapViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Actions
  
  @objc private func handleTap() {
    viewModel.didTapView()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupMapView()
    setupAnnotation()
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }
  
  private func setupMapView() {
    addSubview(mapView)
    mapView.layer.cornerRadius = 4
    mapView.isUserInteractionEnabled = false
    mapView.centerCoordinate = viewModel.coordinate
    mapView.setRegion(mapView.regionThatFits(viewModel.region), animated: false)
    mapView.delegate = self
    mapView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.height.equalTo(128)
    }
  }
  
  private func setupAnnotation() {
    let annotation = MKPointAnnotation()
    annotation.coordinate = viewModel.coordinate
    mapView.addAnnotation(annotation)
  }
}

// MARK: - MKMapViewDelegate

extension LessonDetailsMapView: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MapMarkerView.reuseIdentifier) {
      (annotationView as? MapMarkerView)?.configure(with: viewModel.markerViewModel)
      annotationView.annotation = annotation
      return annotationView
    } else {
      let annotationView = MapMarkerView(annotation: annotation, reuseIdentifier: MapMarkerView.reuseIdentifier)
      annotationView.configure(with: viewModel.markerViewModel)
      return annotationView
    }
  }
}
