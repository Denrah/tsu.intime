//
//  VerticalDashedLineView.swift
//  TSUInTime
//

import UIKit

class VerticalDashedLineView: UIView {
  override var intrinsicContentSize: CGSize {
    CGSize(width: 1, height: UIView.noIntrinsicMetric)
  }
  
  init() {
    super.init(frame: .zero)
    drawLine()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    drawLine()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    drawLine()
  }
  
  private func drawLine() {
    layer.sublayers?.forEach { $0.removeFromSuperlayer() }
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.strokeColor = UIColor.shade2.cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.lineDashPattern = [2, 2]
    
    let path = UIBezierPath()
    path.move(to: .zero)
    path.addLine(to: CGPoint(x: 0, y: frame.height))
    shapeLayer.path = path.cgPath
    
    layer.addSublayer(shapeLayer)
  }
}
