//
//  LessonDetailsRowItemView.swift
//  TSUInTime
//

import UIKit

class LessonDetailsRowItemView: UIView {
  // MARK: - Properties
  
  private let iconImageView = UIImageView()
  private let dashedLineView = VerticalDashedLineView()
  private let stackView = UIStackView()
  private let titleLabel = Label(textStyle: .body)
  private let disclosureIndicatorImageView = UIImageView()
  
  private let viewModel: LessonDetailsRowItemViewModel
  
  // MARK: - Init
  
  init(viewModel: LessonDetailsRowItemViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Public methods
  
  func addAdditionalView(_ view: UIView) {
    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    stackView.addArrangedSubview(titleLabel)
    stackView.addArrangedSubview(view)
  }
  
  // MARK: - Actions
  
  @objc private func handleTap() {
    viewModel.didTapView()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupIconImageView()
    setupDashedLineView()
    setupStackView()
    setupTitleLabel()
    setupDisclosureIndicatorImageView()
    
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }
  
  private func setupIconImageView() {
    addSubview(iconImageView)
    iconImageView.image = viewModel.icon
    iconImageView.contentMode = .scaleAspectFit
    iconImageView.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.top.leading.equalToSuperview()
    }
  }
  
  private func setupDashedLineView() {
    addSubview(dashedLineView)
    dashedLineView.isHidden = viewModel.shouldHideDashedLine
    dashedLineView.snp.makeConstraints { make in
      make.top.equalTo(iconImageView.snp.bottom).offset(2)
      make.centerX.equalTo(iconImageView)
      make.bottom.equalToSuperview().inset(2)
    }
  }
  
  private func setupStackView() {
    addSubview(stackView)
    stackView.axis = .vertical
    stackView.spacing = 8
    stackView.snp.makeConstraints { make in
      make.top.equalToSuperview()
      make.bottom.equalToSuperview().inset(32)
      make.leading.equalTo(iconImageView.snp.trailing).offset(8)
    }
  }
  
  private func setupTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.text = viewModel.title
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .baseBlack
  }
  
  private func setupDisclosureIndicatorImageView() {
    addSubview(disclosureIndicatorImageView)
    disclosureIndicatorImageView.image = R.image.disclosureIndicator()?.withRenderingMode(.alwaysTemplate)
    disclosureIndicatorImageView.tintColor = .accent
    disclosureIndicatorImageView.contentMode = .scaleAspectFit
    disclosureIndicatorImageView.isHidden = viewModel.hidesDisclosureIndicator
    disclosureIndicatorImageView.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.leading.equalTo(stackView.snp.trailing)
      make.top.trailing.equalToSuperview()
    }
  }
}
