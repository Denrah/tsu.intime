//
//  LessonDetailsCoordinator.swift
//  TSUInTime
//

import Foundation

struct LessonDetailsCoordinatorConfiguration {
  let lesson: Lesson
}

class LessonDetailsCoordinator: ConfigurableCoordinator {
  typealias Configuration = LessonDetailsCoordinatorConfiguration
  
  // MARK: - Properties

  let navigationController: NavigationController
  let appDependency: AppDependency
  
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  private let configuration: Configuration

  // MARK: - Init

  required init(navigationController: NavigationController,
                appDependency: AppDependency, configuration: Configuration) {
    self.navigationController = navigationController
    self.appDependency = appDependency
    self.configuration = configuration
  }

  // MARK: - Navigation

  func start(animated: Bool) {
    showLessonDetailsScreen(animated: animated)
  }

  private func showLessonDetailsScreen(animated: Bool) {
    let viewModel = LessonDetailsViewModel(lesson: configuration.lesson)
    viewModel.delegate = self
    let viewController = LessonDetailsViewController(viewModel: viewModel)
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }
}

// MARK: - LessonDetailsViewModelDelegate

extension LessonDetailsCoordinator: LessonDetailsViewModelDelegate {
  func lessonDetailsViewModel(_ viewModel: LessonDetailsViewModel, didRequestToShowTimetableFor timetableInfo: TimetableInfo) {
    let configuration = TimetableCoordinatorConfiguration(timetableInfo: timetableInfo)
    show(TimetableCoordinator.self, configuration: configuration, animated: true)
  }
}
