//
//  TimetableCoordinator.swift
//  TSUInTime
//

import Foundation

struct TimetableCoordinatorConfiguration {
  let timetableInfo: TimetableInfo
}

protocol TimetableCoordinatorDelegate: AnyObject {
  func timetableCoordinatorDidRequestToSignOut(_ coordinator: TimetableCoordinator)
}

class TimetableCoordinator: ConfigurableCoordinator {
  typealias Configuration = TimetableCoordinatorConfiguration
  
  // MARK: - Properties
  
  weak var delegate: TimetableCoordinatorDelegate?
  
  let navigationController: NavigationController
  let appDependency: AppDependency
  
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  private var onNeedsToUpdateTimetableTitleView: ((_ title: String?, _ date: String?) -> Void)?
  private var onNeedsToUpdateTimetable: ((_ timetableInfo: TimetableInfo) -> Void)?
  private var onNeedsToUpdateTimetableDisplayType: (() -> Void)?
  
  private let configuration: Configuration
  
  // MARK: - Init
  
  required init(navigationController: NavigationController,
                appDependency: AppDependency, configuration: Configuration) {
    self.navigationController = navigationController
    self.appDependency = appDependency
    self.configuration = configuration
  }
  
  // MARK: - Public methods
  
  func handleCoordinatorFinished() {
    appDependency.dataStore.observer.unsubscribe(self)
  }
  
  // MARK: - Navigation
  
  func start(animated: Bool) {
    appDependency.dataStore.observer.subscribe(self)
    showTimetableScreen(animated: animated)
  }
  
  private func showTimetableScreen(animated: Bool) {
    let viewModel = TimetableViewModel(timetableInfo: configuration.timetableInfo,
                                       dependencies: appDependency)
    viewModel.delegate = self
    
    onNeedsToUpdateTimetable = { [weak viewModel] timetableInfo in
      viewModel?.update(with: timetableInfo)
    }
    
    onNeedsToUpdateTimetableDisplayType = { [weak viewModel] in
      viewModel?.update()
    }
    
    let viewController = TimetableViewController(viewModel: viewModel)
    
    let titleView = DayScheduleTitleView()
    viewController.navigationItem.titleView = titleView
    onNeedsToUpdateTimetableTitleView = { [weak titleView] title, date in
      titleView?.title = title
      titleView?.date = date
    }
    addPopObserver(for: viewController)
    
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  private func showLessonDetailsScreen(lesson: Lesson) {
    let configuration = LessonDetailsCoordinatorConfiguration(lesson: lesson)
    show(LessonDetailsCoordinator.self, configuration: configuration, animated: true)
  }
}

// MARK: - TimetableViewModelDelegate

extension TimetableCoordinator: TimetableViewModelDelegate {
  func timetableViewModelDidRequestToShowFavorites(_ viewModel: TimetableViewModel) {
    appDependency.analyticsService.logEvent(.openFavsTimetable)
    show(FavoritesCoordinator.self, animated: true)
  }
  
  func timetableViewModelDidRequestToShowMenu(_ viewModel: TimetableViewModel) {
    let coordinator = show(MenuCoordinator.self, animated: true)
    coordinator.delegate = self
  }
  
  func timetableViewModelDidRequestToReturnToMainScreen(_ viewModel: TimetableViewModel) {
    navigationController.popToRootViewController(animated: true)
  }
  
  func timetableViewModel(_ viewModel: TimetableViewModel, didRequestToSetTitleTo title: String?, date: String?) {
    onNeedsToUpdateTimetableTitleView?(title, date)
  }
  
  func timetableViewModel(_ viewModel: TimetableViewModel, didRequestToShowDetailsFor lesson: Lesson) {
    appDependency.analyticsService.logEvent(.openLessonDetails)
    showLessonDetailsScreen(lesson: lesson)
  }
}

// MARK: - MenuCoordinatorDelegate

extension TimetableCoordinator: MenuCoordinatorDelegate {
  func menuCoordinatorDidRequestToSignOut(_ coordinator: MenuCoordinator) {
    delegate?.timetableCoordinatorDidRequestToSignOut(self)
  }
}

// MARK: - DataStoreSubscriber

extension TimetableCoordinator: DataStoreSubscriber {
  func update(event: DataStoreEvent) {
    switch event {
    case .userInfoUpdated:
      guard let userInfo = appDependency.dataStore.userInfo else { return }
      onNeedsToUpdateTimetable?(TimetableInfo(userInfo: userInfo))
    case .preferredDisplayTypeUpdated:
      onNeedsToUpdateTimetableDisplayType?()
    }
  }
}
