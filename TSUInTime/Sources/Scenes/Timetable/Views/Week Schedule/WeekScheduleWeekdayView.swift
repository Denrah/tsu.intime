//
//  WeekScheduleHeaderItem.swift
//  TSUInTime
//

import UIKit

class WeekScheduleWeekdayView: UIView, Configurable {
  // MARK: - Properties
  private let weekdayLabel = Label(textStyle: .bodyBold)
  private let dateLabel = Label(textStyle: .footnote)
  
  // MARK: - Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Public Methods
  func configure(with viewModel: WeekScheduleWeekdayViewModel) {
    weekdayLabel.text = viewModel.weekday
    dateLabel.text = viewModel.dateString
  }
  
  // MARK: - Private Methods
  private func setup() {
    backgroundColor = .baseWhite
    setupMainLabel()
    setupSecondaryLabel()
  }
  
  private func setupMainLabel() {
    addSubview(weekdayLabel)
    weekdayLabel.numberOfLines = 1
    weekdayLabel.snp.makeConstraints { make in
      make.top.equalToSuperview()
      make.leading.equalToSuperview().inset(8)
      make.trailing.lessThanOrEqualToSuperview().inset(8)
    }
  }
  
  private func setupSecondaryLabel() {
    addSubview(dateLabel)
    dateLabel.textColor = .shade3
    dateLabel.numberOfLines = 1
    dateLabel.snp.makeConstraints { make in
      make.top.equalTo(weekdayLabel.snp.bottom)
      make.leading.equalToSuperview().inset(8)
      make.bottom.trailing.lessThanOrEqualToSuperview().inset(8)
    }
  }
  
}
