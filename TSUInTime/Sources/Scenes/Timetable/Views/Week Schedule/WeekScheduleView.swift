//
//  WeekScheduleView.swift
//  TSUInTime
//

import UIKit

final class WeekScheduleView: UIView, Configurable {
  // MARK: - Proreties
  private let scrollView = UIScrollView()
  private let stackView = UIStackView()
  
  private var viewModel: WeekScheduleViewModel?
  
  // MARK: - Init
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Public Methods
  func configure(with viewModel: WeekScheduleViewModel) {
    self.viewModel = viewModel
    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    setupSchedule(with: viewModel.matrix)
  }
  
  // MARK: - Private Methods
  private func setup() {
    setupScrollView()
    setupStackView()
  }
  
  private func setupScrollView() {
    addSubview(scrollView)
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.showsVerticalScrollIndicator = false
    scrollView.contentInset.bottom = Constants.bottomBarHeight + 32
    scrollView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    scrollView.addSubview(stackView)
    stackView.axis = .vertical
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(16)
    }
  }
  
  private func setupSchedule(with matrix: [[WeekScheduleCellType]]) {
    for index in 0..<matrix.count {
      setupElements(matrix[index])
      if index != matrix.count - 1 {
        setupHorizontalDividerView(stackView: stackView)
      }
    }
  }
  
  private func setupElements(_ elements: [WeekScheduleCellType]) {
    let rowStackView = UIStackView()
    rowStackView.axis = .horizontal
    rowStackView.distribution = .equalSpacing
    stackView.addArrangedSubview(rowStackView)
    elements.enumerated().forEach { index, element in
      setupElement(with: element,
                   containerType: index == 0 ? .short : .long,
                   stackView: rowStackView)
      if index != elements.count - 1 {
        setupVerticalDividerView(stackView: rowStackView)
      }
    }
  }
  
  private func setupElement(with viewModel: WeekScheduleCellType,
                            containerType: WeekScheduleCellWidth,
                            stackView: UIStackView) {
    let view: UIView
    switch viewModel {
    case .weekday(let weekdayViewModel):
      let weekdayView = WeekScheduleWeekdayView()
      weekdayView.configure(with: weekdayViewModel)
      view = weekdayView
    case .lessonTime(let lessonTimeViewModel):
      let lessonTimeView = WeekScheduleTimeView()
      lessonTimeView.configure(with: lessonTimeViewModel)
      view = lessonTimeView
    case .lessons(let lessonViewModels):
      let lessonsContainerView = makeLessonsView(with: lessonViewModels)
      view = lessonsContainerView
    case .empty:
      view = UIView()
    }
    view.snp.makeConstraints { make in
      make.width.equalTo(containerType.rawValue)
    }
    stackView.addArrangedSubview(view)
  }
  
  private func makeLessonsView(with viewModels: [WeekScheduleLessonViewModel]) -> UIView {
    let container = UIStackView()
    container.backgroundColor = .baseWhite
    container.axis = .vertical
    container.distribution = .fill
    for lessonViewModel in viewModels {
      let lessonView = WeekScheduleLessonView()
      lessonView.configure(with: lessonViewModel)
      container.addArrangedSubview(lessonView)
    }
    let spacer = UIView()
    spacer.snp.makeConstraints { make in
      make.height.greaterThanOrEqualTo(8)
      make.height.lessThanOrEqualTo(Int.max)
    }
    container.addArrangedSubview(spacer)
    return container
  }
  
  private func setupVerticalDividerView(stackView: UIStackView) {
    let view = UIView()
    view.snp.makeConstraints { make in
      make.width.equalTo(1)
    }
    view.backgroundColor = .shade1
    stackView.addArrangedSubview(view)
  }
  
  private func setupHorizontalDividerView(stackView: UIStackView) {
    let view = UIView()
    view.snp.makeConstraints { make in
      make.height.equalTo(1)
    }
    view.backgroundColor = .shade1
    stackView.addArrangedSubview(view)
  }
  
}
