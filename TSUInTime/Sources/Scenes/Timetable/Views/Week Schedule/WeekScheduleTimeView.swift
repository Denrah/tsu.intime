//
//  WeekScheduleTimeItemView.swift
//  TSUInTime
//

import UIKit

class WeekScheduleTimeView: UIView, Configurable {
  // MARK: - Properties
  private let startTimeLabel = Label(textStyle: .bodyBold)
  private let endTimeLabel = Label(textStyle: .body)
  private let stackView = UIStackView()
  private let firstSpacer = UIView()
  private let secondSpacer = UIView()
  
  // MARK: - Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Public Methods
  func configure(with viewModel: WeekScheduleLessonTimeViewModel) {
    startTimeLabel.text = viewModel.startTime
    endTimeLabel.text = viewModel.endTime
  }
  
  // MARK: - Private Methods
  private func setup() {
    backgroundColor = .baseWhite
    setupStackView()
    setupFirstSpacer()
    setupMainLabel()
    setupSecondaryLabel()
    setupSecondSpacer()
  }
  
  private func setupFirstSpacer() {
    stackView.addArrangedSubview(firstSpacer)
    firstSpacer.snp.makeConstraints { make in
      make.height.greaterThanOrEqualTo(8)
      make.height.lessThanOrEqualTo(Int.max)
    }
  }
  
  private func setupStackView() {
    addSubview(stackView)
    stackView.alignment = .leading
    stackView.axis = .vertical
    stackView.spacing = 0
    stackView.snp.makeConstraints { make in
      make.top.bottom.equalToSuperview().inset(8)
      make.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupMainLabel() {
    stackView.addArrangedSubview(startTimeLabel)
    startTimeLabel.numberOfLines = 1
  }
  
  private func setupSecondaryLabel() {
    stackView.addArrangedSubview(endTimeLabel)
    endTimeLabel.textColor = .shade3
    endTimeLabel.numberOfLines = 1
  }
  
  private func setupSecondSpacer() {
    stackView.addArrangedSubview(secondSpacer)
    secondSpacer.snp.makeConstraints { make in
      make.height.greaterThanOrEqualTo(8)
      make.height.lessThanOrEqualTo(Int.max)
      make.height.equalTo(firstSpacer)
    }
  }
  
}
