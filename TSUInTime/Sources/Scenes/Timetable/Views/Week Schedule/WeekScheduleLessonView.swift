//
//  WeekScheduleLessonItem.swift
//  TSUInTime
//

import UIKit

class WeekScheduleLessonView: UIView, Configurable {
  // MARK: - Properties
  private let cardView = UIView()
  private let accentColumn = UIView()
  private let header = Label(textStyle: .footnote)
  private let subheader = Label(textStyle: .footnote)
  
  private var viewModel: WeekScheduleLessonViewModel?
  
  // MARK: - Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Public Methods
  func configure(with viewModel: WeekScheduleLessonViewModel) {
    self.viewModel = viewModel
    header.text = viewModel.lessonTitle
    subheader.text = viewModel.lessonGroups
    accentColumn.backgroundColor = viewModel.lessonColor
    cardView.backgroundColor = viewModel.lessonColor.withAlphaComponent(0.1)
  }
  
  // MARK: - Actions
  @objc private func handleTap() {
    viewModel?.select()
  }
  
  // MARK: - Private Methods
  private func setup() {
    backgroundColor = .baseWhite
    setupCardView()
    setupAccentColumn()
    setupHeader()
    setupSubheader()
    setupGestureRecognizer()
  }
  
  private func setupCardView() {
    cardView.layer.cornerRadius = 4
    cardView.clipsToBounds = true
    addSubview(cardView)
    cardView.snp.makeConstraints { make in
      make.bottom.equalToSuperview()
      make.top.leading.trailing.equalToSuperview().inset(8)
    }
  }
  
  private func setupAccentColumn() {
    cardView.addSubview(accentColumn)
    accentColumn.snp.makeConstraints { make in
      make.top.bottom.leading.equalToSuperview()
      make.width.equalTo(4)
    }
  }
  
  private func setupHeader() {
    cardView.addSubview(header)
    header.numberOfLines = 0
    header.snp.makeConstraints { make in
      make.leading.equalTo(accentColumn.snp.trailing).offset(4)
      make.trailing.equalToSuperview().inset(4)
      make.top.equalToSuperview().inset(4)
    }
  }
  
  private func setupSubheader() {
    cardView.addSubview(subheader)
    subheader.numberOfLines = 0
    subheader.textColor = .shade4
    subheader.snp.makeConstraints { make in
      make.leading.equalTo(accentColumn.snp.trailing).offset(4)
      make.trailing.equalToSuperview().inset(4)
      make.top.equalTo(header.snp.bottom).offset(8)
      make.bottom.equalToSuperview().inset(4)
    }
  }
  
  private func setupGestureRecognizer() {
    let tap = UITapGestureRecognizer(target: self,
                                     action: #selector(handleTap))
    addGestureRecognizer(tap)
  }
  
}
