//
//  CalendarBarWeekView.swift
//  TSUInTime
//

import UIKit

class CalendarBarWeekView: UIView {
  // MARK: - Properties
  override var intrinsicContentSize: CGSize {
    CGSize(width: UIView.noIntrinsicMetric, height: 56)
  }
  
  private let stackView = UIStackView()
  private let weekInfoLabel = UILabel()
  private let previousWeekButton = UIButton(type: .system)
  private let nextWeekButton = UIButton(type: .system)
  
  private let viewModel: CalendarBarWeekViewModel
  
  // MARK: - Init
  init(viewModel: CalendarBarWeekViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
    bindToViewModel()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Public Methods
  func update() {
    updateDateItems()
  }
  
  // MARK: - Actions
  
  @objc private func didTapPreviousWeekButton() {
    viewModel.switchToPreviousWeek()
  }
  
  @objc private func didTapNextWeekButton() {
    viewModel.switchToNextWeek()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupStackView()
    setupPreviousWeekButton()
    setupNextWeekButton()
    setupWeekInfoLabel()
    updateDateItems()
  }
  
  private func setupContainer() {
    backgroundColor = .baseWhite
    addShadow(offset: CGSize(width: 0, height: 4), radius: 24, color: .zeroBlack, opacity: 0.1)
  }
  
  private func setupStackView() {
    addSubview(stackView)
    stackView.axis = .horizontal
    stackView.distribution = .equalSpacing
    stackView.alignment = .center
    stackView.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview()
      make.top.bottom.equalToSuperview().inset(8)
    }
  }
  
  private func setupWeekInfoLabel() {
    addSubview(weekInfoLabel)
    weekInfoLabel.font = .bodyBold
    weekInfoLabel.lineBreakMode = .byTruncatingMiddle
  }
  
  private func setupPreviousWeekButton() {
    previousWeekButton.setImage(R.image.chevronLeft()?.withRenderingMode(.alwaysTemplate), for: .normal)
    previousWeekButton.tintColor = .accent
    
    if #available(iOS 15.0, *) {
      var configuration = UIButton.Configuration.plain()
      configuration.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 8, bottom: 0, trailing: -8)
      previousWeekButton.configuration = configuration
      previousWeekButton.configurationUpdateHandler = { button in
        button.tintColor = button.isHighlighted ? .accentFaded : .accent
      }
    } else {
      previousWeekButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: -8)
    }
    
    previousWeekButton.addTarget(self, action: #selector(didTapPreviousWeekButton), for: .touchUpInside)
    previousWeekButton.snp.makeConstraints { make in
      make.size.equalTo(40)
    }
  }
  
  private func setupNextWeekButton() {
    nextWeekButton.setImage(R.image.chevronRight()?.withRenderingMode(.alwaysTemplate), for: .normal)
    nextWeekButton.tintColor = .accent
    
    if #available(iOS 15.0, *) {
      var configuration = UIButton.Configuration.plain()
      configuration.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: -8, bottom: 0, trailing: 8)
      nextWeekButton.configuration = configuration
      nextWeekButton.configurationUpdateHandler = { button in
        button.tintColor = button.isHighlighted ? .accentFaded : .accent
      }
    } else {
      nextWeekButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 8)
    }
    
    nextWeekButton.addTarget(self, action: #selector(didTapNextWeekButton), for: .touchUpInside)
    nextWeekButton.snp.makeConstraints { make in
      make.size.equalTo(40)
    }
  }
  
  // MARK: - Bind
  private func bindToViewModel() {
    viewModel.onDidUpdate = { [weak self] in
      self?.updateDateItems()
    }
  }
  
  // MARK: - Private methods
  private func updateDateItems() {
    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    stackView.addArrangedSubview(previousWeekButton)
    setupWeekView()
    stackView.addArrangedSubview(nextWeekButton)
  }
  
  private func setupWeekView() {
    stackView.addArrangedSubview(weekInfoLabel)
    weekInfoLabel.text = viewModel.displayString ?? R.string.common.error()
  }
  
}
