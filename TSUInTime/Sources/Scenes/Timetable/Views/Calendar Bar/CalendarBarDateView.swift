//
//  CalendarBarDateView.swift
//  TSUInTime
//

import UIKit

class CalendarBarDateView: UIView {
  // MARK: - Properties
  override var intrinsicContentSize: CGSize {
    CGSize(width: 24, height: 50)
  }
  
  private let dayOfWeekLabel = Label(textStyle: .smallFootnote)
  private let dateContainerView = UIView()
  private let dateLabel = Label(textStyle: .bodyBold)
  
  private let viewModel: CalendarBarDateViewModel
  
  // MARK: - Init
  init(viewModel: CalendarBarDateViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
    bindToViewModel()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Actions
  @objc private func handleTap() {
    viewModel.didTapView()
  }
  
  // MARK: - Setup
  private func setup() {
    setupContainer()
    setupDayOfWeekLabel()
    setupDateContainerView()
    setupDateLabel()
    updateState()
  }
  
  private func setupContainer() {
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }
  
  private func setupDayOfWeekLabel() {
    addSubview(dayOfWeekLabel)
    dayOfWeekLabel.text = viewModel.dayOfWeek
    dayOfWeekLabel.textColor = .shade3
    dayOfWeekLabel.textAlignment = .center
    dayOfWeekLabel.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupDateContainerView() {
    addSubview(dateContainerView)
    dateContainerView.layer.cornerRadius = 8
    dateContainerView.snp.makeConstraints { make in
      make.size.equalTo(32)
      make.top.equalTo(dayOfWeekLabel.snp.bottom).offset(4)
      make.leading.trailing.bottom.equalToSuperview()
    }
  }
  
  private func setupDateLabel() {
    dateContainerView.addSubview(dateLabel)
    dateLabel.text = viewModel.dayOfMonth
    dateLabel.textColor = .baseBlack
    dateLabel.snp.makeConstraints { make in
      make.center.equalToSuperview()
    }
  }
  
  // MARK: - Bind
  private func bindToViewModel() {
    viewModel.onDidUpdate = { [weak self] in
      self?.updateState()
    }
  }
  
  // MARK: - Private methods
  private func updateState() {
    dateLabel.textColor = viewModel.isSelected ? .baseWhite : .baseBlack
    dateContainerView.backgroundColor = viewModel.isSelected ? .accent : .clear
  }
  
}
