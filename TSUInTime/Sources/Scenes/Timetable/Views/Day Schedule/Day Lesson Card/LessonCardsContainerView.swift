//
//  LessonCardsContainerView.swift
//  TSUInTime
//

import UIKit

typealias LessonCardsContainerCell = TableCellContainer<LessonCardsContainerView>

class LessonCardsContainerView: UIView, Configurable {
  // MARK: - Properties
  
  private let containerView = UIView()
  private let stackView = UIStackView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: LessonCardsContainerViewModel) {
    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    
    viewModel.cardViewModels.forEach { cardViewModel in
      let cardView = LessonCardView()
      cardView.configure(with: cardViewModel)
      stackView.addArrangedSubview(cardView)
    }
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainerView()
    setupStackView()
  }
  
  private func setupContainerView() {
    addSubview(containerView)
    containerView.backgroundColor = .shade1
    containerView.layer.cornerRadius = 8
    containerView.addShadow(offset: CGSize(width: 0, height: 6), radius: 24,
                            color: .baseBlack, opacity: 0.1)
    containerView.snp.makeConstraints { make in
      make.top.trailing.equalToSuperview().inset(8)
      make.leading.bottom.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    addSubview(stackView)
    stackView.axis = .vertical
    stackView.spacing = -7
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
}

// MARK: - PaddingsDescribing

extension LessonCardsContainerView: PaddingsDescribing {
  var paddings: UIEdgeInsets {
    UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 8)
  }
}
