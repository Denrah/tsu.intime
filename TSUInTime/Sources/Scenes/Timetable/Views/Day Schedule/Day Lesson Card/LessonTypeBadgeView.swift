//
//  LessonTypeBadgeView.swift
//  TSUInTime
//

import UIKit

class LessonTypeBadgeView: UIView {
  // MARK: - Properties
  
  private let titleLabel = Label(textStyle: .footnoteBold)
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with lessonType: LessonType) {
    backgroundColor = lessonType.color
    titleLabel.text = lessonType.title
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupTitleLabel()
  }
  
  private func setupContainer() {
    layer.cornerRadius = 4
    snp.makeConstraints { make in
      make.height.equalTo(24)
    }
  }
  
  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .baseWhite
    titleLabel.snp.makeConstraints { make in
      make.centerY.equalToSuperview()
      make.leading.trailing.equalToSuperview().inset(8)
    }
  }
}
