//
//  LessonCardView.swift
//  TSUInTime
//

import UIKit

class LessonCardView: UIView, Configurable {
  // MARK: - Properties
  
  private let containerView = UIView()
  private let stackView = UIStackView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let professorLabel = Label(textStyle: .footnote)
  private let auditoryLabel = Label(textStyle: .footnote)
  private let groupLabel = Label(textStyle: .footnote)
  private let bottomContentStackView = UIStackView()
  private let timeLabel = Label(textStyle: .bodyBold)
  private let lessonNumberLabel = Label(textStyle: .body)
  private let typeBadgeView = LessonTypeBadgeView()
  
  private var viewModel: LessonCardViewModel?
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: LessonCardViewModel) {
    self.viewModel = viewModel
    
    titleLabel.text = viewModel.title
    professorLabel.text = viewModel.professor
    auditoryLabel.text = viewModel.auditory
    groupLabel.text = viewModel.group
    timeLabel.text = viewModel.time
    lessonNumberLabel.text = viewModel.lessonNumber
    typeBadgeView.configure(with: viewModel.type)
    containerView.layer.maskedCorners = viewModel.cornerMask
  }
  
  // MARK: - Actions
  
  @objc private func handleTap() {
    viewModel?.didTapView()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainerView()
    setupStackView()
    setupTitleLabel()
    setupProfessorLabel()
    setupAuditoryLabel()
    setupGroupLabel()
    setupBottomContentStackView()
    setupTimeLabel()
    setupLessonNumberLabel()
    setupTypeBadgeView()
  }
  
  private func setupContainerView() {
    addSubview(containerView)
    containerView.backgroundColor = .baseWhite
    containerView.layer.cornerRadius = 8
    containerView.snp.makeConstraints { make in
      make.top.trailing.equalToSuperview().inset(8)
      make.leading.bottom.equalToSuperview()
    }
    
    containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }
  
  private func setupStackView() {
    containerView.addSubview(stackView)
    stackView.axis = .vertical
    stackView.spacing = 8
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(16)
    }
  }
  
  private func setupTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
  }
  
  private func setupProfessorLabel() {
    let infoRowView = makeInfoRowView(icon: R.image.professorIconSmall(), label: professorLabel)
    stackView.addArrangedSubview(infoRowView)
    professorLabel.textColor = .shade3
    professorLabel.numberOfLines = 0
  }
  
  private func setupAuditoryLabel() {
    let infoRowView = makeInfoRowView(icon: R.image.auditoryIconSmall(), label: auditoryLabel)
    stackView.addArrangedSubview(infoRowView)
    auditoryLabel.textColor = .shade3
    auditoryLabel.numberOfLines = 0
  }
  
  private func setupGroupLabel() {
    let infoRowView = makeInfoRowView(icon: R.image.groupIconSmall(), label: groupLabel)
    stackView.addArrangedSubview(infoRowView)
    groupLabel.textColor = .shade3
    groupLabel.numberOfLines = 0
  }
  
  private func setupBottomContentStackView() {
    stackView.addArrangedSubview(bottomContentStackView)
    bottomContentStackView.axis = .horizontal
    bottomContentStackView.distribution = .equalSpacing
  }
  
  private func setupTimeLabel() {
    bottomContentStackView.addArrangedSubview(timeLabel)
    timeLabel.textColor = .accent
  }
  
  private func setupLessonNumberLabel() {
    bottomContentStackView.addArrangedSubview(lessonNumberLabel)
    lessonNumberLabel.textAlignment = .right
    lessonNumberLabel.textColor = .shade3
  }
  
  private func setupTypeBadgeView() {
    addSubview(typeBadgeView)
    typeBadgeView.snp.makeConstraints { make in
      make.top.trailing.equalToSuperview()
      make.leading.greaterThanOrEqualToSuperview().offset(16)
    }
  }
  
  private func makeInfoRowView(icon: UIImage?, label: UIView) -> UIView {
    let rowStackView = UIStackView()
    rowStackView.axis = .horizontal
    rowStackView.spacing = 8
    rowStackView.alignment = .center
    
    let iconImageView = UIImageView()
    iconImageView.image = icon
    iconImageView.snp.makeConstraints { make in
      make.size.equalTo(16)
    }
    
    rowStackView.addArrangedSubview(iconImageView)
    rowStackView.addArrangedSubview(label)
    
    return rowStackView
  }
}
