//
//  DayScheduleView.swift
//  TSUInTime
//

import UIKit

final class DayScheduleView: UIView, Configurable {
  // MARK: - Properties
  private let scrollView = UIScrollView()
  private let stackView = UIStackView()
  
  private var viewModel: DayScheduleViewModel?
  
  // MARK: - Init
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Public Methods
  func configure(with viewModel: DayScheduleViewModel) {
    self.viewModel = viewModel
    setupTimetablePages()
  }
  
  func jumpToPage(pageNumber: Int) {
    let offset = CGFloat(pageNumber - 1) * frame.width
    scrollView.setContentOffset(CGPoint(x: offset, y: 0), animated: false)
  }
  
  // MARK: - Private Methods
  private func setup() {
    setupScrollView()
    setupStackView()
  }
  
  private func setupScrollView() {
    addSubview(scrollView)
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.isPagingEnabled = true
    scrollView.contentInsetAdjustmentBehavior = .never
    scrollView.delegate = self
    scrollView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    scrollView.addSubview(stackView)
    stackView.axis = .horizontal
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.height.equalToSuperview()
    }
  }
  
  private func setupTimetablePages() {
    guard let viewModels = viewModel?.timetablePageViewModels else { return }
    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    viewModels.forEach { pageViewModel in
      let pageView = DaySchedulePageView(viewModel: pageViewModel)
      stackView.addArrangedSubview(pageView)
      pageView.snp.makeConstraints { make in
        make.width.equalTo(self.snp.width)
      }
    }
  }
  
}

// MARK: - UIScrollViewDelegate

extension DayScheduleView: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let pageIndex = Int(round(scrollView.contentOffset.x / frame.width))
    viewModel?.didScrollToPage(pageIndex)
  }
}
