//
//  DayScheduleTitleView.swift
//  TSUInTime
//

import UIKit
import MarqueeLabel

private extension Constants {
  static let titleAnimationDuration: CGFloat = 10
  static let titleAnimationDelay: CGFloat = 10
  static let titleFadeLength: CGFloat = 16
}

class DayScheduleTitleView: UIView {
  // MARK: - Properties

  var title: String? {
    get {
      titleLabel.text
    }
    set {
      titleLabel.text = newValue
    }
  }

  var date: String? {
    get {
      dateLabel.text
    }
    set {
      dateLabel.text = newValue
    }
  }

  private let stackView = UIStackView()
  private let titleLabel = MarqueeLabel(frame: .zero, duration: Constants.titleAnimationDuration,
                                        fadeLength: Constants.titleFadeLength)
  private let dateLabel = Label(textStyle: .footnote)

  // MARK: - Init

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Lifecycle
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.fadeLength = Constants.titleFadeLength
    titleLabel.animationDelay = Constants.titleAnimationDelay
  }

  // MARK: - Setup

  private func setup() {
    setupStackView()
    setupTitleLabel()
    setupDateLabel()
  }

  private func setupStackView() {
    addSubview(stackView)
    stackView.axis = .vertical
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }

  private func setupTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.font = .appBold
    titleLabel.textColor = .baseBlack
    titleLabel.textAlignment = .center
  }

  private func setupDateLabel() {
    stackView.addArrangedSubview(dateLabel)
    dateLabel.textColor = .shade3
    dateLabel.textAlignment = .center
  }
}
