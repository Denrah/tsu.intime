//
//  DayScheduleLunchView.swift
//  TSUInTime
//

import UIKit

typealias DayScheduleLunchCell = TableCellContainer<DayScheduleLunchView>

class DayScheduleLunchView: UIView, Configurable {
  // MARK: - Properties
  
  private let containerView = UIView()
  private let stackView = UIStackView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let subtitleLabel = Label(textStyle: .body)
  private let imageView = UIImageView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: TimetableLunchViewModel) {
    titleLabel.text = viewModel.title
    subtitleLabel.text = viewModel.subtitle
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainerView()
    setupStackView()
    setupTitleLabel()
    setupSubtitleLabel()
    setupImageView()
  }
  
  private func setupContainerView() {
    addSubview(containerView)
    containerView.backgroundColor = .accentGreenLight
    containerView.layer.cornerRadius = 8
    containerView.snp.makeConstraints { make in
      make.height.equalTo(80)
      make.top.equalToSuperview().inset(8)
      make.leading.equalToSuperview().inset(4)
      make.trailing.bottom.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    containerView.addSubview(stackView)
    stackView.spacing = 4
    stackView.axis = .vertical
    stackView.snp.makeConstraints { make in
      make.center.equalToSuperview()
    }
  }
  
  private func setupTitleLabel() {
    stackView.addArrangedSubview(titleLabel)
    titleLabel.textColor = .accentGreen
    titleLabel.textAlignment = .center
  }
  
  private func setupSubtitleLabel() {
    stackView.addArrangedSubview(subtitleLabel)
    subtitleLabel.textColor = .accentGreen
    subtitleLabel.textAlignment = .center
  }
  
  private func setupImageView() {
    addSubview(imageView)
    imageView.image = R.image.lunchCellImage()
    imageView.contentMode = .scaleAspectFit
    imageView.snp.makeConstraints { make in
      make.top.leading.equalToSuperview()
      make.width.equalTo(78)
      make.height.equalTo(84)
    }
  }
}

// MARK: - PaddingsDescribing

extension DayScheduleLunchView: PaddingsDescribing {
  var paddings: UIEdgeInsets {
    return UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 16)
  }
}
