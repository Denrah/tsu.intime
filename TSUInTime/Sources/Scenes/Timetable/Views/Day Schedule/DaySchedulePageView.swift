//
//  DaySchedulePageView.swift
//  TSUInTime
//

import UIKit

class DaySchedulePageView: UIView {
  // MARK: - Properties

  private let tableView = UITableView(frame: .zero, style: .grouped)
  private let emptyStateView = EmptyStateView()
  
  private let viewModel: TimetablePageViewModel
  
  private let dataSource = TableViewDataSource()

  // MARK: - Init
  
  init(viewModel: TimetablePageViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Setup
  
  private func setup() {
    setupTableView()
    setupEmptyStateView()
  }
  
  private func setupTableView() {
    addSubview(tableView)
    tableView.clipsToBounds = false
    tableView.showsVerticalScrollIndicator = false
    tableView.alwaysBounceVertical = false
    tableView.backgroundColor = .clear
    tableView.separatorStyle = .none
    tableView.contentInset.top = 8
    tableView.contentInset.bottom = 80
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 122
    tableView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    
    tableView.register(LessonCardsContainerCell.self, forCellReuseIdentifier: LessonCardsContainerCell.reuseIdentifier)
    tableView.register(DayScheduleSpaceCell.self, forCellReuseIdentifier: DayScheduleSpaceCell.reuseIdentifier)
    tableView.register(DayScheduleLunchCell.self, forCellReuseIdentifier: DayScheduleLunchCell.reuseIdentifier)
    dataSource.setup(tableView: tableView, viewModel: viewModel)
    
    tableView.setContentOffset(CGPoint(x: 0, y: -8), animated: false)
  }

  private func setupEmptyStateView() {
    addSubview(emptyStateView)
    emptyStateView.configure(with: viewModel.emptyStateViewModel)
    emptyStateView.isHidden = viewModel.hasLessons
    emptyStateView.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(24)
      make.centerY.equalToSuperview().offset(-66)
    }
  }
}
