//
//  DayScheduleSpaceView.swift
//  TSUInTime
//

import UIKit
import SnapKit

typealias DayScheduleSpaceCell = TableCellContainer<DayScheduleSpaceView>

class DayScheduleSpaceView: UIView, Configurable {
  // MARK: - Properties
  
  private let titleLabel = Label(textStyle: .body)
  
  private var heightConstraint: Constraint?
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: TimetableSpaceViewModel) {
    titleLabel.text = viewModel.title
    heightConstraint?.update(offset: viewModel.height)
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupTitleLabel()
  }
  
  private func setupContainer() {
    backgroundColor = .shade1
    layer.cornerRadius = 8
    snp.makeConstraints { make in
      heightConstraint = make.height.equalTo(0).priority(999).constraint
    }
  }
  
  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .shade3
    titleLabel.textAlignment = .center
    titleLabel.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }
}

// MARK: - PaddingsDescribing

extension DayScheduleSpaceView: PaddingsDescribing {
  var paddings: UIEdgeInsets {
    return UIEdgeInsets(top: 16, left: 16, bottom: 8, right: 16)
  }
}
