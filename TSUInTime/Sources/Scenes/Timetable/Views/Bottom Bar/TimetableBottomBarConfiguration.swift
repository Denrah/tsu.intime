//
//  TimetableBottomBarConfiguration.swift
//  TSUInTime
//

import Foundation

enum TimetableBottomBarConfiguration: Codable {
  case mainTimetable, additionalTimetable
  
  var bottomBarItems: [TimetableBottomBarItemType] {
    switch self {
    case .mainTimetable:
      return [.favorites, .date, .today, .day, .menu]
    case .additionalTimetable:
      return [.addToFavorites, .date, .today, .day, .home]
    }
  }
  
}
