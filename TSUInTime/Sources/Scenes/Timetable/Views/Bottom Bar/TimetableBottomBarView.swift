//
//  TimetableBottomBarView.swift
//  TSUInTime
//

import UIKit

class TimetableBottomBarView: UIView {
  // MARK: - Properties
  
  var onDidSelectItem: ((_ itemType: TimetableBottomBarItemType) -> Void)?
  
  override var intrinsicContentSize: CGSize {
    CGSize(width: UIView.noIntrinsicMetric, height: Constants.bottomBarHeight)
  }
  
  private let backgroundView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
  private let stackView = UIStackView()
  
  private let bottomBarItems: [TimetableBottomBarItemType]
  
  // MARK: - Init
  
  init(configuration: TimetableBottomBarConfiguration) {
    self.bottomBarItems = configuration.bottomBarItems
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Public methods

  func update(itemOfType type: TimetableBottomBarItemType, withType newType: TimetableBottomBarItemType) {
    stackView.arrangedSubviews.compactMap { $0 as? TimetableBottomBarItemView }
    .first { $0.itemType == type }?.configure(with: newType)
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupBackgroundView()
    setupStackView()
    setupBottomBarItems()
  }
  
  private func setupContainer() {
    layer.cornerRadius = 16
    addShadow(offset: CGSize(width: 0, height: 6), radius: 24, color: .zeroBlack, opacity: 0.08)
  }
  
  private func setupBackgroundView() {
    addSubview(backgroundView)
    backgroundView.layer.cornerRadius = 16
    backgroundView.clipsToBounds = true
    backgroundView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    addSubview(stackView)
    stackView.axis = .horizontal
    stackView.distribution = .equalSpacing
    stackView.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.top.bottom.equalToSuperview().inset(8)
    }
  }
  
  private func setupBottomBarItems() {
    bottomBarItems.forEach { item in
      let itemView = TimetableBottomBarItemView()
      itemView.configure(with: item)
      itemView.onDidTap = { [weak self] itemType in
        self?.onDidSelectItem?(itemType)
      }
      stackView.addArrangedSubview(itemView)
    }
  }
}
