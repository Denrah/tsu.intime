//
//  TimetableBottomBarItemType.swift
//  TSUInTime
//

import UIKit

enum TimetableBottomBarItemType: CaseIterable {
  case favorites, date, today, day, week, menu, home, addToFavorites, deleteFromFavorites
  
  var title: String {
    switch self {
    case .favorites:
      return R.string.timetable.bottomBarFavoritesTitle()
    case .date:
      return R.string.timetable.bottomBarDateTitle()
    case .today:
      return R.string.timetable.bottomBarTodayTitle()
    case .menu:
      return R.string.timetable.bottomBarMenuTitle()
    case .home:
      return R.string.timetable.bottomBarHomeTitle()
    case .addToFavorites:
      return R.string.timetable.bottomBarAddToFavoritesTitle()
    case .deleteFromFavorites:
      return R.string.timetable.bottomBarDeleteFromFavoritesTitle()
    case .week:
      return R.string.timetable.bottomBarWeekTitle()
    case .day:
      return R.string.timetable.bottomBarDayTitle()
    }
  }
  
  var icon: UIImage? {
    switch self {
    case .favorites:
      return R.image.favoritesIcon()
    case .date:
      return R.image.dateIcon()
    case .today:
      return R.image.todayIcon()
    case .menu:
      return R.image.menuIcon()
    case .home:
      return R.image.homeIcon()
    case .addToFavorites:
      return R.image.favoritesAddIcon()
    case .deleteFromFavorites:
      return R.image.favoritesRemoveIcon()
    case .week:
      return R.image.weekIcon()
    case .day:
      return R.image.dayIcon()
    }
  }

  var color: UIColor {
    switch self {
    case .addToFavorites, .deleteFromFavorites:
      return .accentRed
    default:
      return .accent
    }
  }

  var highlightColor: UIColor {
    switch self {
    case .addToFavorites, .deleteFromFavorites:
      return .accentRedFaded
    default:
      return .accentFaded
    }
  }
}
