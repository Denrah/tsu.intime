//
//  TimetableViewController.swift
//  TSUInTime
//

import UIKit

class TimetableViewController: BaseViewController, DataLoadingView,
                               ActivityIndicatorViewDisplaying, ErrorHandling,
                               EmptyStateErrorViewDisplaying {
  // MARK: - Properties
  
  let activityIndicatorView = ActivityIndicatorView()
  let emptyStateErrorView = EmptyStateErrorView()
  
  private let calendarBarDayView: CalendarBarDayView
  private let calendarBarWeekView: CalendarBarWeekView
  private let dayScheduleView = DayScheduleView()
  private let weekScheduleView = WeekScheduleView()
  private let emptyStateView = EmptyStateView()
  private let bottomBarView: TimetableBottomBarView
  
  private let viewModel: TimetableViewModel
  
  // MARK: - Init
  
  init(viewModel: TimetableViewModel) {
    self.viewModel = viewModel
    self.calendarBarDayView = CalendarBarDayView(viewModel: viewModel.calendarBarDayViewModel)
    self.calendarBarWeekView = CalendarBarWeekView(viewModel: viewModel.calendarBarWeekViewModel)
    self.bottomBarView = TimetableBottomBarView(configuration: viewModel.bottomBarConfiguration)
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
    viewModel.viewIsReady()
  }
  
  // MARK: - Public methods
  
  func handleRequestStarted() {
    emptyStateErrorView.isHidden = true
    emptyStateView.isHidden = true
    dayScheduleView.isHidden = true
    weekScheduleView.isHidden = true
  }
  
  func handleRequestFinished() {
    emptyStateErrorView.isHidden = true
    emptyStateView.isHidden = true
    guard viewModel.hasTimetable else {
      emptyStateView.isHidden = false
      return
    }
    switch viewModel.timetableDisplayType {
    case .day:
      showDaySchedule()
    case .week:
      showWeekdaySchedule()
    }
  }
  
  func handleErrorReceived() {
    dayScheduleView.isHidden = true
    weekScheduleView.isHidden = true
    emptyStateView.isHidden = true
  }
  
  func reloadData() {
    switch viewModel.timetableDisplayType {
    case .day:
      showDaySchedule()
      updateDayScheduleView()
    case .week:
      showWeekdaySchedule()
      updateWeekScheduleView()
    }
    
    if let emptyStateViewModel = viewModel.emptyStateViewModel {
      emptyStateView.configure(with: emptyStateViewModel)
    }
    guard !viewModel.hasTimetable else { return }
    weekScheduleView.isHidden = true
    dayScheduleView.isHidden = true
    emptyStateView.isHidden = false
  }
  
  func handleRefreshButtonTapped() {
    viewModel.viewIsReady()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupCalendarBarDayView()
    setupCalendarBarWeekView()
    setupDayScheduleView()
    setupWeekScheduleView()
    setupEmptyStateView()
    setupBottomBarView()
    setupEmptyStateErrorView()
    setupActivityIndicatorView()
    
    view.bringSubviewToFront(calendarBarDayView)
    view.bringSubviewToFront(calendarBarWeekView)
  }
  
  private func setupCalendarBarWeekView() {
    view.addSubview(calendarBarWeekView)
    calendarBarWeekView.isHidden = viewModel.timetableDisplayType != .week
    calendarBarWeekView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupCalendarBarDayView() {
    view.addSubview(calendarBarDayView)
    calendarBarDayView.isHidden = viewModel.timetableDisplayType != .day
    calendarBarDayView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupDayScheduleView() {
    view.addSubview(dayScheduleView)
    dayScheduleView.snp.makeConstraints { make in
      make.top.equalTo(calendarBarDayView.snp.bottom)
      make.bottom.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupWeekScheduleView() {
    view.addSubview(weekScheduleView)
    weekScheduleView.snp.makeConstraints { make in
      make.top.equalTo(calendarBarWeekView.snp.bottom)
      make.bottom.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupEmptyStateView() {
    view.addSubview(emptyStateView)
    emptyStateView.isHidden = true
    emptyStateView.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(24)
      make.centerY.equalToSuperview()
    }
  }
  
  private func setupBottomBarView() {
    view.addSubview(bottomBarView)
    bottomBarView.onDidSelectItem = { [weak self] itemType in
      self?.viewModel.didSelectBottomBarItem(itemType: itemType)
    }
    bottomBarView.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-16).priority(750)
      make.bottom.greaterThanOrEqualToSuperview().offset(-40)
    }
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    bind(to: viewModel)
    viewModel.onDidRequestToSelectDate = { [weak self] selectedDate, selectedWeek in
      self?.showDatePicker(defaultDate: selectedDate, defaultWeek: selectedWeek)
    }
    viewModel.onDidRequestToJumpToPage = { [weak self] pageNumber in
      self?.dayScheduleView.jumpToPage(pageNumber: pageNumber)
    }
    viewModel.onDidAddToFavorites = { [weak self] in
      self?.showBanner(title: nil, subtitle: R.string.favorites.addedToFavoritesBannerTitle(), style: .success)
    }
    viewModel.onDidRemoveFromFavorites = { [weak self] in
      self?.showBanner(title: nil, subtitle: R.string.favorites.removedFromFavoritesBannerTitle(), style: .success)
    }
    viewModel.onDidRequestToUpdateBottomBarItem = { [weak self] oldType, newType in
      self?.bottomBarView.update(itemOfType: oldType, withType: newType)
    }
    viewModel.onDidSwitchTimetableDisplayType = { [weak self] timetableDisplayType in
      self?.calendarBarDayView.isHidden = timetableDisplayType != .day
      self?.calendarBarWeekView.isHidden = timetableDisplayType != .week
    }
  }
  
  // MARK: - Private methods
  
  private func showDaySchedule() {
    dayScheduleView.isHidden = false
    calendarBarDayView.isHidden = false
    calendarBarWeekView.isHidden = true
    weekScheduleView.isHidden = true
  }
  
  private func showWeekdaySchedule() {
    dayScheduleView.isHidden = true
    calendarBarDayView.isHidden = true
    weekScheduleView.isHidden = false
    calendarBarWeekView.isHidden = false
  }
  
  private func showDatePicker(defaultDate: Date, defaultWeek: Int) {
    let datePickerController = TimetableDatePickerViewController()
    datePickerController.modalPresentationStyle = .overCurrentContext
    datePickerController.defaultDate = defaultDate
    datePickerController.defaultWeek = defaultWeek
    datePickerController.onDidSelectDate = { [weak self] date in
      self?.viewModel.didSelectDate(date)
    }
    datePickerController.onDidSelectWeek = { [weak self] week in
      self?.viewModel.didSelectWeek(week)
    }
    present(datePickerController, animated: false)
  }
  
  private func updateDayScheduleView() {
    guard let viewModel = viewModel.timetableDayViewModel else { return }
    dayScheduleView.configure(with: viewModel)
  }
  
  private func updateWeekScheduleView() {
    guard let viewModel = viewModel.timetableWeekViewModel else { return }
    weekScheduleView.configure(with: viewModel)
  }
  
}
