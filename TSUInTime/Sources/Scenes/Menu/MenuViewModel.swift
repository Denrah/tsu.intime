//
//  MenuViewModel.swift
//  TSUInTime
//

import UIKit

private extension Constants {
  static let feedbackRequestDelay = 30
}

// MARK: - Protocol
protocol MenuViewModelDelegate: AnyObject {
  func menuViewModel(_ viewModel: MenuViewModel, didSelect option: MenuOption)
  func menuViewModelDidRequestToShowFeedbackScreen(_ viewModel: MenuViewModel)
}

// MARK: - ViewModel
class MenuViewModel {
  typealias Dependencies = HasDataStore
  
  weak var delegate: MenuViewModelDelegate?

  var onDidRequestToShowDeleteDataAlert: (() -> Void)?
  
  var shouldRequestFeedback: Bool {
    if let nextRequestDate = dependencies.dataStore.nextFeedbackRequestDate {
      return !dependencies.dataStore.hasSentFeedback && nextRequestDate < Date()
    } else {
      return !dependencies.dataStore.hasSentFeedback
    }
  }
  
  private let menuDict = [
    [MenuOption.favorites, MenuOption.campusMap],
    [MenuOption.group, MenuOption.professors, MenuOption.audience],
    [MenuOption.exportToCalendar, MenuOption.feedback, MenuOption.settings],
    [MenuOption.deleteData]
  ]
  
  private let dependencies: Dependencies
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
  }
  
  func numberOfSections() -> Int {
    return menuDict.count
  }
  
  func numberOfRows(at section: Int) -> Int {
    return menuDict[section].count
  }
  
  func heightForHeader(at section: Int) -> CGFloat {
    return section == 0 ? 16 : 32
  }
  
  func heightForFooter() -> CGFloat {
    return 0
  }
  
  func didSelectOption(option: MenuOption) {
    if option == .deleteData {
      onDidRequestToShowDeleteDataAlert?()
      return
    }
    
    delegate?.menuViewModel(self, didSelect: option)
  }

  func deleteData() {
    delegate?.menuViewModel(self, didSelect: .deleteData)
  }
  
  func cellSectionDefinition(indexPath: IndexPath) -> MenuCellViewModel {
    return MenuCellViewModel(option: menuDict[indexPath.section][indexPath.row])
  }
  
  func didTapLike() {
    dependencies.dataStore.hasSentFeedback = true
  }
  
  func didTapDislike() {
    dependencies.dataStore.hasSentFeedback = true
    delegate?.menuViewModelDidRequestToShowFeedbackScreen(self)
  }
  
  func didCloseFeedbackRequest() {
    let nextRequestDate = Calendar.current.date(byAdding: .day,
                                                value: Constants.feedbackRequestDelay, to: Date())
    dependencies.dataStore.nextFeedbackRequestDate = nextRequestDate
  }
}
