//
//  MenuCoordinator.swift
//  TSUInTime
//

import Foundation
import UIKit

protocol MenuCoordinatorDelegate: AnyObject {
  func menuCoordinatorDidRequestToSignOut(_ coordinator: MenuCoordinator)
}

class MenuCoordinator: Coordinator {
  // MARK: - Properties
  
  weak var delegate: MenuCoordinatorDelegate?
  
  let navigationController: NavigationController
  let appDependency: AppDependency
  
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  // MARK: - Init
  
  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }
  
  // MARK: - Navigation
  
  func start(animated: Bool) {
    showMenuScreen(animated: animated)
  }
  
  private func showMenuScreen(animated: Bool) {
    let viewModel = MenuViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = MenuViewController(viewModel: viewModel)
    viewController.title = R.string.menu.title()
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  private func showFavorites() {
    appDependency.analyticsService.logEvent(.openFavsMenu)
    show(FavoritesCoordinator.self, animated: true)
  }
  
  private func showSearch(type: SearchType) {
    let configuration = SearchCoordinatorConfiguration(searchType: type)
    show(SearchCoordinator.self, configuration: configuration, animated: true)
  }
  
  private func showMap() {
    appDependency.analyticsService.logEvent(.openCampusMap)
    show(MapCoordinator.self, animated: true)
  }
  
  private func showSettings() {
    show(SettingsCoordinator.self, animated: true)
  }
  
  private func showCalendarExport() {
    let navController = NavigationController()
    navController.configureDefaultNavigationBarAppearance()

    let viewModel = CalendarExportViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = CalendarExportViewController(viewModel: viewModel)
    viewController.title = R.string.menu.calendarExportScreenTitle()

    let closeButton = UIBarButtonItem(title: R.string.common.cancel(), style: .plain, target: self, action: #selector(closeModal))
    closeButton.setTitleTextAttributes([.font: UIFont.body ?? .systemFont(ofSize: 16)], for: .normal)
    viewController.navigationItem.rightBarButtonItem = closeButton

    navController.pushViewController(viewController, animated: false)
    navigationController.present(navController, animated: true)
  }
  
  private func showFeedbackScreen() {
    show(FeedbackCoordinator.self, animated: true)
  }

  // MARK: - Private methods
  
  @objc private func closeModal() {
    navigationController.dismiss(animated: true)
  }
}

// MARK: - MenuViewModelDelegate
extension MenuCoordinator: MenuViewModelDelegate {
  func menuViewModel(_ viewModel: MenuViewModel, didSelect option: MenuOption) {
    switch option {
    case .favorites:
      showFavorites()
    case .campusMap:
      showMap()
    case .group:
      showSearch(type: .faculties)
    case .professors:
      showSearch(type: .professors)
    case .audience:
      showSearch(type: .buildings)
    case .deleteData:
      delegate?.menuCoordinatorDidRequestToSignOut(self)
    case .feedback:
      showFeedbackScreen()
    case .settings:
      showSettings()
    case .exportToCalendar:
      showCalendarExport()
    }
  }
  
  func menuViewModelDidRequestToShowFeedbackScreen(_ viewModel: MenuViewModel) {
    showFeedbackScreen()
  }
}

// MARK: - CalendarExportViewModelDelegate

extension MenuCoordinator: CalendarExportViewModelDelegate {
  func calendarExportViewModelDidFinish(_ viewModel: CalendarExportViewModel) {
    closeModal()
  }
}
