//
//  MenuOption.swift
//  TSUInTime
//

import UIKit

// MARK: - Enum
enum MenuOption: CaseIterable {
  case favorites, campusMap, group, professors, audience,
       exportToCalendar, feedback, settings, deleteData

  var imageOption: UIImage? {
    switch self {
    case .favorites:
      return R.image.favoritesLike()
    case .campusMap:
      return R.image.mapCampus()
    case .group:
      return R.image.personGroup()
    case .professors:
      return R.image.professor()
    case .audience:
      return R.image.audience()
    case .exportToCalendar:
      return R.image.exportToCalendar()
    case .feedback:
      return R.image.feedback()
    case .settings:
      return R.image.settings()
    case .deleteData:
      return R.image.deleteData()
    }
  }
  
  var title: String {
    switch self {
    case .favorites:
      return R.string.menu.optionCategoryFavorites()
    case .campusMap:
      return R.string.menu.optionCategoryCampus()
    case .group:
      return R.string.menu.optionCategoryGroup()
    case .professors:
      return R.string.menu.optionCategoryProfessors()
    case .audience:
      return R.string.menu.optionCategoryAudience()
    case .exportToCalendar:
      return R.string.menu.optionCategoryCalendar()
    case .feedback:
      return R.string.menu.optionCategoryFeedback()
    case .settings:
      return R.string.menu.optionCategorySettings()
    case .deleteData:
      return R.string.menu.optionCategoryDelete()
    }
  }
  
  var textColor: UIColor {
    switch self {
    case .deleteData:
      return UIColor.accentRed
    default:
      return UIColor.baseBlack
    }
  }
  
  var subtitleText: String? {
    switch self {
    case .deleteData:
      return R.string.menu.detailMenuLabel()
    default:
      return nil
    }
  }
  
  var hasArrow: Bool {
    switch self {
    case .deleteData:
      return false
    default:
      return true
    }
  }
}
