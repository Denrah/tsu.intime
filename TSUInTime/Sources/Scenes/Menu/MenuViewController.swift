//
//  MenuViewController.swift
//  TSUInTime
//

import UIKit
import StoreKit

private extension Constants {
  static let bottomPadding: CGFloat = 48
}

class MenuViewController: BaseViewController {
  // MARK: - Properties
  
  private let viewModel: MenuViewModel
  
  private lazy var tableView: UITableView = {
    let tableView = UITableView(frame: .zero, style: .grouped)
    tableView.register(MenuCell.self, forCellReuseIdentifier: MenuCell.reuseIdentifier)
    tableView.separatorStyle = .none
    tableView.backgroundColor = .clear
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 48
    tableView.alwaysBounceVertical = false
    tableView.showsVerticalScrollIndicator = false
    tableView.contentInset.bottom = Constants.bottomPadding
    return tableView
  }()
  
  private let feedbackRequestView = FeedbackRequestView()
  
  // MARK: - Init
  
  init(viewModel: MenuViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }

  // MARK: - Setups
  
  private func setup() {
    setupTableView()
    
    if viewModel.shouldRequestFeedback {
      setupFeedbackRequestView()
    }
  }
  
  private func setupTableView() {
    view.addSubview(tableView)
    tableView.delegate = self
    tableView.dataSource = self
    
    tableView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupFeedbackRequestView() {
    view.addSubview(feedbackRequestView)
    feedbackRequestView.onDidLayout = { [weak self] frame in
      self?.tableView.contentInset.bottom = frame.height + Constants.bottomPadding
    }
    feedbackRequestView.onDidTapClose = { [weak self] in
      self?.viewModel.didCloseFeedbackRequest()
      self?.hideFeedbackRequestView()
    }
    feedbackRequestView.onDidTapLike = { [weak self] in
      self?.requestReview()
      self?.hideFeedbackRequestView()
    }
    feedbackRequestView.onDidTapDislike = { [weak self] in
      self?.viewModel.didTapDislike()
      self?.hideFeedbackRequestView()
    }
    feedbackRequestView.snp.makeConstraints { make in
      make.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
    }
  }

  // MARK: - Bind

  private func bindToViewModel() {
    viewModel.onDidRequestToShowDeleteDataAlert = { [weak self] in
      self?.showDeleteDataAlert()
    }
  }

  // MARK: - Private methods

  private func showDeleteDataAlert() {
    let alertController = UIAlertController(title: R.string.menu.deleteDataAlertTitle(),
                                            message: R.string.menu.deleteDataAlertDescription(),
                                            preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: R.string.common.delete(), style: .destructive) { [weak self] _ in
      self?.viewModel.deleteData()
    })
    alertController.addAction(UIAlertAction(title: R.string.common.cancel(), style: .cancel, handler: nil))
    present(alertController, animated: true)
  }
  
  private func requestReview() {
    viewModel.didTapLike()
    
    if #available(iOS 14.0, *) {
      guard let windowScene = view.window?.windowScene else { return }
      SKStoreReviewController.requestReview(in: windowScene)
    } else {
      SKStoreReviewController.requestReview()
    }
  }
  
  private func hideFeedbackRequestView() {
    UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut) { [weak self] in
      self?.tableView.contentInset.bottom = Constants.bottomPadding
      let animationOffset = (self?.view.frame.height ?? 0) - (self?.feedbackRequestView.frame.minY ?? 0)
      self?.feedbackRequestView.transform = CGAffineTransform(translationX: 0, y: animationOffset)
    } completion: { [weak self] _ in
      self?.feedbackRequestView.removeFromSuperview()
    }
  }
}

// MARK: - UITableViewDataSource

extension MenuViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfSections()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows(at: section)
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.reuseIdentifier, for: indexPath) as? MenuCell
    guard let safeCell = cell else { return UITableViewCell() }
    safeCell.configure(with: viewModel.cellSectionDefinition(indexPath: indexPath))
    return safeCell
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return UIView()
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return viewModel.heightForHeader(at: section)
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return viewModel.heightForFooter()
  }
}

// MARK: - UITableViewDelegate

extension MenuViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.didSelectOption(option: viewModel.cellSectionDefinition(indexPath: indexPath).option)
  }
}
