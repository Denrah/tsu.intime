//
//  MenuCellViewModel.swift
//  TSUInTime
//

import UIKit

struct MenuCellViewModel {
  let option: MenuOption
  
  var icon: UIImage?
  var title: String
  var textColor: UIColor
  var hasDisclosureIndicator: Bool
  var hasSubtitle: Bool = false
  var subtitle: String?
  
  init(option: MenuOption) {
    self.option = option
    
    self.title = option.title
    self.icon = option.imageOption
    self.textColor = option.textColor
    self.hasDisclosureIndicator = option.hasArrow
    self.subtitle = option.subtitleText
    if self.subtitle != nil {
      self.hasSubtitle = true
    }
  }
}
    
