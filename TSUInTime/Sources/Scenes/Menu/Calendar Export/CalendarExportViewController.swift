//
//  CalendarExportViewController.swift
//  TSUInTime
//

import UIKit
import SnapKit

private extension Constants {
  static let textFieldPadding = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 16)
}

class CalendarExportViewController: BaseViewController, KeyboardVisibilityHandling,
                                    ErrorHandling, GoToSettingsAlertShowing {
  // MARK: - Properties
  
  var keyboardWillShowObserver: NSObjectProtocol?
  var keyboardWillHideObserver: NSObjectProtocol?
  var keyboardWillChangeFrameObserver: NSObjectProtocol?
  
  private let scrollView = UIScrollView()
  private let containerView = UIView()
  private let radioButtonGroupView = RadioButtonGroupView<CalendarExportType>()
  private let customStartDateTextField = TextField(padding: Constants.textFieldPadding)
  private let customEndDateTextField = TextField(padding: Constants.textFieldPadding)
  private let startDateFieldLeftView = CalendarExportFieldLeftView()
  private let endDateFieldLeftView = CalendarExportFieldLeftView()
  private let exportButton = CommonButton(style: .default)
  private let startDatePicker = UIDatePicker()
  private let endDatePicker = UIDatePicker()
  private let keyboardToolbar = DefaultKeyboardToolbar()
  
  private let viewModel: CalendarExportViewModel
  
  private var scrollViewBottomConstraint: Constraint?
  private var containerViewHeightConstraint: Constraint?
  
  // MARK: - Init
  
  init(viewModel: CalendarExportViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    subscribeForKeyboardStateUpdates()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    unsubscribeFromKeyboardStateUpdates()
  }
  
  // MARK: - Public methods
  
  func keyboardWillHide(keyboardInfo: KeyboardInfo) {
    scrollViewBottomConstraint?.update(offset: 0)
    containerViewHeightConstraint?.update(offset: 0)
    
    UIView.animate(withDuration: keyboardInfo.animationDuration, delay: 0,
                   options: keyboardInfo.animationCurve.option, animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  func keyboardWillShow(keyboardInfo: KeyboardInfo) {
    scrollViewBottomConstraint?.update(offset: -keyboardInfo.keyboardHeight + view.safeAreaInsets.bottom)
    containerViewHeightConstraint?.update(offset: -keyboardInfo.keyboardHeight + view.safeAreaInsets.bottom)
    
    UIView.animate(withDuration: keyboardInfo.animationDuration, delay: 0,
                   options: keyboardInfo.animationCurve.option, animations: {
      self.view.layoutIfNeeded()
      self.scrollToActiveTextField()
    }, completion: nil)
  }
  
  // MARK: - Actions
  
  @objc private func handleDatePickerValueChanged(datePicker: UIDatePicker) {
    switch datePicker {
    case startDatePicker:
      endDatePicker.minimumDate = datePicker.date
      viewModel.setSelectedStartDate(datePicker.date)
    case endDatePicker:
      startDatePicker.maximumDate = datePicker.date
      viewModel.setSelectedEndDate(datePicker.date)
    default:
      break
    }
  }
  
  @objc private func handleViewTap() {
    view.endEditing(true)
  }
  
  @objc private func handleExportButtonTap() {
    viewModel.export()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupScrollView()
    setupContainerView()
    setupRadioButtonGroupView()
    setupCustomStartDateTextField()
    setupCustomEndDateTextField()
    setupTextFieldsTitles()
    setupExportButton()
    
    if #available(iOS 13.4, *) {
      startDatePicker.preferredDatePickerStyle = .wheels
      endDatePicker.preferredDatePickerStyle = .wheels
    }
    
    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleViewTap)))
    keyboardToolbar.onDidTapDone = { [weak self] in
      self?.view.endEditing(true)
    }
  }
  
  private func setupScrollView() {
    view.addSubview(scrollView)
    scrollView.showsVerticalScrollIndicator = false
    scrollView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
      scrollViewBottomConstraint = make.bottom.equalToSuperview().constraint
    }
  }
  
  private func setupContainerView() {
    scrollView.addSubview(containerView)
    containerView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.width.equalTo(view.snp.width)
      containerViewHeightConstraint = make.height.greaterThanOrEqualTo(view.safeAreaLayoutGuide.snp.height).constraint
    }
  }
  
  private func setupRadioButtonGroupView() {
    containerView.addSubview(radioButtonGroupView)
    radioButtonGroupView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
    }
    
    radioButtonGroupView.configure(with: CalendarExportType.allCases, selectedItem: viewModel.selectedExportType)
    radioButtonGroupView.onDidSelectOption = { [weak self] option in
      self?.radioButtonGroupDidSelectOption(option)
    }
  }
  
  private func setupCustomStartDateTextField() {
    containerView.addSubview(customStartDateTextField)
    customStartDateTextField.alpha = 0
    customStartDateTextField.leftViewMode = .always
    customStartDateTextField.inputView = startDatePicker
    customStartDateTextField.canPerformAction = false
    customStartDateTextField.fieldTintColor = .clear
    customStartDateTextField.accessoryView = keyboardToolbar
    customStartDateTextField.delegate = self
    customStartDateTextField.snp.makeConstraints { make in
      make.top.equalTo(radioButtonGroupView.snp.bottom).offset(4)
      make.leading.trailing.equalToSuperview().inset(16)
    }

    startDatePicker.datePickerMode = .date
    startDatePicker.addTarget(self, action: #selector(handleDatePickerValueChanged(datePicker:)), for: .valueChanged)
  }
  
  private func setupCustomEndDateTextField() {
    containerView.addSubview(customEndDateTextField)
    customEndDateTextField.alpha = 0
    customEndDateTextField.leftViewMode = .always
    customEndDateTextField.inputView = endDatePicker
    customEndDateTextField.canPerformAction = false
    customEndDateTextField.fieldTintColor = .clear
    customEndDateTextField.accessoryView = keyboardToolbar
    customEndDateTextField.delegate = self
    customEndDateTextField.snp.makeConstraints { make in
      make.top.equalTo(customStartDateTextField.snp.bottom).offset(8)
      make.leading.trailing.equalToSuperview().inset(16)
    }

    endDatePicker.datePickerMode = .date
    endDatePicker.addTarget(self, action: #selector(handleDatePickerValueChanged(datePicker:)), for: .valueChanged)
  }

  private func setupTextFieldsTitles() {
    let startDateTitle = R.string.menu.calendarExportStartDateTitle()
    let endDateTitle = R.string.menu.calendarExportEndDateTitle()
    let font = UIFont.body ?? .systemFont(ofSize: 16)
    let width = max(startDateTitle.width(withConstrainedHeight: 40, font: font),
                    endDateTitle.width(withConstrainedHeight: 40, font: font))

    startDateFieldLeftView.title = startDateTitle
    startDateFieldLeftView.color = .shade3
    startDateFieldLeftView.update(for: width)
    customStartDateTextField.leftView = startDateFieldLeftView

    endDateFieldLeftView.title = endDateTitle
    endDateFieldLeftView.color = .shade3
    endDateFieldLeftView.update(for: width)
    customEndDateTextField.leftView = endDateFieldLeftView
  }
  
  private func setupExportButton() {
    containerView.addSubview(exportButton)
    exportButton.setTitle(R.string.menu.calendarExportButtonTitle(), for: .normal)
    exportButton.addTarget(self, action: #selector(handleExportButtonTap), for: .touchUpInside)
    exportButton.snp.makeConstraints { make in
      make.leading.trailing.bottom.equalToSuperview().inset(16)
      make.top.greaterThanOrEqualTo(customEndDateTextField.snp.bottom).offset(24)
    }
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidStartRequest = { [weak self] in
      self?.exportButton.startAnimating()
    }
    viewModel.onDidFinishRequest = { [weak self] in
      self?.exportButton.stopAnimating()
    }
    viewModel.onDidUpdate = { [weak self] in
      self?.update()
    }
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error)
    }
    viewModel.onDidExportTimetable = { [weak self] in
      self?.showBanner(title: nil, subtitle: R.string.menu.calendarExportSucceedBannerTitle(),
                       style: .success)
    }
    viewModel.onDidRequestToShowNoLessonsBanner = { [weak self] in
      self?.showBanner(title: nil, subtitle: R.string.errors.nothingToExportToCalendarErrorText(),
                       style: .warning)
    }
    viewModel.onDidRequestToShowExportRestrictedAlert = { [weak self] in
      self?.showCalendarExportRestrictedAlert()
    }
  }
  
  // MARK: - Private methods
  
  private func update() {
    exportButton.isEnabled = viewModel.isExportButtonEnabled
    customStartDateTextField.text = viewModel.formattedStartDate
    customEndDateTextField.text = viewModel.formattedEndDate
  }
  
  private func radioButtonGroupDidSelectOption(_ option: CalendarExportType) {
    viewModel.setSelectedExportType(to: option)
    
    if option != .custom {
      view.endEditing(true)
    }
    
    UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
      self.customStartDateTextField.alpha = option == .custom ? 1 : 0
      self.customEndDateTextField.alpha = option == .custom ? 1 : 0
    }, completion: nil)
  }
  
  private func scrollToActiveTextField() {
    guard let textField = [customStartDateTextField, customEndDateTextField]
            .first(where: \.isFirstResponder) else { return }
    
    let offsetY = scrollView.convert(textField.frame, from: containerView).origin.y - 16
    let targetOffsetY = max(min(offsetY, scrollView.contentSize.height - scrollView.frame.height), 0)
    scrollView.setContentOffset(CGPoint(x: 0, y: targetOffsetY), animated: true)
  }

  private func showCalendarExportRestrictedAlert() {
    showGoToSettingsAlert(title: R.string.menu.calendarExportRestrictedAlertTitle(),
                          description: R.string.menu.calendarExportRestrictedAlertDescription(),
                          cancelButtonTitle: R.string.common.cancel())
  }
}

// MARK: - TextFieldDelegate

extension CalendarExportViewController: TextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: TextField) {
    textField.backgroundColor = .accentLight
    textField.inputView?.backgroundColor = .baseWhite
    
    switch textField {
    case customStartDateTextField:
      startDateFieldLeftView.color = .accent
      handleDatePickerValueChanged(datePicker: startDatePicker)
      if let selectedStartDate = viewModel.selectedStartDate {
        // Workaround to fix datepicker update problems
        startDatePicker.setDate(Date(), animated: false)
        startDatePicker.setDate(selectedStartDate, animated: false)
      }
    case customEndDateTextField:
      endDateFieldLeftView.color = .accent
      handleDatePickerValueChanged(datePicker: endDatePicker)
      if let selectedEndDate = viewModel.selectedEndDate {
        // Workaround to fix datepicker update problems
        endDatePicker.setDate(Date(), animated: false)
        endDatePicker.setDate(selectedEndDate, animated: false)
      }
    default:
      return
    }
  }
  
  func textFieldDidEndEditing(_ textField: TextField) {
    textField.backgroundColor = .shade1
    
    switch textField {
    case customStartDateTextField:
      startDateFieldLeftView.color = .shade3
    case customEndDateTextField:
      endDateFieldLeftView.color = .shade3
    default:
      return
    }
  }
  
  func textFieldDidChangeSelection(_ textField: TextField) {
    textField.selectedTextRange = nil
  }
}
