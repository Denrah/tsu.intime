//
//  CalendarExportFieldLeftView.swift
//  TSUInTime
//

import UIKit

class CalendarExportFieldLeftView: UIView {
  var title: String? {
    get {
      titleLabel.text
    }
    set {
      titleLabel.text = newValue
    }
  }

  var color: UIColor {
    get {
      titleLabel.textColor
    }
    set {
      titleLabel.textColor = newValue
    }
  }

  private let titleLabel = Label(textStyle: .body)

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  func update(for width: CGFloat) {
    titleLabel.snp.remakeConstraints { make in
      make.width.equalTo(width)
      make.top.bottom.trailing.equalToSuperview()
      make.leading.equalToSuperview().inset(16)
    }
  }

  private func setup() {
    addSubview(titleLabel)
    titleLabel.snp.makeConstraints { make in
      make.width.equalTo(24)
      make.top.bottom.trailing.equalToSuperview()
      make.leading.equalToSuperview().inset(16)
    }
  }
}
