//
//  CalendarExportViewModel.swift
//  TSUInTime
//

import Foundation

protocol CalendarExportViewModelDelegate: AnyObject {
  func calendarExportViewModelDidFinish(_ viewModel: CalendarExportViewModel)
}

class CalendarExportViewModel {
  typealias Dependencies = HasCalendarService & HasAnalyticsService

  // MARK: - Properties

  weak var delegate: CalendarExportViewModelDelegate?

  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidUpdate: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  var onDidExportTimetable: (() -> Void)?
  var onDidRequestToShowNoLessonsBanner: (() -> Void)?
  var onDidRequestToShowExportRestrictedAlert: (() -> Void)?

  var isExportButtonEnabled: Bool {
    selectedExportType != .custom || (selectedStartDate != nil && selectedEndDate != nil)
  }
  
  var formattedStartDate: String? {
    guard let selectedStartDate = selectedStartDate else { return nil }
    return DateFormatter.dayMonthYearDisplay.string(from: selectedStartDate)
  }
  
  var formattedEndDate: String? {
    guard let selectedEndDate = selectedEndDate else { return nil }
    return DateFormatter.dayMonthYearDisplay.string(from: selectedEndDate)
  }

  private(set) var selectedExportType: CalendarExportType = .week
  private(set) var selectedStartDate: Date?
  private(set) var selectedEndDate: Date?

  private let dependencies: Dependencies

  // MARK: - Init

  init(dependencies: Dependencies) {
    self.dependencies = dependencies
  }

  // MARK: - Public methods

  func export() {
    dependencies.analyticsService.logEvent(.calendarExport)
    
    guard let startDate = calculateStartDate(), let endDate = calculateEndDate() else { return }

    onDidStartRequest?()

    dependencies.calendarService.exportToCalendar(startDate: startDate, endDate: endDate).ensure {
      self.onDidFinishRequest?()
    }.done { _ in
      self.onDidExportTimetable?()
      self.delegate?.calendarExportViewModelDidFinish(self)
    }.catch { error in
      switch error as? CalendarServiceError {
      case .nothingToExport:
        self.onDidRequestToShowNoLessonsBanner?()
      case .accessIsDenied:
        self.onDidRequestToShowExportRestrictedAlert?()
      default:
        self.onDidReceiveError?(error)
      }
    }
  }

  func setSelectedExportType(to exportType: CalendarExportType) {
    selectedExportType = exportType
    onDidUpdate?()
  }

  func setSelectedStartDate(_ date: Date) {
    selectedStartDate = date
    onDidUpdate?()
  }

  func setSelectedEndDate(_ date: Date) {
    selectedEndDate = date
    onDidUpdate?()
  }

  // MARK: - Private methods

  private func calculateStartDate() -> Date? {
    switch selectedExportType {
    case .week:
      return Date().startOfWeek()
    case .month:
      return Date().startOfMonth()
    case .custom:
      return selectedStartDate
    }
  }

  private func calculateEndDate() -> Date? {
    switch selectedExportType {
    case .week:
      return Date().endOfWeek()
    case .month:
      return Date().endOfMonth()
    case .custom:
      return selectedEndDate
    }
  }
}
