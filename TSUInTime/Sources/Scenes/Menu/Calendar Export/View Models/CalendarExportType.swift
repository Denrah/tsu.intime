//
//  CalendarExportType.swift
//  TSUInTime
//

import Foundation

enum CalendarExportType: RadioButtonItemType, CaseIterable {
  case week, month, custom

  var title: String? {
    switch self {
    case .week:
      return R.string.menu.calendarExportWeekOptionTitle()
    case .month:
      return R.string.menu.calendarExportMonthOptionTitle()
    case .custom:
      return R.string.menu.calendarExportCustomOptionTitle()
    }
  }
}
