//
//  MenuCell.swift
//  TSUInTime
//

import UIKit

class MenuCell: UITableViewCell {
  // MARK: - Properties
  
  private let menuImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.clipsToBounds = true
    return imageView
  }()

  private let stackView = UIStackView()
  private let menuLabel = Label(textStyle: .body)
  private let subtitleMenuLabel = Label(textStyle: .footnote)
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    setupCell()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func configure(with viewModel: MenuCellViewModel) {
    menuImageView.image = viewModel.icon
    menuLabel.text = viewModel.title
    subtitleMenuLabel.text = viewModel.subtitle
    menuLabel.textColor = viewModel.textColor
    subtitleMenuLabel.isHidden = !viewModel.hasSubtitle
    accessoryView?.isHidden = !viewModel.hasDisclosureIndicator
  }
  
  private func setupCell() {
    contentView.addSubview(menuImageView)
    menuImageView.snp.makeConstraints { make in
      make.leading.equalToSuperview().inset(16)
      make.top.equalToSuperview().inset(12)
      make.height.width.equalTo(24)
    }
    contentView.addSubview(stackView)
    stackView.axis = .vertical
    stackView.spacing = 4
    stackView.snp.makeConstraints { make in
      make.leading.equalTo(menuImageView.snp.trailing).offset(16)
      make.trailing.equalToSuperview().inset(16)
      make.top.bottom.equalToSuperview().inset(12)
    }

    stackView.addArrangedSubview(menuLabel)
    menuLabel.textColor = .baseBlack

    stackView.addArrangedSubview(subtitleMenuLabel)
    subtitleMenuLabel.textColor = .shade3
    
    accessoryView = UIImageView(image: R.image.disclosureIndicator()?.withTintColor(.accent))
  }
}
