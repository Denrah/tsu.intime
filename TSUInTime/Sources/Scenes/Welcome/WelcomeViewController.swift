//
//  WelcomeViewController.swift
//  TSUInTime
//

import UIKit

class WelcomeViewController: BaseViewController, NavigationBarHiding {
  // MARK: - Properties

  private let titleLabel = Label(textStyle: .header1)
  private let descriptionLabel = Label(textStyle: .body)
  private let tsuImageView = UIImageView()
  private let startButton = CommonButton()

  private let viewModel: WelcomeViewModel

  // MARK: - Init

  init(viewModel: WelcomeViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  // MARK: - Actions

  @objc private func didTapStartButton() {
    viewModel.didTapStartButton()
  }

  // MARK: - Setup

  private func setup() {
    setupTitleLabel()
    setupDescriptionLabel()
    setupTsuImageView()
    setupStartButton()
  }

  private func setupTitleLabel() {
    view.addSubview(titleLabel)
    titleLabel.text = R.string.welcome.title()
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .accent
    titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    titleLabel.setContentHuggingPriority(.required, for: .vertical)
    titleLabel.snp.makeConstraints { make in
      make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(24)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupDescriptionLabel() {
    view.addSubview(descriptionLabel)
    descriptionLabel.text = R.string.welcome.description()
    descriptionLabel.numberOfLines = 0
    descriptionLabel.textColor = .baseBlack
    descriptionLabel.setContentCompressionResistancePriority(.required, for: .vertical)
    descriptionLabel.setContentHuggingPriority(.required, for: .vertical)
    descriptionLabel.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupTsuImageView() {
    view.addSubview(tsuImageView)
    tsuImageView.image = R.image.tsuWelcome()
    tsuImageView.contentMode = .scaleAspectFit
    tsuImageView.snp.makeConstraints { make in
      make.top.equalTo(descriptionLabel.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(24)
    }
  }

  private func setupStartButton() {
    view.addSubview(startButton)
    startButton.setTitle(R.string.welcome.startButtonTitle(), for: .normal)
    startButton.setContentCompressionResistancePriority(.required, for: .vertical)
    startButton.setContentHuggingPriority(.required, for: .vertical)
    startButton.addTarget(self, action: #selector(didTapStartButton), for: .touchUpInside)
    startButton.snp.makeConstraints { make in
      make.top.equalTo(tsuImageView.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(16)
    }
  }
}
