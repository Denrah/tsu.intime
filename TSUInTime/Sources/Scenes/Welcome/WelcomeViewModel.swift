//
//  WelcomeViewModel.swift
//  TSUInTime
//

import Foundation

protocol WelcomeViewModelDelegate: AnyObject {
  func welcomeViewModelDidRequestToShowOnboarding(_ viewModel: WelcomeViewModel)
}

class WelcomeViewModel {
  weak var delegate: WelcomeViewModelDelegate?

  func didTapStartButton() {
    delegate?.welcomeViewModelDidRequestToShowOnboarding(self)
  }
}
