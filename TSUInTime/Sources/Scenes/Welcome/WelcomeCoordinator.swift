//
//  WelcomeCoordinator.swift
//  TSUInTime
//

import Foundation

protocol WelcomeCoordinatorDelegate: AnyObject {
  func welcomeCoordinatorDidFinish(_ coordinator: WelcomeCoordinator)
}

class WelcomeCoordinator: Coordinator {
  // MARK: - Properties
  
  weak var delegate: WelcomeCoordinatorDelegate?

  let navigationController: NavigationController
  let appDependency: AppDependency
  
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?

  // MARK: - Init

  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }

  // MARK: - Navigation

  func start(animated: Bool) {
    showWelcomeScreen(animated: animated)
  }

  private func showWelcomeScreen(animated: Bool) {
    let viewModel = WelcomeViewModel()
    viewModel.delegate = self
    let viewController = WelcomeViewController(viewModel: viewModel)
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }
}

// MARK: - WelcomeViewModelDelegate

extension WelcomeCoordinator: WelcomeViewModelDelegate {
  func welcomeViewModelDidRequestToShowOnboarding(_ viewModel: WelcomeViewModel) {
    let coordinator = show(OnboardingCoordinator.self, animated: true)
    coordinator.delegate = self
  }
}

// MARK: - OnboardingCoordinatorDelegate

extension WelcomeCoordinator: OnboardingCoordinatorDelegate {
  func onboardingCoordinatorDidFinish(_ coordonator: OnboardingCoordinator) {
    delegate?.welcomeCoordinatorDidFinish(self)
    onDidFinish?()
  }
}
