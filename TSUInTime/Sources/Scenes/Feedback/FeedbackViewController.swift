//
//  FeedbackViewController.swift
//  TSUInTime
//

import SnapKit
import UIKit

class FeedbackViewController: BaseViewController, ErrorHandling, BannerShowing {
  // MARK: - Properties
  
  var keyboardWillShowObserver: NSObjectProtocol?
  var keyboardWillHideObserver: NSObjectProtocol?
  var keyboardWillChangeFrameObserver: NSObjectProtocol?
  
  private let feedbackTextView = TextView()
  private let symbolsCounterLabel = Label(textStyle: .footnote)
  private let sendButton = CommonButton(style: .default)
  
  private var sendButtonBottomConstraint: Constraint?
  
  private let viewModel: FeedbackViewModel
  
  // MARK: - Init
  
  init(viewModel: FeedbackViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    subscribeForKeyboardStateUpdates()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    unsubscribeFromKeyboardStateUpdates()
  }
  
  // MARK: - Actions
  
  @objc private func handleSendButtonTap() {
    viewModel.send()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupFeedbackTextView()
    setupSymbolsCounterLabel()
    setupSendButton()
  }
  
  private func setupFeedbackTextView() {
    view.addSubview(feedbackTextView)
    feedbackTextView.placeholder = R.string.feedback.feedbackInputPlaceholderText()
    feedbackTextView.delegate = self
    feedbackTextView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupSymbolsCounterLabel() {
    view.addSubview(symbolsCounterLabel)
    symbolsCounterLabel.textColor = .shade3
    symbolsCounterLabel.text = viewModel.symbolsCounterText
    symbolsCounterLabel.snp.makeConstraints { make in
      make.top.equalTo(feedbackTextView.snp.bottom).offset(16)
      make.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupSendButton() {
    view.addSubview(sendButton)
    sendButton.setTitle(R.string.feedback.sendButtonTitle(), for: .normal)
    sendButton.isEnabled = false
    sendButton.addTarget(self, action: #selector(handleSendButtonTap), for: .touchUpInside)
    sendButton.snp.makeConstraints { make in
      make.top.equalTo(symbolsCounterLabel.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
      sendButtonBottomConstraint = make.bottom.equalTo(view.safeAreaLayoutGuide)
        .inset(16).priority(750).constraint
    }
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidUpdate = { [weak self] in
      guard let self = self else { return }
      self.symbolsCounterLabel.text = self.viewModel.symbolsCounterText
      self.sendButton.isEnabled = self.viewModel.isSendButtonEnabled
    }
    viewModel.onDidStartRequest = { [weak self] in
      self?.sendButton.startAnimating()
    }
    viewModel.onDidFinishRequest = { [weak self] in
      self?.sendButton.stopAnimating()
    }
    viewModel.onDidSendRequest = { [weak self] in
      self?.showBanner(title: nil, subtitle: R.string.feedback.feedbackSentBannerText(),
                       style: .success)
    }
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error)
    }
  }
}

// MARK: - TextViewDelegate

extension FeedbackViewController: TextViewDelegate {
  func textViewDidChange(_ textView: TextView) {
    viewModel.updateText(with: textView.text)
  }
  
  func textView(_ textView: TextView, shouldChangeTextIn range: NSRange,
                replacementText text: String) -> Bool {
    let resultString = (textView.text as NSString).replacingCharacters(in: range, with: text)
    return resultString.count <= viewModel.maxCharactersCount
  }
}

// MARK: - KeyboardVisibilityHandling

extension FeedbackViewController: KeyboardVisibilityHandling {
  func keyboardWillShow(keyboardInfo: KeyboardInfo) {
    sendButtonBottomConstraint?.update(inset: keyboardInfo.keyboardHeight
                                       + 16 - view.safeAreaInsets.bottom)
    
    UIView.animate(withDuration: keyboardInfo.animationDuration, delay: 0,
                   options: keyboardInfo.animationCurve.option, animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
  
  func keyboardWillHide(keyboardInfo: KeyboardInfo) {
    sendButtonBottomConstraint?.update(inset: 16)
    
    UIView.animate(withDuration: keyboardInfo.animationDuration, delay: 0,
                   options: keyboardInfo.animationCurve.option, animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
  }
}
