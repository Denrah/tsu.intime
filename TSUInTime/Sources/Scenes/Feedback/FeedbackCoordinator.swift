//
//  FeedbackCoordinator.swift
//  TSUInTime
//

import Foundation
import UIKit

class FeedbackCoordinator: NSObject, Coordinator {
  // MARK: - Properties

  let navigationController: NavigationController
  let appDependency: AppDependency

  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?

  // MARK: - Init

  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }

  // MARK: - Navigation

  func start(animated: Bool) {
    showFeedbackScreen(animated: animated)
  }

  private func showFeedbackScreen(animated: Bool) {
    let modalNavigationController = NavigationController()
    modalNavigationController.presentationController?.delegate = self
    
    let viewModel = FeedbackViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = FeedbackViewController(viewModel: viewModel)
    viewController.title = R.string.feedback.screenTitle()
    viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(title: R.string.common.cancel(),
                                                                       style: .plain, target: self,
                                                                       action: #selector(closeModal))
    
    modalNavigationController.pushViewController(viewController, animated: false)
    navigationController.present(modalNavigationController, animated: animated)
  }
  
  // MARK: - Private methods
  
  @objc private func closeModal() {
    navigationController.dismiss(animated: true)
    onDidFinish?()
  }
}

// MARK: - UIAdaptivePresentationControllerDelegate

extension FeedbackCoordinator: UIAdaptivePresentationControllerDelegate {
  func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
    onDidFinish?()
  }
}

// MARK: - FeedbackViewModelDelegate

extension FeedbackCoordinator: FeedbackViewModelDelegate {
  func feedbackViewModelDidFinish(_ viewModel: FeedbackViewModel) {
    closeModal()
  }
}
