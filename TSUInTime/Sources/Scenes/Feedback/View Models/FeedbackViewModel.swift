//
//  FeedbackViewModel.swift
//  TSUInTime
//

import Foundation
import UIKit

private extension Constants {
  static let maxCharactersCount = 500
}

protocol FeedbackViewModelDelegate: AnyObject {
  func feedbackViewModelDidFinish(_ viewModel: FeedbackViewModel)
}

class FeedbackViewModel {
  typealias Dependencies = HasFeedbackService
  
  // MARK: - Properties
  
  weak var delegate: FeedbackViewModelDelegate?
  
  var onDidUpdate: (() -> Void)?
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidSendRequest: (() -> Void)?
  var onDidReceiveError: ((_ error: Error) -> Void)?
  
  var maxCharactersCount: Int {
    Constants.maxCharactersCount
  }
  
  var symbolsCounterText: String {
    R.string.feedback.symbolsCounterText(text.count, maxCharactersCount)
  }
  
  var isSendButtonEnabled: Bool {
    !text.isEmpty
  }
  
  private var text: String = ""
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
  }
  
  // MARK: - Public methods
  
  func updateText(with text: String) {
    self.text = text
    onDidUpdate?()
  }
  
  func send() {
    let appVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    let parameters = FeedbackRequestParameters(text: text,
                                               osVersion: UIDevice.current.systemVersion,
                                               appVersion: appVersion,
                                               deviceModel: getDeviceModel(),
                                               platform: UIDevice.current.systemName)
    
    onDidStartRequest?()
    dependencies.feedbackService.sendFeedback(parameters: parameters).ensure {
      self.onDidFinishRequest?()
    }.done { _ in
      self.onDidSendRequest?()
      self.delegate?.feedbackViewModelDidFinish(self)
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func getDeviceModel() -> String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    return machineMirror.children.reduce("") { identifier, element in
      guard let value = element.value as? Int8, value != 0 else { return identifier }
      return identifier + String(UnicodeScalar(UInt8(value)))
    }
  }
}
