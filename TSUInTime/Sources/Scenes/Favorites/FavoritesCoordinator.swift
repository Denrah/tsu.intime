//
//  FavoritesCoordinator.swift
//  TSUInTime
//

import Foundation

class FavoritesCoordinator: Coordinator {
  // MARK: - Properties

  let navigationController: NavigationController
  let appDependency: AppDependency

  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?

  // MARK: - Init

  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }

  // MARK: - Navigation

  func start(animated: Bool) {
    showFavoritesScreen(animated: animated)
  }

  private func showFavoritesScreen(animated: Bool) {
    let viewModel = FavoritesViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = FavoritesViewController(viewModel: viewModel)
    viewController.title = R.string.favorites.screenTitle()
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }

  private func showTimetableScreen(timetableInfo: TimetableInfo) {
    let configuration = TimetableCoordinatorConfiguration(timetableInfo: timetableInfo)
    show(TimetableCoordinator.self, configuration: configuration, animated: true)
  }
}

// MARK: - FavoritesViewModelDelegate

extension FavoritesCoordinator: FavoritesViewModelDelegate {
  func favoritesViewModel(_ viewModel: FavoritesViewModel, didRequestToShowTimetableWithInfo timetableInfo: TimetableInfo) {
    showTimetableScreen(timetableInfo: timetableInfo)
  }
}
