//
//  FavoritesSectionHeaderViewModel.swift
//  TSUInTime
//

import CoreGraphics

class FavoritesSectionHeaderViewModel: TableHeaderFooterViewModel {
  var tableReuseIdentifier: String {
    FavoritesSectionHeaderView.reuseIdentifier
  }

  let title: String
  let topInset: CGFloat

  init(title: String, isFirstSection: Bool) {
    self.title = title
    self.topInset = isFirstSection ? 16 : 40
  }
}
