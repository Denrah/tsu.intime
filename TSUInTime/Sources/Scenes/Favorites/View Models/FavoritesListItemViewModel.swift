//
//  FavoritesListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol FavoritesListItemViewModelDelegate: AnyObject {
  func favoritesListItemViewModel(_ viewModel: FavoritesListItemViewModel,
                                  didSelectTimetableWithInfo timetableInfo: TimetableInfo)
}

class FavoritesListItemViewModel: SimpleListItemViewModel {
  weak var delegate: FavoritesListItemViewModelDelegate?

  var title: String {
    timetableInfo.timetableName ?? ""
  }

  var highlightedText: String?

  let timetableInfo: TimetableInfo

  init(timetableInfo: TimetableInfo) {
    self.timetableInfo = timetableInfo
  }

  func select() {
    delegate?.favoritesListItemViewModel(self, didSelectTimetableWithInfo: timetableInfo)
  }
}
