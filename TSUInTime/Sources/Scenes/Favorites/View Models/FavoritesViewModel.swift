//
//  FavoritesViewModel.swift
//  TSUInTime
//

import Foundation
import CoreGraphics

protocol FavoritesViewModelDelegate: AnyObject {
  func favoritesViewModel(_ viewModel: FavoritesViewModel, didRequestToShowTimetableWithInfo timetableInfo: TimetableInfo)
}

class FavoritesViewModel: TableViewModel {
  typealias Dependencies = HasFavoritesStorageService

  weak var delegate: FavoritesViewModelDelegate?

  var onDidUpdate: (() -> Void)?
  var onDidRequestToEndEditing: (() -> Void)?
  var onDidRequestToDeleteRow: ((_ indexPath: IndexPath) -> Void)?
  var onDidRequestToDeleteSection: ((_ index: Int) -> Void)?
  var onDidReceiveError: ((_ error: Error) -> Void)?

  var hasFavorites: Bool {
    !(sectionViewModels.isEmpty && searchText.isEmptyOrNil)
  }

  var shouldHideSearchPlaceholder: Bool {
    !sectionViewModels.isEmpty || searchText.isEmptyOrNil
  }

  let emptyStateViewModel: EmptyStateViewModel
  let searchPlaceholderViewModel: EmptyStateViewModel

  private(set) var sectionViewModels: [TableSectionViewModel] = []

  let dependencies: Dependencies

  private var searchText: String?

  init(dependencies: Dependencies) {
    self.dependencies = dependencies
    emptyStateViewModel = EmptyStateViewModel(image: R.image.favoritesEmptyStateImage(),
                                              imageSize: CGSize(width: 240, height: 200),
                                              title: R.string.favorites.emptyStateTitle(),
                                              subtitle: R.string.favorites.emptyStateSubtitle())
    searchPlaceholderViewModel = EmptyStateViewModel(image: nil, imageSize: nil,
                                                     title: R.string.search.searchPlaceholderTitle(),
                                                     subtitle: R.string.search.searchPlaceholderSubtitle(),
                                                     additionalText: R.string.search.searchPlaceholderAdditionalText(),
                                                     additionalTextFont: .header1)
    makeTableSections()
  }

  func update() {
    makeTableSections()
  }

  func updateSearchText(_ text: String?) {
    self.searchText = text
    makeTableSections()
  }

  func deleteTimetable(at indexPath: IndexPath) {
    let itemViewModel = sectionViewModels.element(at: indexPath.section)?.cellViewModels.element(at: indexPath.row)
    guard let timetableID = (itemViewModel as? FavoritesListItemViewModel)?.timetableInfo.timetableID else { return }

    do {
      try dependencies.favoritesStorageService.deleteTimetable(id: timetableID)
      sectionViewModels.element(at: indexPath.section)?.remove(at: indexPath.row)
      onDidRequestToDeleteRow?(indexPath)

      if sectionViewModels.element(at: indexPath.section)?.cellViewModels.isEmpty == true {
        sectionViewModels.remove(at: indexPath.section)
        onDidRequestToDeleteSection?(indexPath.section)
      }

      if sectionViewModels.isEmpty {
        onDidRequestToEndEditing?()
      }
    } catch let error {
      onDidReceiveError?(error)
    }
  }

  private func makeTableSections() {
    sectionViewModels.removeAll()

    let storedTimetables = (try? dependencies.favoritesStorageService.getTimetables()) ?? []

    TimetableType.allCases.forEach { type in
      var timetables = storedTimetables.filter { $0.timetableType == type }
        .sorted { ($0.timetableName ?? "") < ($1.timetableName ?? "") }

      if let searchText = searchText, !searchText.isEmpty {
        timetables = timetables.filter { $0.timetableName?.lowercased().contains(searchText.lowercased()) ?? false }
      }

      let itemViewModels = timetables.map { timetableInfo -> FavoritesListItemViewModel in
        let itemViewModel = FavoritesListItemViewModel(timetableInfo: timetableInfo)
        itemViewModel.highlightedText = searchText
        itemViewModel.delegate = self
        return itemViewModel
      }

      if !itemViewModels.isEmpty {
        let headerViewModel = FavoritesSectionHeaderViewModel(title: titleForTimetableType(type),
                                                              isFirstSection: sectionViewModels.isEmpty)
        let section = TableSectionViewModel(headerViewModel: headerViewModel)
        section.append(cellViewModels: itemViewModels)
        sectionViewModels.append(section)
      }
    }

    onDidUpdate?()
  }

  private func titleForTimetableType(_ type: TimetableType) -> String {
    switch type {
    case .group:
      return R.string.favorites.groupsSectionTitle()
    case .professor:
      return R.string.favorites.professorsSectionTitle()
    case .auditory:
      return R.string.favorites.auditoriesSectionTitle()
    }
  }
}

// MARK: - FavoritesListItemViewModelDelegate

extension FavoritesViewModel: FavoritesListItemViewModelDelegate {
  func favoritesListItemViewModel(_ viewModel: FavoritesListItemViewModel,
                                  didSelectTimetableWithInfo timetableInfo: TimetableInfo) {
    onDidRequestToEndEditing?()
    delegate?.favoritesViewModel(self, didRequestToShowTimetableWithInfo: timetableInfo)
  }
}
