//
//  FavoritesViewController.swift
//  TSUInTime
//

import UIKit

private extension Constants {
  static let additionalTableViewBottomInset: CGFloat = 88
}

class FavoritesViewController: BaseViewController, ErrorHandling {
  // MARK: - Properties

  var keyboardWillShowObserver: NSObjectProtocol?
  var keyboardWillHideObserver: NSObjectProtocol?
  var keyboardWillChangeFrameObserver: NSObjectProtocol?

  private let searchTextField = TextField()
  private let tableView = UITableView(frame: .zero, style: .grouped)
  private let emptyStateView = EmptyStateView()
  private let searchPlaceholderView = EmptyStateView()

  private let viewModel: FavoritesViewModel
  private let dataSource = TableViewDataSource()

  // MARK: - Init

  init(viewModel: FavoritesViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.update()
    subscribeForKeyboardStateUpdates()
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    unsubscribeFromKeyboardStateUpdates()
  }

  // MARK: - Setup

  private func setup() {
    setupSearchTextField()
    setupTableView()
    setupEmptyStateView()
    setupSearchPlaceholderView()
  }

  private func setupSearchTextField() {
    view.addSubview(searchTextField)
    searchTextField.placeholder = R.string.favorites.searchTextFieldPlaceholder()
    searchTextField.autocorrectionType = .no
    searchTextField.returnKeyType = .search
    searchTextField.hasClearButton = true
    searchTextField.delegate = self
    searchTextField.onChange = { [weak self] text in
      self?.viewModel.updateSearchText(text)
    }
    searchTextField.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupTableView() {
    view.addSubview(tableView)
    tableView.backgroundColor = .clear
    tableView.showsVerticalScrollIndicator = false
    tableView.alwaysBounceVertical = false
    tableView.separatorStyle = .none
    tableView.contentInset.bottom = Constants.additionalTableViewBottomInset
    tableView.estimatedRowHeight = 48
    tableView.rowHeight = UITableView.automaticDimension

    tableView.register(FavoritesSectionHeaderView.self,
                       forHeaderFooterViewReuseIdentifier: FavoritesSectionHeaderView.reuseIdentifier)
    tableView.register(SimpleListItemCell.self, forCellReuseIdentifier: SimpleListItemCell.reuseIdentifier)

    tableView.snp.makeConstraints { make in
      make.top.equalTo(searchTextField.snp.bottom).offset(16)
      make.leading.trailing.bottom.equalToSuperview()
      make.bottom.equalToSuperview()
    }

    dataSource.delegate = self
    dataSource.setup(tableView: tableView, viewModel: viewModel)
  }

  private func setupEmptyStateView() {
    view.addSubview(emptyStateView)
    emptyStateView.configure(with: viewModel.emptyStateViewModel)
    emptyStateView.snp.makeConstraints { make in
      make.centerY.equalTo(view.safeAreaLayoutGuide)
      make.leading.trailing.equalToSuperview().inset(24)
    }
  }

  private func setupSearchPlaceholderView() {
    view.addSubview(searchPlaceholderView)
    searchPlaceholderView.configure(with: viewModel.searchPlaceholderViewModel)
    searchPlaceholderView.snp.makeConstraints { make in
      make.top.equalTo(searchTextField.snp.bottom).offset(32)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  // MARK: - Bind

  private func bindToViewModel() {
    viewModel.onDidUpdate = { [weak self] in
      self?.tableView.reloadData()
      self?.updateEmptyState()
    }
    viewModel.onDidRequestToEndEditing = { [weak self] in
      self?.view.endEditing(true)
    }
    viewModel.onDidRequestToDeleteRow = { [weak self] indexPath in
      self?.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    viewModel.onDidRequestToDeleteSection = { [weak self] index in
      self?.tableView.deleteSections([index], with: .fade)
      self?.updateEmptyState()
    }
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error)
    }
  }

  // MARK: - Private methods

  private func updateEmptyState() {
    emptyStateView.isHidden = viewModel.hasFavorites
    searchTextField.isHidden = !viewModel.hasFavorites
    searchPlaceholderView.isHidden = viewModel.shouldHideSearchPlaceholder
  }
}

// MARK: - TableViewDataSourceDelegate

extension FavoritesViewController: TableViewDataSourceDelegate {
  func tableViewDataSource(_ dataSource: TableViewDataSource, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  func tableViewDataSource(_ dataSource: TableViewDataSource, commit editingStyle: UITableViewCell.EditingStyle,
                           forRowAt indexPath: IndexPath) {
    return viewModel.deleteTimetable(at: indexPath)
  }
}

// MARK: - TextFieldDelegate

extension FavoritesViewController: TextFieldDelegate {
  func textFieldShouldReturn(_ textField: TextField) -> Bool {
    view.endEditing(true)
    return true
  }
}

// MARK: - KeyboardVisibilityHandling

extension FavoritesViewController: KeyboardVisibilityHandling {
  func keyboardWillShow(keyboardInfo: KeyboardInfo) {
    tableView.contentInset.bottom = Constants.additionalTableViewBottomInset + keyboardInfo.keyboardHeight
  }

  func keyboardWillHide(keyboardInfo: KeyboardInfo) {
    tableView.contentInset.bottom = Constants.additionalTableViewBottomInset
  }
}
