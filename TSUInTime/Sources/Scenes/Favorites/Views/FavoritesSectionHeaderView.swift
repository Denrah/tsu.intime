//
//  FavoritesSectionHeaderView.swift
//  TSUInTime
//

import UIKit

class FavoritesSectionHeaderView: UITableViewHeaderFooterView, TableHeaderFooterView {
  let titleLabel = Label(textStyle: .bodyBold)

  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  func configure(with viewModel: TableHeaderFooterViewModel) {
    guard let viewModel = viewModel as? FavoritesSectionHeaderViewModel else { return }
    titleLabel.text = viewModel.title
    titleLabel.snp.remakeConstraints { make in
      make.top.equalToSuperview().inset(viewModel.topInset)
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.equalToSuperview().inset(8).priority(999)
    }
  }

  private func setup() {
    contentView.addSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
  }
}
