//
//  SettingsSwitchOptionView.swift
//  TSUInTime
//

import UIKit

class SettingsSwitchOptionView: UIView {
  // MARK: - Properties
  
  override var intrinsicContentSize: CGSize {
    return CGSize(width: UIView.noIntrinsicMetric, height: 40)
  }
  
  private let titleLabel = Label(textStyle: .body)
  private let optionSwitch = UISwitch()
  
  private var viewModel: SettingsSwitchOptionViewModel?
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: SettingsSwitchOptionViewModel) {
    self.viewModel = viewModel
    titleLabel.text = viewModel.title
    optionSwitch.isOn = viewModel.isSelected
    
    viewModel.onDidUpdate = { [weak self, weak viewModel] in
      guard let viewModel = viewModel else { return }
      self?.optionSwitch.setOn(viewModel.isSelected, animated: true)
    }
  }
  
  // MARK: - Actions
  
  @objc private func switchValueDidChange() {
    viewModel?.didUpdateSwitchValue(isSelected: optionSwitch.isOn)
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupTitleLabel()
    setupOptionSwitch()
  }

  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
    titleLabel.snp.makeConstraints { make in
      make.leading.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
      make.top.greaterThanOrEqualToSuperview()
      make.bottom.lessThanOrEqualToSuperview()
    }
  }
  
  private func setupOptionSwitch() {
    addSubview(optionSwitch)
    optionSwitch.onTintColor = .accent
    optionSwitch.setContentCompressionResistancePriority(.required, for: .horizontal)
    optionSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
    optionSwitch.snp.makeConstraints { make in
      make.leading.equalTo(titleLabel.snp.trailing).offset(16)
      make.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }
}
