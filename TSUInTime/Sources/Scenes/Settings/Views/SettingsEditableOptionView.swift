//
//  SettingsEditableOptionView.swift
//  TSUInTime
//

import UIKit

class SettingsEditableOptionView: UIView {
  // MARK: - Properties
  
  private let titleLabel = Label(textStyle: .bodyBold)
  private let valueLabel = Label(textStyle: .body)
  private let editButton = UIButton(type: .system)
  
  private let viewModel: SettingsEditableOptionViewModel
  
  // MARK: - Init
  
  init(viewModel: SettingsEditableOptionViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Actions
  
  @objc private func edit() {
    viewModel.edit()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupTitleLabel()
    setupValueLabel()
    setupEditButton()
  }
  
  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.text = viewModel.title
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .baseBlack
    titleLabel.snp.makeConstraints { make in
      make.top.equalToSuperview()
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupValueLabel() {
    addSubview(valueLabel)
    valueLabel.text = viewModel.value
    valueLabel.numberOfLines = 0
    valueLabel.textColor = .baseBlack
    valueLabel.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(16)
      make.leading.equalToSuperview().inset(16)
      make.bottom.equalToSuperview().inset(8)
    }
  }
  
  private func setupEditButton() {
    addSubview(editButton)
    editButton.setImage(R.image.pencilIcon(), for: .normal)
    editButton.tintColor = .accent
    editButton.addTarget(self, action: #selector(edit), for: .touchUpInside)
    editButton.snp.makeConstraints { make in
      make.size.equalTo(40)
      make.leading.equalTo(valueLabel.snp.trailing).offset(16)
      make.trailing.equalToSuperview().inset(8)
      make.centerY.equalTo(valueLabel)
    }
  }
}
