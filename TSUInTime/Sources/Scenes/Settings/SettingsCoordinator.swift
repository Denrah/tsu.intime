//
//  SettingsCoordinator.swift
//  TSUInTime
//

import Foundation
import SafariServices

class SettingsCoordinator: Coordinator {
  // MARK: - Properties

  let navigationController: NavigationController
  let appDependency: AppDependency

  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  private var onNeedsToPopToSettings: (() -> Void)?
  private var onNeedsToUpdateFacultyAndGroup: ((_ faculty: Faculty, _ group: Group) -> Void)?
  private var onNeedsToUpdateProfessor: ((_ professor: Professor) -> Void)?

  // MARK: - Init

  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }

  // MARK: - Navigation

  func start(animated: Bool) {
    showSettingsScreen(animated: animated)
  }

  private func showSettingsScreen(animated: Bool) {
    let viewModel = SettingsViewModel(dependencies: appDependency)
    viewModel.delegate = self
    
    onNeedsToUpdateFacultyAndGroup = { [weak viewModel] faculty, group in
      viewModel?.update(faculty: faculty, group: group)
    }
    onNeedsToUpdateProfessor = { [weak viewModel] professor in
      viewModel?.update(professor: professor)
    }
    
    let viewController = SettingsViewController(viewModel: viewModel)
    viewController.title = R.string.settings.screenTitle()
    
    onNeedsToPopToSettings = { [weak self, weak viewController] in
      guard let viewController = viewController else { return }
      self?.navigationController.popToViewController(viewController, animated: true)
    }
    
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  private func showTimetableSearchScreen(searchType: SearchType) {
    let viewModel = SearchViewModel(dependencies: appDependency, searchType: searchType)
    viewModel.delegate = self
    let viewController = SearchViewController(viewModel: viewModel)
    viewController.title = titleForSearchScreen(searchType: searchType)
    navigationController.pushViewController(viewController, animated: true)
  }

  private func openSafari(for url: URL) {
    let viewController = SFSafariViewController(url: url)
    navigationController.present(viewController, animated: true)
  }

  // MARK: - Private methods

  private func titleForSearchScreen(searchType: SearchType) -> String? {
    switch searchType {
    case .faculties:
      return R.string.settings.facultiesSearchScreenTitle()
    case .groups:
      return R.string.settings.groupsSearchScreenTitle()
    case .professors:
      return R.string.settings.professorsSearchScreenTitle()
    default:
      return nil
    }
  }
}

// MARK: - SettingsViewModelDelegate

extension SettingsCoordinator: SettingsViewModelDelegate {
  func settingsViewModelDidRequestToEditFaculty(_ viewModel: SettingsViewModel) {
    showTimetableSearchScreen(searchType: .faculties)
  }
  
  func settingsViewModelDidRequestToEditGroup(_ viewModel: SettingsViewModel, faculty: Faculty) {
    showTimetableSearchScreen(searchType: .groups(faculty: faculty))
  }
  
  func settingsViewModelDidRequestToEditProfessor(_ viewModel: SettingsViewModel) {
    showTimetableSearchScreen(searchType: .professors)
  }

  func settingsViewModel(_ viewModel: SettingsViewModel, didRequestToOpenWebPageWith url: URL) {
    openSafari(for: url)
  }
}

// MARK: - SearchViewModelDelegate

extension SettingsCoordinator: SearchViewModelDelegate {
  func searchViewModel(_ viewModel: SearchViewModel, didSelect faculty: Faculty) {
    showTimetableSearchScreen(searchType: .groups(faculty: faculty))
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect group: Group, faculty: Faculty) {
    onNeedsToUpdateFacultyAndGroup?(faculty, group)
    onNeedsToPopToSettings?()
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect professor: Professor) {
    onNeedsToUpdateProfessor?(professor)
    onNeedsToPopToSettings?()
  }
}
