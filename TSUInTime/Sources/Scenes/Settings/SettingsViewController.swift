//
//  SettingsViewController.swift
//  TSUInTime
//

import UIKit

class SettingsViewController: BaseViewController, ErrorHandling, GoToSettingsAlertShowing {
  // MARK: - Properties

  private let scrollView = UIScrollView()
  private let stackView = UIStackView()
  private let notificationsTitleLabel = Label(textStyle: .bodyBold)
  private let timetableNotificationsSwitchView = SettingsSwitchOptionView()
  private let privacyPolicyButton = UIButton(type: .system)
  
  private let viewModel: SettingsViewModel
  
  // MARK: - Init
  
  init(viewModel: SettingsViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }

  // MARK: - Actions

  @objc private func handlePrivacyPolicyButtonTap() {
    viewModel.didTapPrivacyPolicyButton()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupScrollView()
    setupStackView()
    setupSettings()
  }

  private func setupScrollView() {
    view.addSubview(scrollView)
    scrollView.showsVerticalScrollIndicator = false
    scrollView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupStackView() {
    scrollView.addSubview(stackView)
    stackView.axis = .vertical
    stackView.spacing = 16
    stackView.snp.makeConstraints { make in
      make.top.bottom.equalToSuperview().inset(16)
      make.leading.trailing.equalToSuperview()
      make.width.equalTo(view)
    }
  }

  private func setupSettings() {
    setupOptions()
    setupNotificationsTitleLabel()
    setupTimetableNotificationsSwitchView()
    setupPrivacyPolicyButton()
  }
  
  private func setupOptions() {
    viewModel.optionViewModels.enumerated().forEach { index, optionViewModel in
      let optionView = SettingsEditableOptionView(viewModel: optionViewModel)
      stackView.addArrangedSubview(optionView)
      
      if index == viewModel.optionViewModels.count - 1 {
        stackView.setCustomSpacing(48, after: optionView)
      }
    }
  }
  
  private func setupNotificationsTitleLabel() {
    let wrappedLabel = wrapView(notificationsTitleLabel)
    stackView.addArrangedSubview(wrappedLabel)
    notificationsTitleLabel.text = R.string.settings.notificationsTitle()
    notificationsTitleLabel.textColor = .baseBlack
    stackView.setCustomSpacing(8, after: wrappedLabel)
  }
  
  private func setupTimetableNotificationsSwitchView() {
    stackView.addArrangedSubview(timetableNotificationsSwitchView)
    timetableNotificationsSwitchView.configure(with: viewModel.timetableUpdatesOptionViewModel)
    stackView.setCustomSpacing(40, after: timetableNotificationsSwitchView)
  }

  private func setupPrivacyPolicyButton() {
    let wrappedView = wrapView(privacyPolicyButton)
    stackView.addArrangedSubview(wrappedView)
    privacyPolicyButton.setImage(R.image.linkIcon(), for: .normal)
    privacyPolicyButton.setTitle(R.string.settings.privacyPolicyButtonTitle(), for: .normal)
    privacyPolicyButton.setTitleColor(.accent, for: .normal)
    privacyPolicyButton.tintColor = .accent
    privacyPolicyButton.titleLabel?.font = .body
    privacyPolicyButton.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                    action: #selector(handlePrivacyPolicyButtonTap)))

    if #available(iOS 15.0, *) {
      var configuration = UIButton.Configuration.plain()
      configuration.imagePlacement = .trailing
      configuration.contentInsets = .zero
      configuration.imagePadding = 8
      privacyPolicyButton.configuration = configuration
    } else {
      privacyPolicyButton.semanticContentAttribute = .forceRightToLeft
      privacyPolicyButton.imageEdgeInsets.left = 8
      privacyPolicyButton.imageEdgeInsets.right = -8
      privacyPolicyButton.contentEdgeInsets.right = 8
    }

    privacyPolicyButton.snp.remakeConstraints { make in
      make.height.equalTo(40)
      make.leading.equalToSuperview().inset(16)
      make.top.bottom.equalToSuperview()
      make.trailing.lessThanOrEqualToSuperview().inset(16)
    }
  }
  
  private func wrapView(_ view: UIView) -> UIView {
    let containerView = UIView()
    containerView.addSubview(view)
    view.snp.makeConstraints { make in
      make.top.bottom.equalToSuperview()
      make.leading.trailing.equalToSuperview().inset(16)
    }
    return containerView
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidUpdate = { [weak self] in
      self?.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
      self?.setupSettings()
    }
    viewModel.onUserDidRestrictedNotifications = { [weak self] in
      self?.showNotificationsRestrictedAlert()
    }
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error)
    }
  }
  
  // MARK: - Private methods
  
  private func showNotificationsRestrictedAlert() {
    showGoToSettingsAlert(title: R.string.onboarding.notificationsRestrictedAlertTitle(),
                          description: R.string.onboarding.notificationsRestrictedAlertDescription(),
                          cancelButtonTitle: R.string.common.cancel())
  }
}
