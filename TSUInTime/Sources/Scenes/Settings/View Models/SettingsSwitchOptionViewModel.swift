//
//  SettingsSwitchOptionViewModel.swift
//  TSUInTime
//

import Foundation

protocol SettingsSwitchOptionViewModelDelegate: AnyObject {
  func settingsSwitchOptionViewModel(_ viewModel: SettingsSwitchOptionViewModel,
                                     didChangeStateTo isSelected: Bool)
}

class SettingsSwitchOptionViewModel {
  weak var delegate: SettingsSwitchOptionViewModelDelegate?
  
  var onDidUpdate: (() -> Void)?
  
  let title: String
  private(set) var isSelected: Bool
  
  init(title: String, isSelected: Bool) {
    self.title = title
    self.isSelected = isSelected
  }
  
  func didUpdateSwitchValue(isSelected: Bool) {
    self.isSelected = isSelected
    delegate?.settingsSwitchOptionViewModel(self, didChangeStateTo: isSelected)
  }
  
  func setState(isSelected: Bool) {
    self.isSelected = isSelected
    onDidUpdate?()
  }
}
