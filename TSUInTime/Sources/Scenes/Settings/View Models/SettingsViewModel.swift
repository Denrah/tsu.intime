//
//  SettingsViewModel.swift
//  TSUInTime
//

import Foundation

private extension Constants {
  static let privacyPolicyLink = "https://persona.tsu.ru/Content/documents/politika_pd.pdf"
}

protocol SettingsViewModelDelegate: AnyObject {
  func settingsViewModelDidRequestToEditFaculty(_ viewModel: SettingsViewModel)
  func settingsViewModelDidRequestToEditGroup(_ viewModel: SettingsViewModel, faculty: Faculty)
  func settingsViewModelDidRequestToEditProfessor(_ viewModel: SettingsViewModel)
  func settingsViewModel(_ viewModel: SettingsViewModel, didRequestToOpenWebPageWith url: URL)
}

class SettingsViewModel {
  typealias Dependencies = HasDataStore & HasPushNotificationsService
  
  // MARK: - Properties
  
  weak var delegate: SettingsViewModelDelegate?
  
  var onDidUpdate: (() -> Void)?
  var onUserDidRestrictedNotifications: (() -> Void)?
  var onDidReceiveError: ((_ error: Error) -> Void)?
  
  private(set) var optionViewModels: [SettingsEditableOptionViewModel] = []
  let timetableUpdatesOptionViewModel: SettingsSwitchOptionViewModel
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
    
    let timetableUpdatesOptionTitle = R.string.settings.timetableUpdatesNotificationsTitle()
    let timetableUpdatesOptionState = dependencies.dataStore.timetableUpdatesNotificationsEnabled
    timetableUpdatesOptionViewModel = SettingsSwitchOptionViewModel(title: timetableUpdatesOptionTitle,
                                                                    isSelected: timetableUpdatesOptionState)
    timetableUpdatesOptionViewModel.delegate = self
    
    setupTimetableOptions()
  }
  
  // MARK: - Public methods
  
  func update(faculty: Faculty, group: Group) {
    let userInfo = UserInfo(role: .student, timetableType: .group, timetableID: group.id,
                            timetableName: group.name, faculty: faculty, group: group)
    update(userInfo: userInfo)
  }
  
  func update(professor: Professor) {
    let userInfo = UserInfo(role: .professor, timetableType: .professor, timetableID: professor.id,
                            timetableName: professor.shortName, professor: professor)
    update(userInfo: userInfo)
  }

  func didTapPrivacyPolicyButton() {
    guard let url = URL(string: Constants.privacyPolicyLink) else { return }
    delegate?.settingsViewModel(self, didRequestToOpenWebPageWith: url)
  }
  
  // MARK: - Private methods
  
  private func setupTimetableOptions() {
    guard let userInfo = dependencies.dataStore.userInfo else { return }
    optionViewModels.removeAll()
    
    switch userInfo.role {
    case .student:
      let facultyEditViewModel = SettingsEditableOptionViewModel(title: R.string.settings.facultyOptionTitle(),
                                                                 value: userInfo.faculty?.name)
      let groupEditViewModel = SettingsEditableOptionViewModel(title: R.string.settings.groupOptionTitle(),
                                                               value: userInfo.group?.name)
      optionViewModels = [facultyEditViewModel, groupEditViewModel]
      facultyEditViewModel.onDidTapEditButton = { [weak self] in
        guard let self = self else { return }
        self.delegate?.settingsViewModelDidRequestToEditFaculty(self)
      }
      groupEditViewModel.onDidTapEditButton = { [weak self] in
        guard let self = self, let faculty = userInfo.faculty else { return }
        self.delegate?.settingsViewModelDidRequestToEditGroup(self, faculty: faculty)
      }
    case .professor:
      let professorEditViewModel = SettingsEditableOptionViewModel(title: R.string.settings.professorOptionTitle(),
                                                                   value: userInfo.professor?.name)
      optionViewModels = [professorEditViewModel]
      professorEditViewModel.onDidTapEditButton = { [weak self] in
        guard let self = self else { return }
        self.delegate?.settingsViewModelDidRequestToEditProfessor(self)
      }
    }
  }
  
  private func update(userInfo: UserInfo) {
    if timetableUpdatesOptionViewModel.isSelected, let userInfo = dependencies.dataStore.userInfo {
      unsubscribeFromTimetableUpdates(userInfo: userInfo)
    }
    
    dependencies.dataStore.userInfo = userInfo
    setupTimetableOptions()
    onDidUpdate?()
    
    if timetableUpdatesOptionViewModel.isSelected {
      subscribeToTimetableUpdates(userInfo: userInfo)
    }
  }
  
  private func subscribeToTimetableUpdates(userInfo: UserInfo) {
    let rollback = { [weak self] in
      self?.timetableUpdatesOptionViewModel.setState(isSelected: false)
    }
    dependencies.pushNotificationsService.register().then { _ in
      self.dependencies.pushNotificationsService.subscribe(to: .timetableUpdates(userInfo: userInfo))
    }.catch { error in
      if (error as? PushNotificationsServiceError) == .restrictedByUser {
        self.onUserDidRestrictedNotifications?()
      } else {
        self.onDidReceiveError?(error)
      }
      rollback()
    }
  }
  
  private func unsubscribeFromTimetableUpdates(userInfo: UserInfo) {
    let rollback = { [weak self] in
      self?.timetableUpdatesOptionViewModel.setState(isSelected: true)
    }
    dependencies.pushNotificationsService.subscribe(to: .timetableUpdates(userInfo: userInfo)).catch { error in
      self.onDidReceiveError?(error)
      rollback()
    }
  }
}

// MARK: - SettingsSwitchOptionViewModelDelegate

extension SettingsViewModel: SettingsSwitchOptionViewModelDelegate {
  func settingsSwitchOptionViewModel(_ viewModel: SettingsSwitchOptionViewModel,
                                     didChangeStateTo isSelected: Bool) {
    guard let userInfo = dependencies.dataStore.userInfo else { return }
    
    if isSelected {
      subscribeToTimetableUpdates(userInfo: userInfo)
    } else {
      unsubscribeFromTimetableUpdates(userInfo: userInfo)
    }
  }
}
