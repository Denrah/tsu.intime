//
//  SettingsEditableOptionViewModel.swift
//  TSUInTime
//

import Foundation

class SettingsEditableOptionViewModel {
  var onDidTapEditButton: (() -> Void)?
  
  let title: String?
  let value: String?
  
  init(title: String?, value: String?) {
    self.title = title
    self.value = value
  }
  
  func edit() {
    onDidTapEditButton?()
  }
}
