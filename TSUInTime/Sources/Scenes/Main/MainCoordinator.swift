//
//  MainCoordinator.swift
//  TSUInTime
//

import UIKit

class MainCoordinator: Coordinator {
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  let navigationController: NavigationController
  let appDependency: AppDependency
  
  private let utility: MainCoordinatorUtility
  
  required init(navigationController: NavigationController,
                appDependency: AppDependency = AppDependency()) {
    self.navigationController = navigationController
    self.appDependency = appDependency
    self.utility = MainCoordinatorUtility(appDependency: appDependency)

    self.appDependency.pushNotificationsService.delegate = self
  }
  
  func start(animated: Bool) {
    utility.start()
    
    if appDependency.dataStore.userInfo == nil {
      showWelcomeScreen()
    } else {
      showTimetable()
    }
  }
  
  private func showWelcomeScreen() {
    let coordinator = show(WelcomeCoordinator.self, animated: false)
    coordinator.delegate = self
  }
  
  private func showTimetable() {
    guard let userInfo = appDependency.dataStore.userInfo else {
      resetCoordinators()
      return
    }
    let timetableInfo = TimetableInfo(userInfo: userInfo)
    let configuration = TimetableCoordinatorConfiguration(timetableInfo: timetableInfo)
    let coordinator = show(TimetableCoordinator.self, configuration: configuration, animated: false)
    coordinator.delegate = self
  }
  
  private func resetCoordinators() {
    navigationController.dismiss(animated: false, completion: nil)
    navigationController.setViewControllers([], animated: false)
    navigationController.removeAllPopObservers()
    childCoordinators.removeAll()
    if let window = UIApplication.shared.connectedScenes
        .filter({ $0.activationState == .foregroundActive })
        .map({ $0 as? UIWindowScene })
        .compactMap({ $0 })
        .first?.windows
        .first(where: { $0.isKeyWindow }) {
      changeRootViewController(of: window, to: navigationController)
    }
    start(animated: false)
  }
  
  private func changeRootViewController(of window: UIWindow,
                                        to viewController: UIViewController,
                                        animationDuration: TimeInterval = 0.5) {
    let animations = {
      UIView.performWithoutAnimation {
        window.rootViewController = self.navigationController
      }
    }
    UIView.transition(with: window, duration: animationDuration, options: .transitionFlipFromLeft,
                      animations: animations, completion: nil)
  }
}

// MARK: - WelcomeCoordinatorDelegate

extension MainCoordinator: WelcomeCoordinatorDelegate {
  func welcomeCoordinatorDidFinish(_ coordinator: WelcomeCoordinator) {
    resetCoordinators()
  }
}

// MARK: - TimetableCoordinatorDelegate

extension MainCoordinator: TimetableCoordinatorDelegate {
  func timetableCoordinatorDidRequestToSignOut(_ coordinator: TimetableCoordinator) {
    appDependency.dataStore.clearAllData()
    try? appDependency.favoritesStorageService.deleteAllTimetables()
    resetCoordinators()
  }
}

// MARK: - PushNotificationsServiceDelegate, BannerShowing

extension MainCoordinator: PushNotificationsServiceDelegate, BannerShowing {
  func pushNotificationsService(_ service: PushNotificationsService, didReceiveNotification notification: PushNotification) {
    showBanner(title: notification.title, subtitle: notification.body, style: .info)
  }
}
