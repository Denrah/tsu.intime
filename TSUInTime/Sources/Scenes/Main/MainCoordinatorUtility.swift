//
//  MainCoordinatorUtility.swift
//  TSUInTime
//

import Foundation

class MainCoordinatorUtility {
  // MARK: - Properties
  
  private let appDependency: AppDependency
  
  // MARK: - Init
  
  init(appDependency: AppDependency) {
    self.appDependency = appDependency
  }
  
  // MARK: - Public methods
  
  func start() {
    updateFirebaseSubscriptions()
  }
  
  // MARK: - Private methods
  
  private func updateFirebaseSubscriptions() {
    guard let storedTopics = appDependency.dataStore.firebaseSubscribedTopics else { return }
    var timetableUpdatesTopic: FirebaseTopic?
    if let userInfo = appDependency.dataStore.userInfo {
      timetableUpdatesTopic = FirebaseTopic.timetableUpdates(userInfo: userInfo)
    }
    storedTopics.filter { $0 != timetableUpdatesTopic?.topicName }.forEach { topic in
      appDependency.pushNotificationsService.unsubscribe(from: topic).cauterize()
    }
  }
}
