//
//  OnboardingRolePageViewModel.swift
//  TSUInTime
//

import Foundation

protocol OnboardingRolePageViewModelDelegate: AnyObject {
  func onboardingRolePageViewModel(_ viewModel: OnboardingRolePageViewModel,
                                   didSelect role: UserRole)
}

class OnboardingRolePageViewModel {
  weak var delegate: OnboardingRolePageViewModelDelegate?
  
  func didSelectRole(_ role: UserRole) {
    delegate?.onboardingRolePageViewModel(self, didSelect: role)
  }
}
