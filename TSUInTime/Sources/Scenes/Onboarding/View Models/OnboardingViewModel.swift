//
//  OnboardingViewModel.swift
//  TSUInTime
//

import Foundation

private extension Constants {
  static let studentPagesCount = 3
  static let professorPagesCount = 2
  static let rolePageNumber = 1
}

protocol OnboardingViewModelDelegate: AnyObject {
  func onboardingViewModelDidFinish(_ viewModel: OnboardingViewModel)
}

class OnboardingViewModel {
  typealias Dependencies = HasFacultiesService & HasProfessorsService & HasDataStore
  
  // MARK: - Properties
  
  weak var delegate: OnboardingViewModelDelegate?

  var onDidRequestToShowNextPage: (() -> Void)?
  var onDidSelectRole: ((_ role: UserRole) -> Void)?
  var onDidUpdatePagesCount: (() -> Void)?
  
  let rolePageViewModel: OnboardingRolePageViewModel
  let facultiesPageViewModel: FacultiesSearchiewModel
  let groupsPageViewModel: GroupsSearchViewModel
  let professorsPageViewModel: ProfessorsSearchViewModel
  
  private(set) var pagesCount = Constants.studentPagesCount
  
  private var hasSelectedRole = false
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
    
    rolePageViewModel = OnboardingRolePageViewModel()
    facultiesPageViewModel = FacultiesSearchiewModel(dependencies: dependencies,
                                                     title: R.string.onboarding.facultiesPageTitle())
    groupsPageViewModel = GroupsSearchViewModel(dependencies: dependencies,
                                                title: R.string.onboarding.groupsPageTitle())
    professorsPageViewModel = ProfessorsSearchViewModel(dependencies: dependencies,
                                                        title: R.string.onboarding.professorsPageTitle())
    
    rolePageViewModel.delegate = self
    facultiesPageViewModel.delegate = self
    groupsPageViewModel.delegate = self
    professorsPageViewModel.delegate = self
  }

  func updateForSelectedPage(pageNumber: Int) {
    if pageNumber == Constants.rolePageNumber {
      hasSelectedRole = false
      pagesCount = Constants.studentPagesCount
      onDidUpdatePagesCount?()
    }
  }
}

// MARK: - OnboardingRolePageViewModelDelegate

extension OnboardingViewModel: OnboardingRolePageViewModelDelegate {
  func onboardingRolePageViewModel(_ viewModel: OnboardingRolePageViewModel,
                                   didSelect role: UserRole) {
    guard !hasSelectedRole else { return}
    
    hasSelectedRole = true
    
    switch role {
    case .student:
      pagesCount = Constants.studentPagesCount
    case .professor:
      pagesCount = Constants.professorPagesCount
    }
    onDidSelectRole?(role)
    onDidRequestToShowNextPage?()
  }
}

// MARK: - FacultiesSearchViewModelDelegate

extension OnboardingViewModel: FacultiesSearchViewModelDelegate {
  func facultiesSearchViewModel(_ viewModel: FacultiesSearchiewModel,
                                didSelect faculty: Faculty) {
    groupsPageViewModel.update(faculty: faculty)
    onDidRequestToShowNextPage?()
  }
}

// MARK: - GroupsSearchViewModelDelegate

extension OnboardingViewModel: GroupsSearchViewModelDelegate {
  func groupsSearchViewModel(_ viewModel: GroupsSearchViewModel, didSelect group: Group, faculty: Faculty) {
    let userInfo = UserInfo(role: .student, timetableType: .group, timetableID: group.id,
                            timetableName: group.name, faculty: faculty, group: group, professor: nil)
    dependencies.dataStore.userInfo = userInfo
    delegate?.onboardingViewModelDidFinish(self)
  }
}

// MARK: - ProfessorsSearchViewModelDelegate

extension OnboardingViewModel: ProfessorsSearchViewModelDelegate {
  func professorsSearchViewModel(_ viewModel: ProfessorsSearchViewModel,
                                 didSelect professor: Professor) {
    let userInfo = UserInfo(role: .professor, timetableType: .professor, timetableID: professor.id,
                            timetableName: professor.shortName, faculty: nil, group: nil, professor: professor)
    dependencies.dataStore.userInfo = userInfo
    delegate?.onboardingViewModelDidFinish(self)
  }
}
