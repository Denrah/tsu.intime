//
//  ProfessorListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol ProfessorListItemViewModelDelegate: AnyObject {
  func professorListItemViewModel(_ viewModel: ProfessorListItemViewModel, didSelect professor: Professor)
}

class ProfessorListItemViewModel: SimpleListItemViewModel {
  weak var delegate: ProfessorListItemViewModelDelegate?
  
  var title: String {
    professor.name
  }
  
  var highlightedText: String?
  
  private let professor: Professor
  
  init(professor: Professor) {
    self.professor = professor
  }
  
  func select() {
    delegate?.professorListItemViewModel(self, didSelect: professor)
  }
}
