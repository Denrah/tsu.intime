//
//  OnboardingNotificationsViewModel.swift
//  TSUInTime
//

import Foundation
import PromiseKit

protocol OnboardingNotificationsViewModelDelegate: AnyObject {
  func onboardingNotificationsViewModelDidFinish(_ viewModel: OnboardingNotificationsViewModel)
}

class OnboardingNotificationsViewModel {
  typealias Dependencies = HasPushNotificationsService & HasDataStore
  
  weak var delegate: OnboardingNotificationsViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onUserDidRestrictedNotifications: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  private let dependencies: Dependencies
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
  }
  
  func enablePushNotifications() {
    dependencies.pushNotificationsService.register().then { _ -> Promise<Void> in
      self.onDidStartRequest?()
      return self.subscribeToTopics()
    }.ensure {
      self.onDidFinishRequest?()
    }.done { _ in
      self.delegate?.onboardingNotificationsViewModelDidFinish(self)
      self.dependencies.dataStore.timetableUpdatesNotificationsEnabled = true
    }.catch { error in
      if (error as? PushNotificationsServiceError) == .restrictedByUser {
        self.onUserDidRestrictedNotifications?()
      } else {
        self.onDidReceiveError?(error)
      }
    }
  }
  
  func skip() {
    delegate?.onboardingNotificationsViewModelDidFinish(self)
  }
  
  private func subscribeToTopics() -> Promise<Void> {
    guard let userInfo = dependencies.dataStore.userInfo else {
      return Promise(error: PushNotificationsServiceError.noUserInfo)
    }
    return dependencies.pushNotificationsService.subscribe(to: .timetableUpdates(userInfo: userInfo))
  }
}
