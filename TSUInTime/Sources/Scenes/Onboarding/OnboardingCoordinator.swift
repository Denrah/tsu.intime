//
//  OnboardingCoordinator.swift
//  TSUInTime
//

import Foundation

protocol OnboardingCoordinatorDelegate: AnyObject {
  func onboardingCoordinatorDidFinish(_ coordonator: OnboardingCoordinator)
}

class OnboardingCoordinator: Coordinator {
  // MARK: - Properties
  
  weak var delegate: OnboardingCoordinatorDelegate?
  
  let navigationController: NavigationController
  let appDependency: AppDependency
  
  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?
  
  // MARK: - Init
  
  required init(navigationController: NavigationController, appDependency: AppDependency) {
    self.navigationController = navigationController
    self.appDependency = appDependency
  }
  
  // MARK: - Navigation
  
  func start(animated: Bool) {
    showOnboarding(animated: animated)
  }
  
  private func showOnboarding(animated: Bool) {
    let viewModel = OnboardingViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = OnboardingViewController(viewModel: viewModel)
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }
  
  private func showNotificationScreen() {
    let viewModel = OnboardingNotificationsViewModel(dependencies: appDependency)
    viewModel.delegate = self
    let viewController = OnboardingNotificationsViewController(viewModel: viewModel)
    navigationController.pushViewController(viewController, animated: true)
  }
  
  private func showTimetable() {
    show(TimetableCoordinator.self, animated: true)
  }
}

// MARK: - OnboardingViewModelDelegate

extension OnboardingCoordinator: OnboardingViewModelDelegate {
  func onboardingViewModelDidFinish(_ viewModel: OnboardingViewModel) {
    showNotificationScreen()
  }
}

// MARK: - OnboardingNotificationsViewModelDelegate

extension OnboardingCoordinator: OnboardingNotificationsViewModelDelegate {
  func onboardingNotificationsViewModelDidFinish(_ viewModel: OnboardingNotificationsViewModel) {
    delegate?.onboardingCoordinatorDidFinish(self)
    onDidFinish?()
  }
}
