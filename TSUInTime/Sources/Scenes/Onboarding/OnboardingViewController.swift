//
//  OnboardingViewController.swift
//  TSUInTime
//

import UIKit

class OnboardingViewController: BaseViewController, NavigationBarHiding {
  // MARK: - Properties

  private let stepProgressView = StepProgressView()
  private let scrollView = UIScrollView()
  private let stackView = UIStackView()
  
  private let viewModel: OnboardingViewModel

  private var pages: [UIView] = []
  private var currentPage = 1
  
  // MARK: - Init
  
  init(viewModel: OnboardingViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }

  // MARK: - Setup

  private func setup() {
    setupStepProgressView()
    setupScrollView()
    setupStackView()
    setupPages()
  }

  private func setupStepProgressView() {
    view.addSubview(stepProgressView)
    stepProgressView.onDidSelectStep = { [weak self] stepNumber in
      guard let self = self, stepNumber < self.currentPage else { return }
      self.showPage(pageNumber: stepNumber)
      self.viewModel.updateForSelectedPage(pageNumber: stepNumber)
    }
    stepProgressView.snp.makeConstraints { make in
      make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupScrollView() {
    view.addSubview(scrollView)
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.isPagingEnabled = true
    scrollView.isScrollEnabled = false
    scrollView.snp.makeConstraints { make in
      make.top.equalTo(stepProgressView.snp.bottom).offset(8)
      make.leading.trailing.bottom.equalToSuperview()
    }
  }

  private func setupStackView() {
    scrollView.addSubview(stackView)
    stackView.axis = .horizontal
    stackView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.height.equalToSuperview()
    }
  }

  private func setupPages() {
    let rolePageView = OnboardingRolePageView(viewModel: viewModel.rolePageViewModel)
    pages.append(rolePageView)
    stackView.addArrangedSubview(rolePageView)
    
    rolePageView.snp.makeConstraints { make in
      make.width.equalTo(view.snp.width)
    }
    
    stepProgressView.stepsCount = viewModel.pagesCount
  }

  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidRequestToShowNextPage = { [weak self] in
      self?.showNextPage()
    }
    
    viewModel.onDidSelectRole = { [weak self] role in
      self?.updateForRole(role)
    }

    viewModel.onDidUpdatePagesCount = { [weak self] in
      guard let self = self else { return }
      self.stepProgressView.stepsCount = self.viewModel.pagesCount
    }
  }
  
  // MARK: - Private methods

  private func showPage(pageNumber: Int) {
    view.endEditing(true)

    if pageNumber <= pages.count, pageNumber > 0 {
      currentPage = pageNumber
      scrollView.setContentOffset(CGPoint(x: view.frame.width * CGFloat(currentPage - 1), y: 0), animated: true)
      stepProgressView.setCurrentStep(currentPage, animated: true)
    }
  }

  private func showNextPage() {
    showPage(pageNumber: currentPage + 1)
  }
  
  private func updateForRole(_ role: UserRole) {
    let rolePageView = OnboardingRolePageView(viewModel: viewModel.rolePageViewModel)
    
    switch role {
    case .student:
      let facultiesPageView = SearchableTableView<FacultyListItemCell>(viewModel: viewModel.facultiesPageViewModel)
      let groupsPageView = SearchableTableView<SimpleListItemCell>(viewModel: viewModel.groupsPageViewModel)
      self.pages = [rolePageView, facultiesPageView, groupsPageView]
    case .professor:
      let professorsPageView = SearchableTableView<SimpleListItemCell>(viewModel: viewModel.professorsPageViewModel)
      self.pages = [rolePageView, professorsPageView]
    }

    stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    
    self.pages.forEach { page in
      stackView.addArrangedSubview(page)
      page.snp.makeConstraints { make in
        make.width.equalTo(view.snp.width)
      }
    }
    
    stepProgressView.stepsCount = viewModel.pagesCount
  }
}
