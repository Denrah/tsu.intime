//
//  StepProgressView.swift
//  TSUInTime
//

import UIKit

class StepProgressView: UIView {
  // MARK: - Properties

  var onDidSelectStep: ((_ stepNumber: Int) -> Void)?

  var stepsCount: Int = 0 {
    didSet {
      updateStepsCount()
    }
  }

  private let progressView = UIView()
  private let progressViewContainer = UIView()
  private let stepsStackView = UIStackView()

  private var currentStep: Int = 1

  // MARK: - Init

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  // MARK: - Public methods

  func setCurrentStep(_ currentStep: Int, animated: Bool) {
    guard currentStep > 0, currentStep <= stepsCount else { return }
    self.currentStep = currentStep

    if animated {
      UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
        self.update()
      }, completion: nil)
    } else {
      update()
    }
  }

  // MARK: - Setup

  private func setup() {
    setupProgressViewContainer()
    setupProgressView()
    setupStepsStackView()
  }

  private func setupProgressViewContainer() {
    addSubview(progressViewContainer)
    progressViewContainer.backgroundColor = .shade1
    progressViewContainer.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
      make.height.equalTo(4)
    }
  }

  private func setupProgressView() {
    progressViewContainer.addSubview(progressView)
    progressView.backgroundColor = .accent
    remakeProgressViewConstraints()
  }

  func setupStepsStackView() {
    addSubview(stepsStackView)
    stepsStackView.axis = .horizontal
    stepsStackView.distribution = .equalSpacing
    stepsStackView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.height.equalTo(32)
    }
  }

  // MARK: - Private methods

  private func update() {
    stepsStackView.arrangedSubviews.enumerated().forEach { index, item in
      (item as? StepProgressCircleView)?.isActive = index < currentStep
    }
    remakeProgressViewConstraints()
    layoutIfNeeded()
  }

  private func updateStepsCount() {
    stepsStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }

    for stepNumber in 1...stepsCount {
      let circleView = StepProgressCircleView()
      circleView.title = "\(stepNumber)"
      circleView.onDidTap = { [weak self] in
        self?.handleStepTap(stepNumber: stepNumber)
      }
      UIView.transition(with: stepsStackView, duration: 0.3,
                        options: [.curveEaseInOut, .transitionCrossDissolve], animations: {
        self.stepsStackView.addArrangedSubview(circleView)
      }, completion: nil)
    }

    update()
  }

  private func remakeProgressViewConstraints() {
    let multiplier: Double
    if stepsCount > 1 {
      multiplier = Double(currentStep - 1) / Double(stepsCount - 1)
    } else {
      multiplier = 0
    }
    progressView.snp.remakeConstraints { make in
      make.top.bottom.leading.equalToSuperview()
      make.width.equalToSuperview().multipliedBy(multiplier)
    }
  }

  private func handleStepTap(stepNumber: Int) {
    onDidSelectStep?(stepNumber)
  }
}
