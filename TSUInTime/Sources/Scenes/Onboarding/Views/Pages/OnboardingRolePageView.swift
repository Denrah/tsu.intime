//
//  OnboardingRolePageView.swift
//  TSUInTime
//

import UIKit

class OnboardingRolePageView: UIView {
  // MARK: - Properties

  private let titleLabel = Label(textStyle: .bodyBold)
  private let subtitleLabel = Label(textStyle: .body)
  private let studentRoleCardView = OnboardingRoleCardView(type: .student)
  private let professorRoleCardView = OnboardingRoleCardView(type: .professor)
  
  private let viewModel: OnboardingRolePageViewModel

  // MARK: - Init

  init(viewModel: OnboardingRolePageViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Setup

  private func setup() {
    setupTitleLabel()
    setupSubtitleLabel()
    setupStudentRoleCardView()
    setupProfessorRoleCardView()
  }

  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
    titleLabel.text = R.string.onboarding.rolePageTitle()
    titleLabel.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupSubtitleLabel() {
    addSubview(subtitleLabel)
    subtitleLabel.textColor = .baseBlack
    subtitleLabel.numberOfLines = 0
    subtitleLabel.text = R.string.onboarding.rolePageSubtitle()
    subtitleLabel.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(8)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupStudentRoleCardView() {
    addSubview(studentRoleCardView)
    studentRoleCardView.onDidTapView = { [weak self] role in
      self?.viewModel.didSelectRole(role)
    }
    studentRoleCardView.snp.makeConstraints { make in
      make.top.equalTo(subtitleLabel.snp.bottom).offset(64).priority(250)
      make.top.greaterThanOrEqualTo(subtitleLabel.snp.bottom).offset(16)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  private func setupProfessorRoleCardView() {
    addSubview(professorRoleCardView)
    professorRoleCardView.onDidTapView = { [weak self] role in
      self?.viewModel.didSelectRole(role)
    }
    professorRoleCardView.snp.makeConstraints { make in
      make.top.equalTo(studentRoleCardView.snp.bottom).offset(24)
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.lessThanOrEqualToSuperview().offset(-16)
    }
  }
}
