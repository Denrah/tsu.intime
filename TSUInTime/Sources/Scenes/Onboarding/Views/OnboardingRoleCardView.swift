//
//  OnboardingRoleCardView.swift
//  TSUInTime
//

import UIKit

enum OnboardingRoleCardType {
  case student, professor

  var backgroundColor: UIColor {
    switch self {
    case .student:
      return .accent
    case .professor:
      return .accentGreen
    }
  }

  var avatarImage: UIImage? {
    switch self {
    case .student:
      return R.image.roleStudentAvatar()
    case .professor:
      return R.image.roleProfessorAvatar()
    }
  }

  var title: String {
    switch self {
    case .student:
      return R.string.onboarding.roleCardStudentTitle()
    case .professor:
      return R.string.onboarding.roleCardProfessorTitle()
    }
  }

  var subtitle: String {
    switch self {
    case .student:
      return R.string.onboarding.roleCardStudentSubtitle()
    case .professor:
      return R.string.onboarding.roleCardProfessorSubtitle()
    }
  }
  
  var role: UserRole {
    switch self {
    case .student:
      return .student
    case .professor:
      return .professor
    }
  }
}

class OnboardingRoleCardView: UIView {
  // MARK: - Properties
  
  var onDidTapView: ((_ role: UserRole) -> Void)?

  private let avatarImageView = UIImageView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let subtitleLabel = Label(textStyle: .footnote)
  private let disclosureIndicatorImageView = UIImageView()

  private let type: OnboardingRoleCardType

  // MARK: - Init

  init(type: OnboardingRoleCardType) {
    self.type = type
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    type = .student
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Actions
  
  @objc private func didTapView() {
    onDidTapView?(type.role)
  }

  // MARK: - Setup

  private func setup() {
    setupContainer()
    setupAvatarImageView()
    setupTitleLabel()
    setupSubtitleLabel()
    setupDisclosureIndicatorImageView()
  }

  private func setupContainer() {
    backgroundColor = type.backgroundColor
    layer.cornerRadius = 8
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
  }

  private func setupAvatarImageView() {
    addSubview(avatarImageView)
    avatarImageView.contentMode = .scaleAspectFit
    avatarImageView.image = type.avatarImage
    avatarImageView.snp.makeConstraints { make in
      make.leading.equalToSuperview().inset(8)
      make.bottom.equalToSuperview()
      make.width.equalTo(78)
      make.height.equalTo(100)
    }
  }

  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .baseWhite
    titleLabel.numberOfLines = 0
    titleLabel.text = type.title
    titleLabel.snp.makeConstraints { make in
      make.top.equalToSuperview().inset(16)
      make.leading.equalTo(avatarImageView.snp.trailing).offset(16)
    }
  }

  private func setupSubtitleLabel() {
    addSubview(subtitleLabel)
    subtitleLabel.textColor = .baseWhite
    subtitleLabel.numberOfLines = 0
    subtitleLabel.text = type.subtitle
    subtitleLabel.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(4)
      make.bottom.equalToSuperview().inset(16)
      make.leading.equalTo(avatarImageView.snp.trailing).offset(16)
    }
  }

  private func setupDisclosureIndicatorImageView() {
    addSubview(disclosureIndicatorImageView)
    disclosureIndicatorImageView.image = R.image.disclosureIndicator()?.withTintColor(.baseWhite)
    disclosureIndicatorImageView.contentMode = .scaleAspectFit
    disclosureIndicatorImageView.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.centerY.equalToSuperview()
      make.trailing.equalToSuperview().inset(16)
      make.leading.equalTo(titleLabel.snp.trailing)
      make.leading.equalTo(subtitleLabel.snp.trailing)
    }
  }
}
