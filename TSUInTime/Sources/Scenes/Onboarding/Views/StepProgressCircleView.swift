//
//  StepProgressCircleView.swift
//  TSUInTime
//

import UIKit

class StepProgressCircleView: UIView {
  // MARK: - Properties

  var onDidTap: (() -> Void)?

  var title: String? {
    get {
      titleLabel.text
    }
    set {
      titleLabel.text = newValue
    }
  }

  var isActive: Bool = false {
    didSet {
      update()
    }
  }

  private let titleLabel = Label(textStyle: .bodyBold)

  // MARK: - Init

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  // MARK: - Actions

  @objc private func handleTap() {
    onDidTap?()
  }

  // MARK: - Setup

  private func setup() {
    setupContainer()
    setupTitleLabel()
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }

  private func setupContainer() {
    layer.cornerRadius = 16
    backgroundColor = .shade1
    snp.makeConstraints { make in
      make.size.equalTo(32)
    }
  }

  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .shade4
    titleLabel.snp.makeConstraints { make in
      make.center.equalToSuperview()
    }
  }

  // MARK: - Private methods

  private func update() {
    backgroundColor = isActive ? .accent : .shade1
    titleLabel.textColor = isActive ? .baseWhite : .shade4
  }
}
