//
//  OnboardingNotificationsViewController.swift
//  TSUInTime
//

import UIKit

class OnboardingNotificationsViewController: BaseViewController, NavigationBarHiding,
                                             ErrorHandling, ActivityIndicatorViewDisplaying,
                                             GoToSettingsAlertShowing {
  // MARK: - Properties
  
  let activityIndicatorView = ActivityIndicatorView()
  let activityIndicatorContainerView = UIView()
  
  private let titleLabel = Label(textStyle: .header1)
  private let descriptionLabel = Label(textStyle: .body)
  private let skipButton = CommonButton(style: .clear)
  private let enableButton = CommonButton(style: .default)
  private let placeholderImageView = UIImageView()
  
  private let viewModel: OnboardingNotificationsViewModel
  
  // MARK: - Init
  
  init(viewModel: OnboardingNotificationsViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }
  
  // MARK: - Actions
  
  @objc private func handleEnableButtonTap() {
    viewModel.enablePushNotifications()
  }
  
  @objc private func handleSkipButtonTap() {
    viewModel.skip()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupTitleLabel()
    setupDescriptionLabel()
    setupSkipButton()
    setupEnableButton()
    setupPlaceholderImageView()
    setupActivityIndicatorContainerView()
    setupActivityIndicatorView()
  }
  
  private func setupTitleLabel() {
    view.addSubview(titleLabel)
    titleLabel.textColor = .accent
    titleLabel.text = R.string.onboarding.notificationsTitle()
    titleLabel.setContentHuggingPriority(.required, for: .vertical)
    titleLabel.snp.makeConstraints { make in
      make.top.equalTo(view.safeAreaLayoutGuide).inset(24)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupDescriptionLabel() {
    view.addSubview(descriptionLabel)
    descriptionLabel.text = R.string.onboarding.notificationsDescription()
    descriptionLabel.textColor = .baseBlack
    descriptionLabel.numberOfLines = 0
    descriptionLabel.setContentHuggingPriority(.required, for: .vertical)
    descriptionLabel.snp.makeConstraints { make in
      make.top.equalTo(titleLabel.snp.bottom).offset(24)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupSkipButton() {
    view.addSubview(skipButton)
    skipButton.setTitle(R.string.onboarding.skipButtonTitle(), for: .normal)
    skipButton.setContentHuggingPriority(.required, for: .vertical)
    skipButton.addTarget(self, action: #selector(handleSkipButtonTap), for: .touchUpInside)
    skipButton.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.greaterThanOrEqualToSuperview().inset(40)
      make.bottom.equalTo(view.safeAreaLayoutGuide).inset(16).priority(750)
    }
  }
  
  private func setupEnableButton() {
    view.addSubview(enableButton)
    enableButton.setTitle(R.string.onboarding.notificationsEnableButtonTitle(), for: .normal)
    enableButton.setContentHuggingPriority(.required, for: .vertical)
    enableButton.addTarget(self, action: #selector(handleEnableButtonTap), for: .touchUpInside)
    enableButton.snp.makeConstraints { make in
      make.leading.trailing.equalToSuperview().inset(16)
      make.bottom.equalTo(skipButton.snp.top).offset(-8)
    }
  }
  
  private func setupPlaceholderImageView() {
    view.addSubview(placeholderImageView)
    placeholderImageView.image = R.image.notificationsImage()
    placeholderImageView.contentMode = .scaleAspectFit
    placeholderImageView.setContentHuggingPriority(.defaultLow, for: .vertical)
    placeholderImageView.snp.makeConstraints { make in
      make.width.equalTo(216)
      make.centerX.equalToSuperview()
      make.top.equalTo(descriptionLabel.snp.bottom).offset(24)
      make.bottom.equalTo(enableButton.snp.top).offset(-24)
    }
  }
  
  private func setupActivityIndicatorContainerView() {
    view.addSubview(activityIndicatorContainerView)
    activityIndicatorContainerView.backgroundColor = .baseWhite.withAlphaComponent(0.6)
    activityIndicatorContainerView.isHidden = true
    activityIndicatorContainerView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidStartRequest = { [weak self] in
      self?.activityIndicatorView.startAnimating()
      self?.activityIndicatorContainerView.isHidden = false
    }
    viewModel.onDidFinishRequest = { [weak self] in
      self?.activityIndicatorView.stopAnimating()
      self?.activityIndicatorContainerView.isHidden = true
    }
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error)
    }
    viewModel.onUserDidRestrictedNotifications = { [weak self] in
      self?.showNotificationsRestrictedAlert()
    }
  }
  
  // MARK: - Private methods
  
  private func showNotificationsRestrictedAlert() {
    showGoToSettingsAlert(title: R.string.onboarding.notificationsRestrictedAlertTitle(),
                          description: R.string.onboarding.notificationsRestrictedAlertDescription(),
                          cancelButtonTitle: R.string.onboarding.notificationsRestrictedAlertSkipOption())
  }
}
