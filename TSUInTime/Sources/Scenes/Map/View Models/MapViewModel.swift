//
//  MapViewModel.swift
//  TSUInTime
//

import Foundation
import MapKit

class MapViewModel: DataLoadingViewModel {
  typealias Dependencies = HasBuildingsService
  
  // MARK: - Properties
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?

  private(set) var annotations: [MKPointAnnotation] = []
  
  private var annotationsBuildings: [MKPointAnnotation: Building] = [:]
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies) {
    self.dependencies = dependencies
  }
  
  // MARK: - Public methods

  func loadData() {
    onDidStartRequest?()
    
    dependencies.buildingsService.getBuildings().ensure {
      self.onDidFinishRequest?()
    }.done { buildings in
      self.handle(buildings)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }

  func markerViewModel(annotation: MKPointAnnotation) -> MapMarkerViewModel {
    return MapMarkerViewModel(type: .universityBuilding, size: .big,
                              title: annotationsBuildings[annotation]?.name,
                              subtitle: annotationsBuildings[annotation]?.address)
  }
  
  // MARK: - Private methods
  
  private func handle(_ buildings: [Building]) {
    annotationsBuildings.removeAll()
    annotations = buildings.compactMap { building in
      guard let coordinate = building.coordinate else { return nil }
      let annotation = MKPointAnnotation()
      annotation.coordinate = coordinate
      annotationsBuildings[annotation] = building
      return annotation
    }
  }
}
