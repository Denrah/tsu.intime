//
//  SearchProfessorViewModel.swift
//  TSUInTime
//

import Foundation

protocol SearchViewModelDelegate: AnyObject {
  func searchViewModel(_ viewModel: SearchViewModel, didSelect faculty: Faculty)
  func searchViewModel(_ viewModel: SearchViewModel, didSelect group: Group, faculty: Faculty)
  func searchViewModel(_ viewModel: SearchViewModel, didSelect professor: Professor)
  func searchViewModel(_ viewModel: SearchViewModel, didSelect building: Building)
  func searchViewModel(_ viewModel: SearchViewModel, didSelect auditory: Auditory)
}

extension SearchViewModelDelegate {
  func searchViewModel(_ viewModel: SearchViewModel, didSelect faculty: Faculty) {}
  func searchViewModel(_ viewModel: SearchViewModel, didSelect group: Group) {}
  func searchViewModel(_ viewModel: SearchViewModel, didSelect professor: Professor) {}
  func searchViewModel(_ viewModel: SearchViewModel, didSelect building: Building) {}
  func searchViewModel(_ viewModel: SearchViewModel, didSelect auditory: Auditory) {}
}

class SearchViewModel {
  typealias Dependencies = HasFacultiesService & HasProfessorsService & HasBuildingsService
  
  // MARK: - Properties

  weak var delegate: SearchViewModelDelegate?
  
  var onDidRequestToEndEditing: (() -> Void)?
  
  let searchType: SearchType
  let searchTableViewModel: DataLoadingViewModel & SearchableTableViewModel
  
  private let dependencies: Dependencies
  
  // MARK: - Init

  init(dependencies: Dependencies, searchType: SearchType) {
    self.dependencies = dependencies
    self.searchType = searchType
    switch searchType {
    case .faculties:
      self.searchTableViewModel = FacultiesSearchiewModel(dependencies: dependencies)
      (self.searchTableViewModel as? FacultiesSearchiewModel)?.delegate = self
    case .groups(let faculty):
      self.searchTableViewModel = GroupsSearchViewModel(dependencies: dependencies, title: faculty.name)
      (self.searchTableViewModel as? GroupsSearchViewModel)?.delegate = self
      (self.searchTableViewModel as? GroupsSearchViewModel)?.update(faculty: faculty, reloadData: false)
    case .professors:
      self.searchTableViewModel = ProfessorsSearchViewModel(dependencies: dependencies)
      (self.searchTableViewModel as? ProfessorsSearchViewModel)?.delegate = self
    case .buildings:
      self.searchTableViewModel = BuildingsSearchViewModel(dependencies: dependencies)
      (self.searchTableViewModel as? BuildingsSearchViewModel)?.delegate = self
    case .auditories(let building):
      self.searchTableViewModel = AuditoriesSearchViewModel(dependencies: dependencies, title: building.name)
      (self.searchTableViewModel as? AuditoriesSearchViewModel)?.delegate = self
      (self.searchTableViewModel as? AuditoriesSearchViewModel)?.update(buildingID: building.id, reloadData: false)
    }
  }
}

// MARK: - FacultiesSearchViewModelDelegate

extension SearchViewModel: FacultiesSearchViewModelDelegate {
  func facultiesSearchViewModel(_ viewModel: FacultiesSearchiewModel, didSelect faculty: Faculty) {
    delegate?.searchViewModel(self, didSelect: faculty)
    onDidRequestToEndEditing?()
  }
}

// MARK: - GroupsSearchViewModelDelegate

extension SearchViewModel: GroupsSearchViewModelDelegate {
  func groupsSearchViewModel(_ viewModel: GroupsSearchViewModel, didSelect group: Group, faculty: Faculty) {
    delegate?.searchViewModel(self, didSelect: group, faculty: faculty)
    onDidRequestToEndEditing?()
  }
}

// MARK: - ProfessorsSearchViewModelDelegate

extension SearchViewModel: ProfessorsSearchViewModelDelegate {
  func professorsSearchViewModel(_ viewModel: ProfessorsSearchViewModel, didSelect professor: Professor) {
    delegate?.searchViewModel(self, didSelect: professor)
    onDidRequestToEndEditing?()
  }
}

// MARK: - BuildingsSearchViewModelDelegate

extension SearchViewModel: BuildingsSearchViewModelDelegate {
  func buildingsSearchViewModel(_ viewModel: BuildingsSearchViewModel, didSelect building: Building) {
    delegate?.searchViewModel(self, didSelect: building)
    onDidRequestToEndEditing?()
  }
}

// MARK: - AuditoriesSearchViewModelDelegate

extension SearchViewModel: AuditoriesSearchViewModelDelegate {
  func auditoriesSearchViewModel(_ viewModel: AuditoriesSearchViewModel, didSelect auditory: Auditory) {
    delegate?.searchViewModel(self, didSelect: auditory)
    onDidRequestToEndEditing?()
  }
}
