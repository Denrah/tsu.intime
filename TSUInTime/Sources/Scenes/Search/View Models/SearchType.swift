//
//  SearchType.swift
//  TSUInTime
//

import Foundation

enum SearchType {
  case faculties, groups(faculty: Faculty), professors, buildings, auditories(building: Building)
  
  var screenTitle: String {
    switch self {
    case .faculties, .groups:
      return R.string.search.groupsSearchScreenTitle()
    case .professors:
      return R.string.search.professorsSearchScreenTitle()
    case .buildings, .auditories:
      return R.string.search.auditoriesSearchScreenTitle()
    }
  }
}
