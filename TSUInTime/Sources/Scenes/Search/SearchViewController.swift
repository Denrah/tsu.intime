//
//  SearchProfessorViewController.swift
//  TSUInTime
//

import UIKit

class SearchViewController: BaseViewController {
  // MARK: - Properties
  
  private let tableView: UIView
  
  private let viewModel: SearchViewModel
  
  // MARK: - Init
  
  init(viewModel: SearchViewModel) {
    self.viewModel = viewModel
    switch viewModel.searchType {
    case .faculties:
      self.tableView = SearchableTableView<FacultyListItemCell>(viewModel: viewModel.searchTableViewModel)
    default:
      self.tableView = SearchableTableView<SimpleListItemCell>(viewModel: viewModel.searchTableViewModel)
    }
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bindToViewModel()
  }
  
  // MARK: - Setup

  private func setup() {
    view.addSubview(tableView)
    tableView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  // MARK: - Bind
  
  private func bindToViewModel() {
    viewModel.onDidRequestToEndEditing = { [weak self] in
      self?.view.endEditing(true)
    }
  }
}
