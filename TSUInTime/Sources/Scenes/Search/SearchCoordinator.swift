//
//  SearchCoordinator.swift
//  TSUInTime
//

import UIKit

struct SearchCoordinatorConfiguration {
  let searchType: SearchType
}

final class SearchCoordinator: ConfigurableCoordinator {
  typealias Configuration = SearchCoordinatorConfiguration
  
  // MARK: - Properties

  var childCoordinators: [Coordinator] = []
  var onDidFinish: (() -> Void)?

  let navigationController: NavigationController
  let appDependency: AppDependency
  
  private let configuration: Configuration
  
  // MARK: - Init

  init(navigationController: NavigationController, appDependency: AppDependency, configuration: Configuration) {
    self.navigationController = navigationController
    self.appDependency = appDependency
    self.configuration = configuration
  }
  
  // MARK: - Navigation

  func start(animated: Bool) {
    let viewModel = SearchViewModel(dependencies: appDependency, searchType: configuration.searchType)
    viewModel.delegate = self
    let viewController = SearchViewController(viewModel: viewModel)
    viewController.title = configuration.searchType.screenTitle
    addPopObserver(for: viewController)
    navigationController.pushViewController(viewController, animated: animated)
  }

  private func showTimetable(timetableInfo: TimetableInfo) {
    let configuration = TimetableCoordinatorConfiguration(timetableInfo: timetableInfo)
    show(TimetableCoordinator.self, configuration: configuration, animated: true)
  }
}

// MARK: - SearchViewModelDelegate

extension SearchCoordinator: SearchViewModelDelegate {
  func searchViewModel(_ viewModel: SearchViewModel, didSelect faculty: Faculty) {
    let configuration = SearchCoordinatorConfiguration(searchType: .groups(faculty: faculty))
    show(SearchCoordinator.self, configuration: configuration, animated: true)
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect group: Group, faculty: Faculty) {
    showTimetable(timetableInfo: TimetableInfo(timetableName: group.name, timetableType: .group,
                                               timetableID: group.id,
                                               bottomBarConfiguration: .additionalTimetable))
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect professor: Professor) {
    showTimetable(timetableInfo: TimetableInfo(timetableName: professor.name, timetableType: .professor,
                                               timetableID: professor.id,
                                               bottomBarConfiguration: .additionalTimetable))
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect building: Building) {
    let configuration = SearchCoordinatorConfiguration(searchType: .auditories(building: building))
    show(SearchCoordinator.self, configuration: configuration, animated: true)
  }
  
  func searchViewModel(_ viewModel: SearchViewModel, didSelect auditory: Auditory) {
    showTimetable(timetableInfo: TimetableInfo(timetableName: auditory.name, timetableType: .auditory,
                                               timetableID: auditory.id,
                                               bottomBarConfiguration: .additionalTimetable))
  }
}
