//
//  NetworkService+Professors.swift
//  TSUInTime
//

import PromiseKit

extension NetworkService: ProfessorsNetworkProtocol {
  func getProfessors() -> Promise<[Professor]> {
    return request(method: .get, url: URLFactory.Professors.professors)
  }
}
