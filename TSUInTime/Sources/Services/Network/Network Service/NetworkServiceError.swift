//
//  NetworkServiceError.swift
//  TSUInTime
//

import Foundation

enum NetworkServiceError: LocalizedError {
  case failedToDecodeData
  case noData
  case requestFailed
  
  var errorDescription: String? {
    switch self {
    case .failedToDecodeData, .noData:
      return R.string.errors.failedToDecodeDataErrorText()
    case .requestFailed:
      return R.string.errors.requestFailedErrorText()
    }
  }
}
