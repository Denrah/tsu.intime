//
//  NetworkService.swift
//  TSUInTime
//

import Alamofire
import PromiseKit
import Foundation

class NetworkService {
  private let crashReportService: CrashReportServiceProtocol
  
  init(crashReportService: CrashReportServiceProtocol) {
    self.crashReportService = crashReportService
  }
  
  func request<T: Decodable>(method: HTTPMethod, url: URLConvertible,
                             parameters: Parameters? = nil) -> Promise<T> {
    let request = AF.request(url, method: method, parameters: parameters,
                             encoding: method == .get ? URLEncoding.default : JSONEncoding.default)
    
    return Promise { seal in
      firstly {
        request.responseDataPromise()
      }.then { response in
        self.handleResponse(response)
      }.done { object in
        seal.fulfill(object)
      }.catch { error in
        if !NetworkServiceErrorUtility.isNoInternetError(error) {
          self.crashReportService.report(error: .requestFailed(error: error, method: method,
                                                               url: url, parameters: parameters))
        }
        seal.reject(error)
      }
    }
  }
  
  private func handleResponse<T: Decodable>(_ response: AFDataResponse<Data>) -> Promise<T> {
    logResponse(response)
    
    return Promise { seal in
      if case .failure(let error) = response.result {
        seal.reject(error.underlyingError ?? error)
        return
      }
      
      let statusCode: HTTPStatusCode
      if let code = response.response?.statusCode {
        statusCode = HTTPStatusCode(rawValue: code) ?? .internalServerError
      } else {
        statusCode = .internalServerError
      }
      
      switch statusCode {
      case .okStatus, .created, .accepted, .noContent:
        if let data = response.data {
          decode(data, ofType: T.self).done { object in
            seal.fulfill(object)
          }.catch { error in
            seal.reject(error)
          }
        } else if T.self == EmptyResponse.self, let response = EmptyResponse() as? T {
          seal.fulfill(response)
        } else {
          seal.reject(NetworkServiceError.noData)
        }
      default:
        seal.reject(NetworkServiceError.requestFailed)
      }
    }
  }
  
  private func decode<T: Decodable>(_ data: Data, ofType type: T.Type) -> Promise<T> {
    return Promise { seal in
      do {
        let object = try JSONDecoder().decode(type.self, from: data)
        seal.fulfill(object)
      } catch {
        print(error)
        crashReportService.report(error: .requestFailed(error: error, method: nil, url: nil, parameters: nil))
        seal.reject(NetworkServiceError.failedToDecodeData)
      }
    }
  }
  
  private func logResponse(_ response: AFDataResponse<Data>) {
    print("----------------------------------------")
    print("\(response.request?.method?.rawValue ?? "") \(response.request?.url?.absoluteString ?? "")")
    print("----------------------------------------")
    switch response.result {
    case .success(let responseData):
      if let object = try? JSONSerialization.jsonObject(with: responseData, options: []),
         let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
         let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
        print(prettyPrintedString)
      }
    case .failure(let error):
      print(error)
    }
    print("----------------------------------------")
  }
}
