//
//  NetworkService+Faculties.swift
//  TSUInTime
//

import PromiseKit

extension NetworkService: FacultiesNetworkProtocol {
  func getFaculties() -> Promise<[Faculty]> {
    return request(method: .get, url: URLFactory.Faculties.faculties)
  }
  
  func getGroups(facultyID: String) -> Promise<[Group]> {
    return request(method: .get, url: URLFactory.Faculties.groups(facultyID: facultyID))
  }
}
