//
//  NetworkProtocols.swift
//  TSUInTime
//

import Foundation
import PromiseKit

protocol FacultiesNetworkProtocol {
  func getFaculties() -> Promise<[Faculty]>
  func getGroups(facultyID: String) -> Promise<[Group]>
}

protocol ProfessorsNetworkProtocol {
  func getProfessors() -> Promise<[Professor]>
}

protocol BuildingsNetworkProtocol {
  func getBuildings() -> Promise<[Building]>
  func getAuditories(buildingID: String) -> Promise<[Auditory]>
}

protocol ScheduleNetworkProtocol {
  func getDaysTimetable(type: TimetableType, id: String, startDate: Date, endDate: Date) -> Promise<[TimetableDay]>
  func getWeekTimetable(type: TimetableType, id: String, startDate: Date, endDate: Date) -> Promise<TimetableWeek>
}

protocol FeedbackNetworkProtocol {
  func sendFeedback(parameters: FeedbackRequestParameters) -> Promise<EmptyResponse>
}
