//
//  NetworkService+Feedback.swift
//  TSUInTime
//

import Alamofire
import Foundation
import PromiseKit

extension NetworkService: FeedbackNetworkProtocol {
  private struct Keys {
    static let text = "text"
    static let osVersion = "osVersion"
    static let appVersion = "appVersion"
    static let deviceModel = "deviceModel"
    static let platform = "platform"
  }
  
  func sendFeedback(parameters: FeedbackRequestParameters) -> Promise<EmptyResponse> {
    let requestParameters: Parameters = [
      Keys.text: parameters.text,
      Keys.osVersion: parameters.osVersion,
      Keys.appVersion: parameters.appVersion,
      Keys.deviceModel: parameters.deviceModel,
      Keys.platform: parameters.platform
    ]
    return request(method: .post, url: URLFactory.Feedback.feedback, parameters: requestParameters)
  }
}
