//
//  URLFactory.swift
//  TSUInTime
//

import Foundation

struct URLFactory {
  private static let baseURL = "https://test.intime.kreosoft.space/api/mobile"
  
  struct APIVersion {
    // swiftlint:disable identifier_name
    static let v1 = "/v1"
    static let v2 = "/v2"
    static let v3 = "/v3"
  }
  
  struct Faculties {
    static let faculties = baseURL + APIVersion.v1 + "/faculties"
    static func groups(facultyID: String) -> String {
      return faculties + "/\(facultyID)/groups"
    }
  }
  
  struct Professors {
    static let professors = baseURL + APIVersion.v1 + "/professors"
  }
  
  struct Buildings {
    static let buildings = baseURL + APIVersion.v1 + "/buildings"
    static func auditories(buildingID: String) -> String {
      return buildings + "/\(buildingID)/audiences"
    }
  }

  struct Timetable {
    static func dayScheduleTimetable() -> String {
      return baseURL + APIVersion.v3 + "/schedule/day"
    }
    static func weekScheduleTimetable() -> String {
      return baseURL + APIVersion.v3 + "/schedule/week"
    }
  }
  
  struct Feedback {
    static let feedback = baseURL + APIVersion.v1 + "/feedback"
  }
}
