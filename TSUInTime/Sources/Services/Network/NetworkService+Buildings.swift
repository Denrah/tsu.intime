//
//  NetworkService+Buildings.swift
//  TSUInTime
//

import PromiseKit

extension NetworkService: BuildingsNetworkProtocol {
  func getBuildings() -> Promise<[Building]> {
    return request(method: .get, url: URLFactory.Buildings.buildings)
  }
  
  func getAuditories(buildingID: String) -> Promise<[Auditory]> {
    return request(method: .get, url: URLFactory.Buildings.auditories(buildingID: buildingID))
  }
}
