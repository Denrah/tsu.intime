//
//  FeedbackRequestParameters.swift
//  TSUInTime
//

import Foundation

struct FeedbackRequestParameters {
  let text: String
  let osVersion: String
  let appVersion: String
  let deviceModel: String
  let platform: String
}
