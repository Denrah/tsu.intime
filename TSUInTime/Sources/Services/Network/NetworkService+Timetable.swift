//
//  NetworkService+Timetable.swift
//  TSUInTime
//

import Foundation
import PromiseKit
import Alamofire

extension NetworkService: ScheduleNetworkProtocol {
  private struct Keys {
    static let scheduleType = "scheduleType"
    static let id = "id"
    static let startDate = "dateFrom"
    static let endDate = "dateTo"
  }
  
  func getDaysTimetable(type: TimetableType, id: String, startDate: Date, endDate: Date) -> Promise<[TimetableDay]> {
    let formatter = DateFormatter.yearMonthDayISO
    let parameters: Parameters = [
      Keys.scheduleType: type.rawValue,
      Keys.id: id,
      Keys.startDate: formatter.string(from: startDate),
      Keys.endDate: formatter.string(from: endDate)
    ]
    return request(method: .get, url: URLFactory.Timetable.dayScheduleTimetable(),
                   parameters: parameters)
  }
  
  func getWeekTimetable(type: TimetableType, id: String, startDate: Date, endDate: Date) -> Promise<TimetableWeek> {
    let formatter = DateFormatter.yearMonthDayISO
    let parameters: Parameters = [
      Keys.scheduleType: type.rawValue,
      Keys.id: id,
      Keys.startDate: formatter.string(from: startDate),
      Keys.endDate: formatter.string(from: endDate)
    ]
    return request(method: .get, url: URLFactory.Timetable.weekScheduleTimetable(), parameters: parameters)
  }
}
