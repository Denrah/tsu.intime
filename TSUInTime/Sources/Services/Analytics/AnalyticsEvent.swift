//
//  AnalyticsEvent.swift
//  TSUInTime
//

import Foundation

enum AnalyticsEvent: String {
  case openLessonDetails = "open_lesson_details"
  case openCampusMap = "open_campus_map"
  case openFavsTimetable = "open_favs_timetable"
  case openFavsMenu = "open_favs_menu"
  case calendarExport = "calendar_export"
}
