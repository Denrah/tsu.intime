//
//  AnalyticsService.swift
//  TSUInTime
//

import Foundation
import FirebaseAnalytics

protocol AnalyticsServiceProtocol {
  func logEvent(_ event: AnalyticsEvent)
}

class AnalyticsService: AnalyticsServiceProtocol {
  func logEvent(_ event: AnalyticsEvent) {
    Analytics.logEvent(event.rawValue, parameters: nil)
  }
}
