//
//  StorageServiceProtocols.swift
//  TSUInTime
//

import Foundation

protocol FavoritesStorageProtocol {
  func saveTimetable(timetableInfo: TimetableInfo) throws
  func getTimetables() throws -> [TimetableInfo]
  func deleteTimetable(id: String) throws
  func deleteAllTimetables() throws
  func isTimetableInFavorites(id: String) -> Bool
}
