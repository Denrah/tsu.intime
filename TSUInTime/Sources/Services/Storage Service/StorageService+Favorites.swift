//
//  StorageService+Favorites.swift
//  TSUInTime
//

import Foundation

extension StorageService: FavoritesStorageProtocol {
  private struct Keys {
    static let favorites = "favorites"
  }

  func saveTimetable(timetableInfo: TimetableInfo) throws {
    var storedTimetables = (try? getObject(ofType: [TimetableInfo].self, key: Keys.favorites)) ?? []
    guard !storedTimetables.contains(where: { $0.timetableID == timetableInfo.timetableID }) else { return }
    storedTimetables.append(timetableInfo)
    try saveObject(storedTimetables, key: Keys.favorites)
  }

  func getTimetables() throws -> [TimetableInfo] {
    return try getObject(ofType: [TimetableInfo].self, key: Keys.favorites)
  }

  func deleteTimetable(id: String) throws {
    var storedTimetables = (try? getObject(ofType: [TimetableInfo].self, key: Keys.favorites)) ?? []
    storedTimetables.removeAll { $0.timetableID == id }
    try saveObject(storedTimetables, key: Keys.favorites)
  }

  func deleteAllTimetables() throws {
    try saveObject([] as [TimetableInfo], key: Keys.favorites)
  }

  func isTimetableInFavorites(id: String) -> Bool {
    let storedTimetables = (try? getObject(ofType: [TimetableInfo].self, key: Keys.favorites)) ?? []
    return storedTimetables.contains { $0.timetableID == id }
  }
}
