//
//  CrashReportError.swift
//  TSUInTime
//

import Foundation
import Alamofire

private extension Constants {
  static let crashReportErrorDomain = "CustomNonFatalError"
}

enum CrashReportError {
  case lessonWithUnknownStartTime(lesson: Lesson)
  case differentTimetableNames(timetableInfo: TimetableInfo, receivedName: String?)
  case requestFailed(error: Error, method: HTTPMethod?, url: URLConvertible?, parameters: Parameters?)
  
  var domain: String {
    Constants.crashReportErrorDomain
  }
  
  var code: Int {
    switch self {
    case .lessonWithUnknownStartTime:
      return 0
    case .differentTimetableNames:
      return 1
    case .requestFailed:
      return 2
    }
  }
  
  var userInfo: [String: Any]? {
    switch self {
    case .lessonWithUnknownStartTime(let lesson):
      return [
        "Type": "Unknown lesson start time",
        "Lesson": lesson
      ]
    case .differentTimetableNames(let timetableInfo, let receivedName):
      return [
        "Type": "Different timetable names",
        "Timetable info": timetableInfo,
        "Received name": receivedName ?? "null"
      ]
    case .requestFailed(let error, let method, let url, let parameters):
      return [
        "Type": "Request failed",
        "Error": error,
        "Methods": method ?? "null",
        "URL": url ?? "null",
        "Parameters": parameters ?? "null"
      ]
    }
  }
}
