//
//  CrashReportService.swift
//  TSUInTime
//

import Foundation
import FirebaseCrashlytics

protocol CrashReportServiceProtocol {
  func report(error: CrashReportError)
}

class CrashReportService: CrashReportServiceProtocol {
  func report(error: CrashReportError) {
    let nsError = NSError(domain: error.domain, code: error.code, userInfo: error.userInfo)
    Crashlytics.crashlytics().record(error: nsError)
  }
}
