//
//  DataStoreService.swift
//  TSUInTime
//

import Foundation

class DataStoreService: DataStoreProtocol {
  private struct UserDefaultsKeys {
    static let hasSentFeedback = "hasSentReview"
    static let nextFeedbackRequestDate = "nextFeedbackRequestDate"
    static let userInfo = "userInfo"
    static let preferredDisplayType = "preferredDisplayType"
    static let ekCalendarInfo = "EKCalendarInfo"
    static let timetableUpdatesNotificationsEnabled = "timetableUpdatesNotificationsEnabled"
    static let firebaseSubscribedTopics = "firebaseSubscribedTopics"
  }
  
  let observer = DataStoreObserver()
  
  private let userDefaults = UserDefaults.standard
  
  // MARK: - UserDefaults
  
  var hasSentFeedback: Bool {
    get {
      userDefaults.bool(forKey: UserDefaultsKeys.hasSentFeedback)
    }
    set {
      userDefaults.set(newValue, forKey: UserDefaultsKeys.hasSentFeedback)
    }
  }
  
  var nextFeedbackRequestDate: Date? {
    get {
      userDefaults.object(forKey: UserDefaultsKeys.nextFeedbackRequestDate) as? Date
    }
    set {
      userDefaults.set(newValue, forKey: UserDefaultsKeys.nextFeedbackRequestDate)
    }
  }
  
  var userInfo: UserInfo? {
    get {
      guard let data = userDefaults.data(forKey: UserDefaultsKeys.userInfo) else { return nil }
      return try? JSONDecoder().decode(UserInfo.self, from: data)
    }
    set {
      let data = (try? JSONEncoder().encode(newValue)) ?? Data()
      userDefaults.set(data, forKey: UserDefaultsKeys.userInfo)
      observer.notify(event: .userInfoUpdated)
    }
  }
  
  var preferredDisplayType: TimetableDisplayType? {
    get {
      guard let rawValue = userDefaults.value(forKey: UserDefaultsKeys.preferredDisplayType) as? String,
            let displayType = TimetableDisplayType(rawValue: rawValue) else { return nil }
      return displayType
    }
    set {
      userDefaults.set(newValue?.rawValue, forKey: UserDefaultsKeys.preferredDisplayType)
      observer.notify(event: .preferredDisplayTypeUpdated)
    }
  }
  
  var ekCalendarInfo: EKCalendarInfo? {
    get {
      guard let data = userDefaults.data(forKey: UserDefaultsKeys.ekCalendarInfo) else { return nil }
      return try? JSONDecoder().decode(EKCalendarInfo.self, from: data)
    }
    set {
      let data = (try? JSONEncoder().encode(newValue)) ?? Data()
      userDefaults.set(data, forKey: UserDefaultsKeys.ekCalendarInfo)
    }
  }
  
  var timetableUpdatesNotificationsEnabled: Bool {
    get {
      userDefaults.bool(forKey: UserDefaultsKeys.timetableUpdatesNotificationsEnabled)
    }
    set {
      userDefaults.set(newValue, forKey: UserDefaultsKeys.timetableUpdatesNotificationsEnabled)
    }
  }
  
  var firebaseSubscribedTopics: Set<String>? {
    get {
      guard let data = userDefaults.data(forKey: UserDefaultsKeys.firebaseSubscribedTopics) else { return nil }
      return try? JSONDecoder().decode(Set<String>.self, from: data)
    }
    set {
      let data = (try? JSONEncoder().encode(newValue)) ?? Data()
      userDefaults.set(data, forKey: UserDefaultsKeys.firebaseSubscribedTopics)
    }
  }
  
  // MARK: - Public methods
  
  func clearAllData() {
    userInfo = nil
  }
}
