//
//  DataStoreProtocol.swift
//  TSUInTime
//

import Foundation

protocol DataStoreProtocol: AnyObject {
  var observer: DataStoreObserver { get }
  
  var hasSentFeedback: Bool { get set }
  var nextFeedbackRequestDate: Date? { get set }
  var userInfo: UserInfo? { get set }
  var preferredDisplayType: TimetableDisplayType? { get set }
  var ekCalendarInfo: EKCalendarInfo? { get set }
  var timetableUpdatesNotificationsEnabled: Bool { get set }
  var firebaseSubscribedTopics: Set<String>? { get set }

  func clearAllData()
}
