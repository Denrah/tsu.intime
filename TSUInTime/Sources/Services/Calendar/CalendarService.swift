//
//  CalendarService.swift
//  TSUInTime
//

import Foundation
import PromiseKit

private extension Constants {
  static let titleSeparator = " – "
  static let groupsSeparator = ", "
}

final class CalendarService: CalendarServiceProtocol {
  private let calendarManager: CalendarManager
  private let timetableNetworkProtocol: ScheduleNetworkProtocol
  private let dataStoreService: DataStoreProtocol
  
  init(timetableNetworkProtocol: ScheduleNetworkProtocol,
       dataStoreService: DataStoreProtocol) {
    self.timetableNetworkProtocol = timetableNetworkProtocol
    self.dataStoreService = dataStoreService
    self.calendarManager = CalendarManager(dataStoreService: dataStoreService)
  }
  
  var authorizationStatus: EKCalendarAuthorizationState {
    calendarManager.authorizationStatus
  }
  
  func exportToCalendar(startDate: Date, endDate: Date) -> Promise<Void> {
    guard let safeUserInfo = dataStoreService.userInfo else {
      return Promise<Void>(error: CalendarServiceError.unexpectedError)
    }
    
    return Promise { seal in
      firstly {
        timetableNetworkProtocol.getDaysTimetable(type: safeUserInfo.timetableType, id: safeUserInfo.timetableID,
                                                  startDate: startDate, endDate: endDate)
      }.then { timetable in
        self.exportToCalendar(timetables: timetable)
      }.done {
        seal.fulfill(())
      }.catch { error in
        seal.reject(error)
      }
    }
  }
  
  // MARK: - Helpers
  
  private func exportToCalendar(timetables: [TimetableDay]) -> Promise<Void> {
    let calendarEvents = generateTransientCalendarEvents(from: timetables)
    
    guard !calendarEvents.isEmpty else {
      return Promise(error: CalendarServiceError.nothingToExport)
    }
    
    return Promise { seal in
      firstly {
        calendarManager.requestCalendarAccess()
      }.then { isGranted -> Promise<Void> in
        guard isGranted else {
          return Promise(error: CalendarServiceError.accessIsDenied)
        }
        return self.calendarManager.exportToCalendar(events: calendarEvents)
      }.done { _ in
        seal.fulfill(())
      }.catch { error in
        seal.reject(error)
      }
    }
  }
  
  private func generateTransientCalendarEvents(from timetables: [TimetableDay]) -> [TransientCalendarEvent] {
    var result: [TransientCalendarEvent] = []
    
    for timetableDay in timetables {
      for lessonEntity in timetableDay.timeSlots {
        let safeLesson: Lesson
        switch lessonEntity {
        case .lesson(let lesson):
          safeLesson = lesson
        default:
          continue
        }
        
        guard let lessonStartDate = timetableDay.dateGMT0?.addingTimeInterval(safeLesson.startTime),
              let lessonEndDate = timetableDay.dateGMT0?.addingTimeInterval(safeLesson.endTime) else {
          continue
        }
        
        let lessonTitle = makeLessonTitle(with: safeLesson)
        
        result.append(TransientCalendarEvent(startDate: lessonStartDate,
                                             endDate: lessonEndDate,
                                             calendarEventTitle: lessonTitle))
      }
    }
    
    return result
  }
  
  private func makeLessonTitle(with lesson: Lesson) -> String {
    return lesson.title + Constants.titleSeparator
    + lesson.groups.map(\.name).joined(separator: Constants.groupsSeparator)
  }
}
