//
//  CalendarManager.swift
//  TSUInTime
//

import UIKit
import EventKit
import PromiseKit

final class CalendarManager {
  private let dataStoreService: DataStoreProtocol
  private let eventStore = EKEventStore()
  private var inTimeCalendar: EKCalendar?
  
  private var inTimeCalendarID: String? {
    get {
      dataStoreService.ekCalendarInfo?.EKCalendarID
    }
    set {
      guard let safeValue = newValue else { return }
      dataStoreService.ekCalendarInfo = EKCalendarInfo(EKCalendarID: safeValue)
    }
  }
  
  init(dataStoreService: DataStoreProtocol) {
    self.dataStoreService = dataStoreService
  }
  
  var authorizationStatus: EKCalendarAuthorizationState {
    switch EKEventStore.authorizationStatus(for: .event) {
    case .notDetermined:
      return .notDetermined
    case .restricted:
      return .denied
    case .denied:
      return .denied
    case .authorized:
      return .authorized
    @unknown default:
      return .notDetermined
    }
  }
  
  func requestCalendarAccess() -> Promise<Bool> {
    return Promise { seal in
      eventStore.requestAccess(to: .event) { isGranted, error in
        if let error = error {
          seal.reject(error)
          return
        }
        seal.fulfill(isGranted)
      }
    }
  }
  
  func exportToCalendar(events: [TransientCalendarEvent]) -> Promise<Void> {
    guard EKEventStore.authorizationStatus(for: .event) != .denied else {
      return Promise<Void>(error: CalendarServiceError.accessIsDenied)
    }
    
    return Promise { seal in
      eventStore.requestAccess(to: .event) { granted, _ in
        guard granted else {
          seal.reject(CalendarServiceError.accessIsDenied)
          return
        }
        
        do {
          try self.setupCalendar()
          try self.addEventsToCalendar(events)
          seal.fulfill(())
        } catch {
          seal.reject(CalendarServiceError.unexpectedError)
        }
      }
    }
    
  }
  
  // MARK: - Helpers
  private func setupCalendar() throws {
    guard let calendarID = inTimeCalendarID,
          let calendar = eventStore.calendar(withIdentifier: calendarID)
    else {
      inTimeCalendar = try createCalendar()
      inTimeCalendarID = inTimeCalendar?.calendarIdentifier
      return
    }
    inTimeCalendar = calendar
  }
  
  private func removeIrrelevantPersistentEvents(startSearchDate: Date, endSearchDate: Date) throws {
    let searchPredicate = try generateSearchEventsPredicate(startSearchDate: startSearchDate,
                                                            endSearchDate: endSearchDate)
    let exisitingPersistentEvents = eventStore.events(matching: searchPredicate)
    for event in exisitingPersistentEvents {
      try eventStore.remove(event, span: .thisEvent)
    }
  }
  
  private func addEventsToCalendar(_ events: [TransientCalendarEvent]) throws {
    guard let startSearchDate = events.min(by: { $0.startDate < $1.startDate })?.startDate,
          let endSearchDate = events.max(by: { $0.endDate < $1.endDate })?.endDate else {
            throw CalendarServiceError.unexpectedError
          }
    let predicate = try generateSearchEventsPredicate(startSearchDate: startSearchDate,
                                                      endSearchDate: endSearchDate)
    let exisitingPersistentEvents = eventStore.events(matching: predicate)
    
    var setForCheck: Set<ComparableCalendarEvent> = Set()
    for event in exisitingPersistentEvents {
      let comparableCalendarEvent = ComparableCalendarEvent(startDate: event.startDate,
                                                            endDate: event.endDate,
                                                            calendarEventTitle: event.title)
      setForCheck.update(with: comparableCalendarEvent)
    }
    
    for event in events {
      guard !setForCheck.contains(event.asComparableCalendarEvent()) else { continue }
      try addEventToCalendar(event)
    }
  }
  
  private func addEventToCalendar(_ event: TransientCalendarEvent) throws {
    let persistentEvent = EKEvent(eventStore: eventStore)
    persistentEvent.calendar = inTimeCalendar
    persistentEvent.startDate = event.startDate
    persistentEvent.endDate = event.endDate
    persistentEvent.isAllDay = false
    persistentEvent.title = event.calendarEventTitle
    guard event.needsAlarm else {
      try eventStore.save(persistentEvent, span: .thisEvent)
      return
    }
    let offset = generateOffsetInMinutesFromValue(event.alarmOffsetInMinutes)
    persistentEvent.addAlarm(EKAlarm(relativeOffset: offset))
    try eventStore.save(persistentEvent, span: .thisEvent)
  }
  
  private func createCalendar() throws -> EKCalendar {
    let calendar = EKCalendar(for: .event, eventStore: eventStore)
    calendar.title = R.string.ekCalendar.calendarName()
    calendar.cgColor = UIColor.accentDark.cgColor
    guard let source = bestPossibleEKSource() else {
      throw CalendarServiceError.unexpectedError
    }
    calendar.source = source
    try eventStore.saveCalendar(calendar, commit: true)
    return calendar
  }
  
  private func bestPossibleEKSource() -> EKSource? {
    let defaultSource = eventStore.defaultCalendarForNewEvents?.source
    // this is fragile, user can rename the source
    let iCloudSource = eventStore.sources.first { $0.title == Constants.iCloudSource }
    let localSource = eventStore.sources.first { $0.sourceType == .local }
    
    return defaultSource ?? iCloudSource ?? localSource
  }
  
  private func generateSearchEventsPredicate(startSearchDate: Date, endSearchDate: Date) throws -> NSPredicate {
    let predicate: NSPredicate
    guard let safeCalendar = inTimeCalendar else {
      throw CalendarServiceError.unexpectedError
    }
    predicate = eventStore.predicateForEvents(withStart: startSearchDate,
                                              end: endSearchDate,
                                              calendars: [safeCalendar])
    return predicate
  }
  
  private func generateOffsetInMinutesFromValue(_ value: Int) -> TimeInterval {
    return TimeInterval(value * 60)
  }
  
}

// MARK: - Constants
private extension Constants {
  static let iCloudSource = "iCloud"
}
