//
//  TransientCalendarEvent+ComparableCalendarEvent.swift
//  TSUInTime
//

import Foundation

struct TransientCalendarEvent {
  let startDate: Date
  let endDate: Date
  let calendarEventTitle: String
  let needsAlarm = true
  let alarmOffsetInMinutes = -15
  
  func asComparableCalendarEvent() -> ComparableCalendarEvent {
    ComparableCalendarEvent(startDate: startDate,
                            endDate: endDate,
                            calendarEventTitle: calendarEventTitle)
  }
}

struct ComparableCalendarEvent: Hashable {
  let startDate: Date
  let endDate: Date
  let calendarEventTitle: String
}
