//
//  CalendarServiceProtocol.swift
//  TSUInTime
//

import Foundation
import PromiseKit

enum EKCalendarAuthorizationState {
  case authorized
  case notDetermined
  case denied
}

protocol CalendarServiceProtocol: AnyObject {
  var authorizationStatus: EKCalendarAuthorizationState { get }
  func exportToCalendar(startDate: Date, endDate: Date) -> Promise<Void>
}
