//
//  CalendarServiceError.swift
//  TSUInTime
//

import Foundation

enum CalendarServiceError: LocalizedError {
  case accessIsDenied
  case nothingToExport
  case unexpectedError
  
  var errorDescription: String? {
    switch self {
    case .unexpectedError:
      return R.string.errors.unexpectedErrorText()
      
    case .accessIsDenied:
      return R.string.errors.calendarAccessWasDeniedErrorText()
      
    case .nothingToExport:
      return R.string.errors.nothingToExportToCalendarErrorText()
      
    }
  }
  
}
