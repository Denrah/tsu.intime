//
//  PushNotificationsServiceError.swift
//  TSUInTime
//

import Foundation

enum PushNotificationsServiceError: LocalizedError {
  case restrictedByUser, noUserInfo

  var errorDescription: String? {
    switch self {
    case .restrictedByUser:
      return R.string.onboarding.notificationsErrorRestrictedByUserText()
    case .noUserInfo:
      return R.string.onboarding.notificationsErrorNoUserInfoText()
    }
  }
}
