//
//  PushNotificationsService.swift
//  TSUInTime
//

import Foundation
import UserNotifications
import PromiseKit
import Firebase

protocol PushNotificationsServiceProtocol: AnyObject {
  var delegate: PushNotificationsServiceDelegate? { get set }

  func register() -> Promise<Void>
  func subscribe(to topic: FirebaseTopic) -> Promise<Void>
  func unsubscribe(from topic: FirebaseTopic) -> Promise<Void>
  func unsubscribe(from topicName: String) -> Promise<Void>
}

protocol PushNotificationsServiceDelegate: AnyObject {
  func pushNotificationsService(_ service: PushNotificationsService, didReceiveNotification notification: PushNotification)
}

class PushNotificationsService: NSObject, PushNotificationsServiceProtocol {
  weak var delegate: PushNotificationsServiceDelegate?

  private let notificationCenter = UNUserNotificationCenter.current()
  private let messaging = Messaging.messaging()
  private let dataStore: DataStoreProtocol

  init(dataStore: DataStoreProtocol) {
    self.dataStore = dataStore
    super.init()
    notificationCenter.delegate = self
  }
  
  func register() -> Promise<Void> {
    return firstly {
      self.getAuthorizationStatus()
    }.then { status -> Promise<Void> in
      switch status {
      case .notDetermined:
        return self.requestAuthorization()
      case .authorized:
        return Promise.value(())
      case .denied:
        return Promise(error: PushNotificationsServiceError.restrictedByUser)
      default:
        return Promise.value(())
      }
    }
  }
  
  func subscribe(to topic: FirebaseTopic) -> Promise<Void> {
    return Promise { seal in
      messaging.subscribe(toTopic: topic.topicName) { error in
        if let error = error {
          seal.reject(error)
          return
        }
        self.addStoredTopic(topic.topicName)
        seal.fulfill(())
      }
    }
  }
  
  func unsubscribe(from topic: FirebaseTopic) -> Promise<Void> {
    return unsubscribe(from: topic.topicName)
  }
  
  func unsubscribe(from topicName: String) -> Promise<Void> {
    return Promise { seal in
      messaging.unsubscribe(fromTopic: topicName) { error in
        if let error = error {
          seal.reject(error)
          return
        }
        self.removeStoredTopic(topicName)
        seal.fulfill(())
      }
    }
  }
  
  private func requestAuthorization() -> Promise<Void> {
    return Promise { seal in
      self.notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
        if let error = error {
          seal.reject(error)
          return
        }
        
        if granted {
          seal.fulfill(())
        } else {
          seal.reject(PushNotificationsServiceError.restrictedByUser)
        }
      }
    }
  }
  
  private func getAuthorizationStatus() -> Promise<UNAuthorizationStatus> {
    return Promise { seal in
      self.notificationCenter.getNotificationSettings { settings in
        seal.fulfill(settings.authorizationStatus)
      }
    }
  }
  
  private func addStoredTopic(_ topic: String) {
    var topics = dataStore.firebaseSubscribedTopics ?? Set<String>()
    topics.insert(topic)
    dataStore.firebaseSubscribedTopics = topics
  }
  
  private func removeStoredTopic(_ topic: String) {
    var topics = dataStore.firebaseSubscribedTopics ?? Set<String>()
    topics.remove(topic)
    dataStore.firebaseSubscribedTopics = topics
  }
}

// MARK: - UNUserNotificationCenterDelegate

extension PushNotificationsService: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let notification = PushNotification(content: notification.request.content)
    delegate?.pushNotificationsService(self, didReceiveNotification: notification)
  }
}
