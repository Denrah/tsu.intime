//
//  FirebaseTopic.swift
//  TSUInTime
//

import Foundation

enum FirebaseTopic {
  case timetableUpdates(userInfo: UserInfo)

  var topicName: String {
    switch self {
    case .timetableUpdates(let userInfo):
      return "intime.schedule.\(userInfo.pushNotificationsType).\(userInfo.timetableID)"
    }
  }
}
