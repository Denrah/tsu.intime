//
//  PushNotification.swift
//  TSUInTime
//

import Foundation
import UserNotifications

struct PushNotification {
  let title: String
  let body: String

  init(content: UNNotificationContent) {
    self.title = content.title
    self.body = content.body
  }
}
