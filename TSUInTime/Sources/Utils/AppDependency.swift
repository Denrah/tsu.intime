//
//  AppDependency.swift
//  TSUInTime
//

import Foundation

protocol HasCalendarService {
  var calendarService: CalendarServiceProtocol { get }
}

protocol HasDataStore {
  var dataStore: DataStoreProtocol { get }
}

protocol HasFacultiesService {
  var facultiesService: FacultiesNetworkProtocol { get }
}

protocol HasProfessorsService {
  var professorsService: ProfessorsNetworkProtocol { get }
}

protocol HasBuildingsService {
  var buildingsService: BuildingsNetworkProtocol { get }
}

protocol HasScheduleNetworkService {
  var scheduleNetworkService: ScheduleNetworkProtocol { get }
}

protocol HasCrashReportService {
  var crashReportService: CrashReportServiceProtocol { get }
}

protocol HasFavoritesStorageService {
  var favoritesStorageService: FavoritesStorageProtocol { get }
}

protocol HasPushNotificationsService {
  var pushNotificationsService: PushNotificationsServiceProtocol { get }
}

protocol HasFeedbackService {
  var feedbackService: FeedbackNetworkProtocol { get }
}

protocol HasAnalyticsService {
  var analyticsService: AnalyticsServiceProtocol { get }
}

class AppDependency: HasCrashReportService, HasPushNotificationsService, HasAnalyticsService {
  let crashReportService: CrashReportServiceProtocol
  let pushNotificationsService: PushNotificationsServiceProtocol
  let analyticsService: AnalyticsServiceProtocol
  private let networkService: NetworkService
  private let dataStoreService: DataStoreService
  private let storageService: StorageService
  private let calendarRepresentativeService: CalendarServiceProtocol
  
  init() {
    crashReportService = CrashReportService()
    analyticsService = AnalyticsService()
    networkService = NetworkService(crashReportService: crashReportService)
    dataStoreService = DataStoreService()
    storageService = StorageService()
    pushNotificationsService = PushNotificationsService(dataStore: dataStoreService)
    calendarRepresentativeService = CalendarService(timetableNetworkProtocol: networkService,
                                                    dataStoreService: dataStoreService)
  }
}

// MARK: - HasDataStore

extension AppDependency: HasDataStore {
  var dataStore: DataStoreProtocol {
    dataStoreService
  }
}

// MARK: - HasFacultiesService

extension AppDependency: HasFacultiesService {
  var facultiesService: FacultiesNetworkProtocol {
    networkService
  }
}

// MARK: - HasProfessorsService

extension AppDependency: HasProfessorsService {
  var professorsService: ProfessorsNetworkProtocol {
    networkService
  }
}

// MARK: - HasBuildingsService

extension AppDependency: HasBuildingsService {
  var buildingsService: BuildingsNetworkProtocol {
    networkService
  }
}

// MARK: - HasDayDisplayTimetableService

extension AppDependency: HasScheduleNetworkService {
  var scheduleNetworkService: ScheduleNetworkProtocol {
    networkService
  }
}

// MARK: - HasFeedbackService

extension AppDependency: HasFeedbackService {
  var feedbackService: FeedbackNetworkProtocol {
    networkService
  }
}

// MARK: - HasFavoritesStorageService

extension AppDependency: HasFavoritesStorageService {
  var favoritesStorageService: FavoritesStorageProtocol {
    storageService
  }
}

// MARK: - HasCalendarService

extension AppDependency: HasCalendarService {
  var calendarService: CalendarServiceProtocol {
    calendarRepresentativeService
  }
}
