//
//  AcademicPeriodsHelper.swift
//  TSUInTime
//

import Foundation

private extension Constants {
  static let academicYearStartMonthNumber = 9 // September
}

struct AcademicPeriodsHelper {
  static private var calendar: Calendar {
    Date.calendarISO
  }

  static func numberOfAcademicWeek(for date: Date) -> Int {
    let firstFullWeekDate = firstFullAcademicWeekDate(date: date)
    let daysFromFirstFullWeek = (calendar.dateComponents([.day], from: firstFullWeekDate,
                                                         to: date).day ?? 0) + 1
    return Int(ceil(Double(daysFromFirstFullWeek) / 7) + 1)
  }
  
  static func startOfAcademicWeek(week: Int) -> Date? {
    if week == 1 {
      return startOfAcademicYear(for: Date())
    }
    let firstFullWeekDate = firstFullAcademicWeekDate(date: Date())
    return calendar.date(byAdding: .day, value: 7 * (week - 2), to: firstFullWeekDate)
  }
  
  private static func startOfAcademicYear(for date: Date) -> Date? {
    let currentMonth = calendar.component(.month, from: date)
    let currentAcademicYear: Int
    if currentMonth < Constants.academicYearStartMonthNumber {
      currentAcademicYear = calendar.component(.year, from: date) - 1
    } else {
      currentAcademicYear = calendar.component(.year, from: date)
    }
    return calendar.date(from: DateComponents(year: currentAcademicYear,
                                              month: Constants.academicYearStartMonthNumber))
  }
  
  private static func firstFullAcademicWeekDate(date: Date) -> Date {
    let academicYearStart = startOfAcademicYear(for: date) ?? Date()
    let academicYearStartWeekday = academicYearStart.dayOfWeek()
    return calendar.date(byAdding: .day, value: 8 - academicYearStartWeekday, to: academicYearStart) ?? Date()
  }
}
