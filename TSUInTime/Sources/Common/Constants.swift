//
//  Constants.swift
//  TSUInTime
//

import UIKit

struct Constants {
  static let daysInWeek = 7
  static let bottomBarHeight: CGFloat = 56
}
