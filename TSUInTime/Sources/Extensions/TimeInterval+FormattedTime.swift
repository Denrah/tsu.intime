//
//  TimeInterval+FormattedTime.swift
//  TSUInTime
//

import Foundation

extension TimeInterval {
  func formattedTime() -> String {
    let formatter = DateFormatter.shortHoursMinutes
    return formatter.string(from: Date(timeIntervalSince1970: self))
  }
}
