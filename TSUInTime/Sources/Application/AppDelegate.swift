//
//  AppDelegate.swift
//  TSUInTime
//

import UIKit
import Firebase
import FirebaseCrashlytics

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    configureFirebase()
    application.registerForRemoteNotifications()
    return true
  }
  
  private func configureFirebase() {
    FirebaseApp.configure()
  }
}
