//
//  PaddingsDescribing.swift
//  TSUInTime
//

import UIKit

protocol PaddingsDescribing {
  var paddings: UIEdgeInsets { get }
}
