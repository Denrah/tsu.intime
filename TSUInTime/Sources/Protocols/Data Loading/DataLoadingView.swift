//
//  DataLoadingView.swift
//  TSUInTime
//

import Foundation

protocol DataLoadingView: AnyObject {
  func bind(to viewModel: DataLoadingViewModel)
  func handleRequestStarted()
  func handleRequestFinished()
  func handleErrorReceived()
  func reloadData()
}

extension DataLoadingView where Self: ActivityIndicatorViewDisplaying & ErrorHandling {
  func bind(to viewModel: DataLoadingViewModel) {
    viewModel.onDidStartRequest = { [weak self] in
      self?.activityIndicatorView.startAnimating()
      (self as? EmptyStateErrorViewDisplaying)?.emptyStateErrorView.isHidden = true
      self?.handleRequestStarted()
    }
    
    viewModel.onDidFinishRequest = { [weak self] in
      self?.activityIndicatorView.stopAnimating()
      self?.handleRequestFinished()
    }
    
    viewModel.onDidLoadData = { [weak self] in
      self?.reloadData()
    }
    
    viewModel.onDidReceiveError = { [weak self] error in
      self?.handle(error, showAsEmptyState: true)
      self?.handleErrorReceived()
    }
  }
  
  func handleRequestStarted() {}
  func handleRequestFinished() {}
  func handleErrorReceived() {}
  func reloadData() {}
}
