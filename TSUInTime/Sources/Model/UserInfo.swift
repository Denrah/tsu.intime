//
//  UserInfo.swift
//  TSUInTime
//

import Foundation

struct UserInfo: Codable {
  let role: UserRole
  let timetableType: TimetableType
  let timetableID: String
  let timetableName: String
  
  let faculty: Faculty?
  let group: Group?
  let professor: Professor?
  
  var timetableTitle: String {
    switch timetableType {
    case .group:
      return R.string.timetable.screenGroupTitle(timetableName)
    case .professor, .auditory:
      return timetableName
    }
  }
  
  var pushNotificationsType: Int {
    switch role {
    case .student:
      return 0
    case .professor:
      return 1
    }
  }
  
  init(role: UserRole, timetableType: TimetableType, timetableID: String, timetableName: String,
       faculty: Faculty? = nil, group: Group? = nil, professor: Professor? = nil) {
    self.role = role
    self.timetableType = timetableType
    self.timetableID = timetableID
    self.timetableName = timetableName
    self.faculty = faculty
    self.group = group
    self.professor = professor
  }
  
}
 
