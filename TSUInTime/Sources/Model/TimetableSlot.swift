//
//  TimetableSlot.swift
//  TSUInTime
//

import Foundation

struct TimetableSlot: Codable, Hashable {
  enum CodingKeys: String, CodingKey {
    case startTime = "starts", endTime = "ends", lessonNumber
  }
  
  let startTime: TimeInterval
  let endTime: TimeInterval
  let lessonNumber: Int
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    startTime = (try container.decode(TimeInterval.self, forKey: .startTime))
    endTime = (try container.decode(TimeInterval.self, forKey: .endTime))
    lessonNumber = (try container.decode(Int.self, forKey: .lessonNumber))
  }

  private func convertTimeStringToInterval(timeString: String) -> TimeInterval? {
    let stringParts = timeString.split(separator: ":", omittingEmptySubsequences: true)
    guard stringParts.count == 2, let hoursString = stringParts.first, let minutesString = stringParts.last,
          let hours = Int(hoursString), let minutes = Int(minutesString) else { return nil }
    return TimeInterval((hours * 60 + minutes) * 60)
  }
}
