//
//  Faculty.swift
//  TSUInTime
//

import Foundation

struct Faculty: Codable {
  enum CodingKeys: String, CodingKey {
    case id, name, hexColor = "color", avatar
  }
  
  let id: String
  let name: String
  let hexColor: String
  let avatar: String
  
  var avatarURL: URL? {
    URL(string: avatar)
  }
}
