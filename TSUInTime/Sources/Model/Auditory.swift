//
//  Auditory.swift
//  TSUInTime
//

import Foundation

struct Auditory: Codable {
  enum CodingKeys: String, CodingKey {
    case id, name, shortName, buildingID = "buildingId", building
  }
  
  let id: String
  let name: String
  let shortName: String?
  let buildingID: String?
  let building: Building?
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = (try? container.decodeIfPresent(String.self, forKey: .id)) ?? ""
    name = try container.decode(String.self, forKey: .name)
    shortName = (try? container.decodeIfPresent(String.self, forKey: .shortName))
    buildingID = (try? container.decodeIfPresent(String.self, forKey: .buildingID))
    building = (try? container.decodeIfPresent(Building.self, forKey: .building))
  }
}
