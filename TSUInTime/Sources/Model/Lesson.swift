//
//  Lesson.swift
//  TSUInTime
//

import Foundation

struct Lesson: Codable {
  enum CodingKeys: String, CodingKey {
    case title, professor, groups, auditory = "audience", startTime = "starts",
         endTime = "ends", lessonNumber, type = "lessonType", id
  }
  
  let title: String
  let professor: Professor
  let groups: [Group]
  let auditory: Auditory
  let startTime: TimeInterval
  let endTime: TimeInterval
  let lessonNumber: Int
  let type: LessonType
  let id: String
}

extension Lesson {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    title = try container.decode(String.self, forKey: .title)
    professor = try container.decode(Professor.self, forKey: .professor)
    groups = (try container.decodeIfPresent([Group].self, forKey: .groups)) ?? []
    auditory = try container.decode(Auditory.self, forKey: .auditory)
    startTime = try container.decode(TimeInterval.self, forKey: .startTime)
    endTime = try container.decode(TimeInterval.self, forKey: .endTime)
    lessonNumber = try container.decode(Int.self, forKey: .lessonNumber)
    type = (try? container.decodeIfPresent(LessonType.self, forKey: .type)) ?? .otherWork
    id = try container.decode(String.self, forKey: .id)
  }
}
