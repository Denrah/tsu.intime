//
//  Building.swift
//  TSUInTime
//

import Foundation
import CoreLocation

struct Building: Codable {
  let id: String
  let name: String
  let address: String?
  let latitude: Double?
  let longitude: Double?
  
  var coordinate: CLLocationCoordinate2D? {
    guard let latitude = latitude, let longitude = longitude else { return nil }
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
}
