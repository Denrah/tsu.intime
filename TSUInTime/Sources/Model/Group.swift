//
//  Group.swift
//  TSUInTime
//

import Foundation

struct Group: Codable {
  enum CodingKeys: String, CodingKey {
    case id, name, facultyID = "idFaculty", isSubgroup
  }
  
  let id: String
  let name: String
  let facultyID: String?
  let isSubgroup: Bool
}
