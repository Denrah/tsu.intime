//
//  TimetableBreak.swift
//  TSUInTime
//

import Foundation

struct TimetableBreak: Codable {
  enum CodingKeys: String, CodingKey {
    case startTime = "starts", endTime = "ends", lessonsCount = "lessonCount",
         duration = "durationInSeconds"
  }
  
  let startTime: TimeInterval
  let endTime: TimeInterval
  let lessonsCount: Int
  let duration: Int
  
  var breakInMinutes: Int {
    return duration / 60
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    startTime = try container.decode(TimeInterval.self, forKey: .startTime)
    endTime = try container.decode(TimeInterval.self, forKey: .endTime)
    lessonsCount = (try? container.decodeIfPresent(Int.self, forKey: .lessonsCount)) ?? 0
    duration = try container.decode(Int.self, forKey: .duration)
  }
}
