//
//  TimetableDisplayType.swift
//  TSUInTime
//

import Foundation

enum TimetableDisplayType: String, Codable {
  case day, week
}
