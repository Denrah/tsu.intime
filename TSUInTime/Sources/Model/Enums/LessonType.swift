//
//  LessonType.swift
//  TSUInTime
//

import Foundation
import UIKit

enum LessonType: String, Codable {
  case lecture = "LECTURE", practice = "PRACTICE", seminar = "SEMINAR",
       laboratory = "LABORATORY", individual = "INDIVIDUAL", otherWork = "OTHER",
       testWork = "CONTROL_POINT", consultation = "CONSULTATION", exam = "EXAM",
       credit = "CREDIT", differentialCredit = "DIFFERENTIAL_CREDIT"
  
  var title: String {
    switch self {
    case .lecture:
      return R.string.timetable.lessonTypeLectureTitle()
    case .practice:
      return R.string.timetable.lessonTypePracticeTitle()
    case .seminar:
      return R.string.timetable.lessonTypeSeminarTitle()
    case .laboratory:
      return R.string.timetable.lessonTypeLaboratoryTitle()
    case .individual:
      return R.string.timetable.lessonTypeIndividualTitle()
    case .otherWork:
      return R.string.timetable.lessonTypeOtherWorkTitle()
    case .testWork:
      return R.string.timetable.lessonTypeTestWorkTitle()
    case .consultation:
      return R.string.timetable.lessonTypeConsultationTitle()
    case .exam:
      return R.string.timetable.lessonTypeExamTitle()
    case .credit:
      return R.string.timetable.lessonTypeCreditTitle()
    case .differentialCredit:
      return R.string.timetable.lessonTypeDifferentialCreditTitle()
    }
  }
  
  var color: UIColor {
    switch self {
    case .lecture:
      return .lessonTypeLecture
    case .practice:
      return .lessonTypePractice
    case .seminar:
      return .lessonTypeSeminar
    case .laboratory:
      return .lessonTypeLaboratory
    case .individual:
      return .lessonTypeIndividual
    case .otherWork:
      return .lessonTypeOtherWork
    case .testWork, .exam, .credit, .differentialCredit:
      return .lessonTypeTestWork
    case .consultation:
      return .lessonTypeConsultation
    }
  }
}
