//
//  WeekScheduleCellContainerType.swift
//  TSUInTime
//

import UIKit

enum WeekScheduleCellWidth: CGFloat {
  case short = 56
  case long = 144
}
