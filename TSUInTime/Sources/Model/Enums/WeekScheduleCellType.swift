//
//  WeekScheduleCellType.swift
//  TSUInTime
//

import Foundation

enum WeekScheduleCellType {
  case weekday(weekdayViewModel: WeekScheduleWeekdayViewModel)
  case lessonTime(lessonTimeViewModel: WeekScheduleLessonTimeViewModel)
  case lessons(lessonViewModels: [WeekScheduleLessonViewModel])
  case empty
}
