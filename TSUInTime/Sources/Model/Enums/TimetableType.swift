//
//  TimetableType.swift
//  TSUInTime
//

import Foundation

enum TimetableType: String, Codable, CaseIterable {
  case group, professor, auditory = "audience"

  func title(for timetableName: String?) -> String? {
    guard let timetableName = timetableName else { return nil }
    switch self {
    case .group:
      return R.string.timetable.screenGroupTitle(timetableName)
    case .professor, .auditory:
      return timetableName
    }
  }
}
