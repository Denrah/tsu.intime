//
//  TimetableEntity.swift
//  TSUInTime
//

import Foundation

enum TimetableEntityType: String, Codable {
  case lesson = "LESSON", `break` = "BREAK", staticBreak = "STATIC_BREAK"
}

enum TimetableEntity: Decodable {
  case lesson(lesson: Lesson), lessonsBreak(timetableBreak: TimetableBreak),
       staticBreak(timetableBreak: TimetableBreak)
  
  enum CodingKeys: String, CodingKey {
    case type
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let type = try container.decode(TimetableEntityType.self, forKey: .type)
    
    switch type {
    case .lesson:
      self = .lesson(lesson: try Lesson(from: decoder))
    case .break:
      self = .lessonsBreak(timetableBreak: try TimetableBreak(from: decoder))
    case .staticBreak:
      self = .staticBreak(timetableBreak: try TimetableBreak(from: decoder))
    }
  }
}
