//
//  UserRole.swift
//  TSUInTime
//

import Foundation

enum UserRole: String, Codable {
  case student, professor
}
 
