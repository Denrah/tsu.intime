//
//  Professor.swift
//  TSUInTime
//

import Foundation

struct Professor: Codable {
  enum CodingKeys: String, CodingKey {
    case id, name = "fullName", shortName
  }
  
  let id: String
  let name: String
  let shortName: String
}
