//
//  TimetableGridElement.swift
//  TSUInTime
//

import Foundation

struct TimetableGridElement: Codable {
  enum CodingKeys: String, CodingKey {
    case dateString = "date", lessons
  }
  
  let dateString: String
  let lessons: [Lesson]
  
  var date: Date? {
    return DateFormatter.yearMonthDayISO.date(from: dateString)
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    dateString = (try container.decode(String.self, forKey: .dateString))
    lessons = (try container.decode([Lesson].self, forKey: .lessons))
  }
}
