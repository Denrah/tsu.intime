//
//  TimetableWeek.swift
//  TSUInTime
//

import Foundation

struct TimetableWeek: Codable {
  let grid: [TimetableGridElement]
  let timeSlots: [TimetableSlot]
}
