//
//  TimetableDay.swift
//  TSUInTime
//

import Foundation

struct TimetableDay: Decodable {
  enum CodingKeys: String, CodingKey {
    case dateString = "date", timeSlots
  }
  
  let timeSlots: [TimetableEntity]
  let dateString: String?
  
  var date: Date? {
    guard let dateString = dateString else { return nil }
    return DateFormatter.yearMonthDayISO.date(from: dateString)
  }
  
  var dateGMT0: Date? {
    guard let dateString = dateString else { return nil }
    return DateFormatter.yearMonthDayGMT0.date(from: dateString)
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    dateString = (try? container.decodeIfPresent(String.self, forKey: .dateString))
    timeSlots = (try container.decode([TimetableEntity].self, forKey: .timeSlots))
  }
}
