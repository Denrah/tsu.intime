//
//  RadioButtonGroupView.swift
//  TSUInTime
//

import UIKit

protocol RadioButtonItemType: Equatable {
  var title: String? { get }
}

class RadioButtonGroupView<ItemType: RadioButtonItemType>: UIStackView {
  var onDidSelectOption: ((_ option: ItemType) -> Void)?

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  func configure(with items: [ItemType], selectedItem: ItemType? = nil) {
    arrangedSubviews.forEach { $0.removeFromSuperview() }
    items.forEach { item in
      let radioButton = RadioButton()
      radioButton.title = item.title
      radioButton.onDidTap = { [weak self, weak radioButton] in
        guard let radioButton = radioButton else { return }
        self?.handleRadioButtonTap(radioButton, option: item)
      }
      addArrangedSubview(radioButton)

      if item == selectedItem {
        radioButton.isSelected = true
      }
    }
  }

  private func setup() {
    axis = .vertical
  }

  private func handleRadioButtonTap(_ radioButton: RadioButton, option: ItemType) {
    arrangedSubviews.forEach { item in
      if let item = item as? RadioButton {
        item.isSelected = item == radioButton
      }
    }
    onDidSelectOption?(option)
  }
}
