//
//  RadioButton.swift
//  TSUInTime
//

import UIKit
import SnapKit

class RadioButton: UIView {
  // MARK: - Properties

  override var intrinsicContentSize: CGSize {
    return CGSize(width: 64 + titleLabel.intrinsicContentSize.width, height: 44)
  }

  var isSelected: Bool = false {
    didSet {
      updateSelectedState()
    }
  }

  var title: String? {
    get {
      titleLabel.text
    }
    set {
      titleLabel.text = newValue
    }
  }

  var onDidTap: (() -> Void)?

  private let outerCircle = UIView()
  private let innerCircle = UIView()
  private let titleLabel = Label(textStyle: .body)

  private var innerCircleSizeConstraint: Constraint?

  // MARK: - Init

  init() {
    super.init(frame: .zero)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  // MARK: - Actions

  @objc private func handleTap() {
    self.onDidTap?()
  }

  // MARK: - Setup

  private func setup() {
    setupOuterCircle()
    setupInnerCircle()
    setupTitleLabel()
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
  }

  private func setupOuterCircle() {
    addSubview(outerCircle)
    outerCircle.layer.cornerRadius = 12
    outerCircle.layer.borderWidth = 2
    outerCircle.layer.borderColor = UIColor.shade2.cgColor
    outerCircle.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.leading.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }

  private func setupInnerCircle() {
    outerCircle.addSubview(innerCircle)
    innerCircle.backgroundColor = .accent
    innerCircle.layer.cornerRadius = 0
    innerCircle.snp.makeConstraints { make in
      innerCircleSizeConstraint = make.size.equalTo(0).constraint
      make.center.equalToSuperview()
    }
  }

  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .baseBlack
    titleLabel.snp.makeConstraints { make in
      make.leading.equalTo(outerCircle.snp.trailing).offset(8)
      make.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
      make.top.bottom.greaterThanOrEqualToSuperview().inset(10)
    }
  }

  // MARK: - Private methods

  private func updateSelectedState() {
    innerCircleSizeConstraint?.update(offset: isSelected ? 12 : 0)
    UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
      self.layoutIfNeeded()
      self.outerCircle.layer.borderColor = self.isSelected ? UIColor.accent.cgColor : UIColor.shade2.cgColor
      self.innerCircle.layer.cornerRadius = self.isSelected ? 6 : 0
    }, completion: nil)
  }
}
