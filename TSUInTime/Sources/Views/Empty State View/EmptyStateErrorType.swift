//
//  EmptyStateErrorType.swift
//  TSUInTime
//

import UIKit

enum EmptyStateErrorType {
  case noInternet, serverError
  
  var image: UIImage? {
    switch self {
    case .noInternet:
      return R.image.noInternetImage()
    case .serverError:
      return R.image.serverErrorImage()
    }
  }
  
  var title: String {
    switch self {
    case .noInternet:
      return R.string.errors.noInternetErrorTitle()
    case .serverError:
      return R.string.errors.serverErrorTitle()
    }
  }
  
  var subtitle: String {
    switch self {
    case .noInternet:
      return R.string.errors.noInternetErrorSubtitle()
    case .serverError:
      return R.string.errors.serverErrorSubtitle()
    }
  }
}
