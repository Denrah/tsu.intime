//
//  AuditoriesSearchViewModel.swift
//  TSUInTime
//

import Foundation

protocol AuditoriesSearchViewModelDelegate: AnyObject {
  func auditoriesSearchViewModel(_ viewModel: AuditoriesSearchViewModel,
                                 didSelect auditory: Auditory)
}

class AuditoriesSearchViewModel: DataLoadingViewModel, SearchableTableViewModel {
  typealias Dependencies = HasBuildingsService
  
  // MARK: - Properties
  
  weak var delegate: AuditoriesSearchViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  let title: String?
  let placeholder: String?
  
  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasData: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] {
    filteredItemViewModels()
  }
  
  private var buildingID: String?
  private var searchText: String?
  private var auditoryListItemViewModels: [SimpleListItemViewModel] = []
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies, title: String? = nil,
       placeholder: String? = R.string.search.auditoriesPageSearchFieldPlaceholder()) {
    self.dependencies = dependencies
    self.title = title
    self.placeholder = placeholder
  }
  
  // MARK: - Public methods
  
  func update(buildingID: String, reloadData: Bool = true) {
    self.buildingID = buildingID
    
    if reloadData {
      loadData(buildingID: buildingID)
    }
  }
  
  func updateSearchText(text: String?) {
    searchText = text
    auditoryListItemViewModels.forEach { $0.highlightedText = text }
    onDidLoadData?()
  }
  
  func onViewIsReady() {
    guard let buildingID = buildingID else { return }
    loadData(buildingID: buildingID)
  }
  
  // MARK: - Private methods
  
  private func loadData(buildingID: String) {
    onDidStartRequest?()
    dependencies.buildingsService.getAuditories(buildingID: buildingID).ensure {
      self.onDidFinishRequest?()
    }.done { auditories in
      self.handle(auditories)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func handle(_ auditories: [Auditory]) {
    auditoryListItemViewModels = auditories.map { auditory in
      let viewModel = AuditoryListItemViewModel(auditory: auditory)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private func filteredItemViewModels() -> [SimpleListItemViewModel] {
    guard let text = searchText, !text.isEmpty else {
      return auditoryListItemViewModels
    }
    return auditoryListItemViewModels.filter { $0.title.lowercased().contains(text.lowercased()) }
  }
}

// MARK: - AuditoryListItemViewModelDelegate

extension AuditoriesSearchViewModel: AuditoryListItemViewModelDelegate {
  func auditoryListItemViewModel(_ viewModel: AuditoryListItemViewModel, didSelect auditory: Auditory) {
    delegate?.auditoriesSearchViewModel(self, didSelect: auditory)
  }
}
