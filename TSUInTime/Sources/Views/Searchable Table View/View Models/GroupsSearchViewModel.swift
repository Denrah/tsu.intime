//
//  GroupsSearchViewModel.swift
//  TSUInTime
//

import Foundation

protocol GroupsSearchViewModelDelegate: AnyObject {
  func groupsSearchViewModel(_ viewModel: GroupsSearchViewModel,
                             didSelect group: Group, faculty: Faculty)
}

class GroupsSearchViewModel: DataLoadingViewModel, SearchableTableViewModel {
  typealias Dependencies = HasFacultiesService
  
  // MARK: - Properties
  
  weak var delegate: GroupsSearchViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  let title: String?
  let placeholder: String?
  
  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasData: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] {
    filteredItemViewModels()
  }
  
  private var faculty: Faculty?
  private var searchText: String?
  private var groupListItemViewModels: [SimpleListItemViewModel] = []
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies, title: String? = nil,
       placeholder: String? = R.string.search.groupsPageSearchFieldPlaceholder()) {
    self.dependencies = dependencies
    self.title = title
    self.placeholder = placeholder
  }
  
  // MARK: - Public methods
  
  func update(faculty: Faculty, reloadData: Bool = true) {
    self.faculty = faculty
    
    if reloadData {
      loadData(facultyID: faculty.id)
    }
  }
  
  func updateSearchText(text: String?) {
    searchText = text
    groupListItemViewModels.forEach { $0.highlightedText = text }
    onDidLoadData?()
  }
  
  func onViewIsReady() {
    guard let facultyID = faculty?.id else { return }
    
    loadData(facultyID: facultyID)
  }
  
  // MARK: - Private methods
  
  private func loadData(facultyID: String) {
    onDidStartRequest?()
    dependencies.facultiesService.getGroups(facultyID: facultyID).ensure {
      self.onDidFinishRequest?()
    }.done { groups in
      self.handle(groups)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func handle(_ groups: [Group]) {
    groupListItemViewModels = groups.map { group in
      let viewModel = GroupListItemViewModel(group: group)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private func filteredItemViewModels() -> [SimpleListItemViewModel] {
    guard let text = searchText, !text.isEmpty else {
      return groupListItemViewModels
    }
    return groupListItemViewModels.filter { $0.title.lowercased().contains(text.lowercased()) }
  }
}

// MARK: - GroupListItemViewModelDelegate

extension GroupsSearchViewModel: GroupListItemViewModelDelegate {
  func groupListItemViewModel(_ viewModel: GroupListItemViewModel, didSelect group: Group) {
    guard let faculty = faculty else { return }
    delegate?.groupsSearchViewModel(self, didSelect: group, faculty: faculty)
  }
}
