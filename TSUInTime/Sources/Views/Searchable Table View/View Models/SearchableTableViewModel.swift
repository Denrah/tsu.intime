//
//  SearchableTableViewModel.swift
//  TSUInTime
//

import Foundation

protocol SearchableTableViewModel: TableViewModel {
  var title: String? { get }
  var placeholder: String? { get }
  var hasTitle: Bool { get }
  var hasData: Bool { get }
  var emptyStateViewModel: EmptyStateViewModel { get }
  
  func updateSearchText(text: String?)
  func onViewIsReady()
}

extension SearchableTableViewModel {
  func onViewIsReady() {}
  
  var hasTitle: Bool {
    title != nil
  }
  
  var emptyStateViewModel: EmptyStateViewModel {
    EmptyStateViewModel(image: nil, imageSize: nil,
                        title: R.string.search.searchPlaceholderTitle(),
                        subtitle: R.string.search.searchPlaceholderSubtitle(),
                        additionalText: R.string.search.searchPlaceholderAdditionalText(),
                        additionalTextFont: .header1)
  }
}
