//
//  ProfessorsSearchViewModel.swift
//  TSUInTime
//

import Foundation

protocol ProfessorsSearchViewModelDelegate: AnyObject {
  func professorsSearchViewModel(_ viewModel: ProfessorsSearchViewModel,
                                 didSelect professor: Professor)
}

class ProfessorsSearchViewModel: DataLoadingViewModel, SearchableTableViewModel {
  typealias Dependencies = HasProfessorsService
  
  // MARK: - Properties
  
  weak var delegate: ProfessorsSearchViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  let title: String?
  let placeholder: String?
  
  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasData: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] {
    filteredItemViewModels()
  }
  
  private var searchText: String?
  private var professorsListItemViewModels: [SimpleListItemViewModel] = []
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies, title: String? = nil,
       placeholder: String? = R.string.search.professorsPageSearchFieldPlaceholder()) {
    self.dependencies = dependencies
    self.title = title
    self.placeholder = placeholder
  }
  
  // MARK: - Public methods
  
  func onViewIsReady() {
    loadData()
  }
  
  func updateSearchText(text: String?) {
    searchText = text
    professorsListItemViewModels.forEach { $0.highlightedText = text }
    onDidLoadData?()
  }
  
  // MARK: - Private methods
  
  private func loadData() {
    onDidStartRequest?()
    dependencies.professorsService.getProfessors().ensure {
      self.onDidFinishRequest?()
    }.done { professors in
      self.handle(professors)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func handle(_ professors: [Professor]) {
    professorsListItemViewModels = professors.map { professor in
      let viewModel = ProfessorListItemViewModel(professor: professor)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private func filteredItemViewModels() -> [SimpleListItemViewModel] {
    guard let text = searchText, !text.isEmpty else {
      return professorsListItemViewModels
    }
    return professorsListItemViewModels.filter { $0.title.lowercased().contains(text.lowercased()) }
  }
}

// MARK: - ProfessorListItemViewModelDelegate

extension ProfessorsSearchViewModel: ProfessorListItemViewModelDelegate {
  func professorListItemViewModel(_ viewModel: ProfessorListItemViewModel, didSelect professor: Professor) {
    delegate?.professorsSearchViewModel(self, didSelect: professor)
  }
}
