//
//  BuildingsSearchViewModel.swift
//  TSUInTime
//

import Foundation

protocol BuildingsSearchViewModelDelegate: AnyObject {
  func buildingsSearchViewModel(_ viewModel: BuildingsSearchViewModel,
                                didSelect building: Building)
}

class BuildingsSearchViewModel: DataLoadingViewModel, SearchableTableViewModel {
  typealias Dependencies = HasBuildingsService
  
  // MARK: - Properties
  
  weak var delegate: BuildingsSearchViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  let title: String?
  let placeholder: String?
  
  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasData: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] {
    filteredItemViewModels()
  }
  
  private var searchText: String?
  private var buildingListItemViewModels: [BuildingListItemViewModel] = []
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies, title: String? = nil,
       placeholder: String? = R.string.search.buildingsPageSearchFieldPlaceholder()) {
    self.dependencies = dependencies
    self.title = title
    self.placeholder = placeholder
  }
  
  // MARK: - Public methods
  
  func onViewIsReady() {
    loadData()
  }
  
  func updateSearchText(text: String?) {
    searchText = text
    buildingListItemViewModels.forEach { $0.highlightedText = text }
    onDidLoadData?()
  }
  
  // MARK: - Private methods
  
  private func loadData() {
    onDidStartRequest?()
    
    dependencies.buildingsService.getBuildings().ensure {
      self.onDidFinishRequest?()
    }.done { buildings in
      self.handle(buildings)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func handle(_ buildings: [Building]) {
    buildingListItemViewModels = buildings.map { building in
      let viewModel = BuildingListItemViewModel(building: building)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private func filteredItemViewModels() -> [BuildingListItemViewModel] {
    guard let text = searchText, !text.isEmpty else {
      return buildingListItemViewModels
    }
    return buildingListItemViewModels.filter { $0.title.lowercased().contains(text.lowercased()) }
  }
}

// MARK: - BuildingListItemViewModelDelegate

extension BuildingsSearchViewModel: BuildingListItemViewModelDelegate {
  func buildingListItemViewModel(_ viewModel: BuildingListItemViewModel, didSelect building: Building) {
    delegate?.buildingsSearchViewModel(self, didSelect: building)
  }
}
