//
//  FacultiesSearchiewModel.swift
//  TSUInTime
//

import Foundation

protocol FacultiesSearchViewModelDelegate: AnyObject {
  func facultiesSearchViewModel(_ viewModel: FacultiesSearchiewModel,
                                didSelect faculty: Faculty)
}

class FacultiesSearchiewModel: DataLoadingViewModel, SearchableTableViewModel {
  typealias Dependencies = HasFacultiesService
  
  // MARK: - Properties
  
  weak var delegate: FacultiesSearchViewModelDelegate?
  
  var onDidStartRequest: (() -> Void)?
  var onDidFinishRequest: (() -> Void)?
  var onDidLoadData: (() -> Void)?
  var onDidReceiveError: ((Error) -> Void)?
  
  let title: String?
  
  let placeholder: String?
  
  var sectionViewModels: [TableSectionViewModel] {
    let section = TableSectionViewModel()
    section.append(cellViewModels: itemViewModels)
    return [section]
  }
  
  var hasData: Bool {
    !itemViewModels.isEmpty
  }
  
  private var itemViewModels: [TableCellViewModel] {
    filteredItemViewModels()
  }
  
  private var searchText: String?
  private var facultyListItemViewModels: [FacultyListItemViewModel] = []
  
  private let dependencies: Dependencies
  
  // MARK: - Init
  
  init(dependencies: Dependencies, title: String? = nil,
       placeholder: String? = R.string.search.facultiesPageSearchFieldPlaceholder()) {
    self.dependencies = dependencies
    self.title = title
    self.placeholder = placeholder
  }
  
  // MARK: - Public methods
  
  func onViewIsReady() {
    loadData()
  }
  
  func updateSearchText(text: String?) {
    searchText = text
    facultyListItemViewModels.forEach { $0.highlightedText = text }
    onDidLoadData?()
  }
  
  // MARK: - Private methods
  
  private func loadData() {
    onDidStartRequest?()
    
    dependencies.facultiesService.getFaculties().ensure {
      self.onDidFinishRequest?()
    }.done { faculties in
      self.handle(faculties)
      self.onDidLoadData?()
    }.catch { error in
      self.onDidReceiveError?(error)
    }
  }
  
  private func handle(_ faculties: [Faculty]) {
    facultyListItemViewModels = faculties.map { faculty in
      let viewModel = FacultyListItemViewModel(faculty: faculty)
      viewModel.delegate = self
      return viewModel
    }
  }
  
  private func filteredItemViewModels() -> [FacultyListItemViewModel] {
    guard let text = searchText, !text.isEmpty else {
      return facultyListItemViewModels
    }
    return facultyListItemViewModels.filter { $0.name.lowercased().contains(text.lowercased()) }
  }
}

// MARK: - FacultyListItemViewModelDelegate

extension FacultiesSearchiewModel: FacultyListItemViewModelDelegate {
  func facultyListItemViewModel(_ viewModel: FacultyListItemViewModel, didSelect faculty: Faculty) {
    delegate?.facultiesSearchViewModel(self, didSelect: faculty)
  }
}
