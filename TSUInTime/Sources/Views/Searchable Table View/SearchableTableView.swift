//
//  SearchableTableView.swift
//  TSUInTime
//

import UIKit

class SearchableTableView<ItemCell: UITableViewCell & TableCell>: UIView, ActivityIndicatorViewDisplaying,
                                                                  DataLoadingView, ErrorHandling,
                                                                  EmptyStateErrorViewDisplaying {
  // MARK: - Properties
  
  let activityIndicatorView = ActivityIndicatorView()
  let emptyStateErrorView = EmptyStateErrorView()

  private let headerContainerView = UIView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let searchTextField = TextField()
  private let tableView = UITableView(frame: .zero, style: .grouped)
  private let emptyStateView = EmptyStateView()
  
  private let viewModel: SearchableTableViewModel & DataLoadingViewModel
  private let dataSource = TableViewDataSource()
  
  // MARK: - Init
  
  init(viewModel: SearchableTableViewModel & DataLoadingViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
    bind(to: viewModel)
    viewModel.onViewIsReady()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Public methods
  
  func handleRequestStarted() {
    headerContainerView.isHidden = true
    tableView.isHidden = true
    emptyStateView.isHidden = true
  }
  
  func handleRequestFinished() {
    headerContainerView.isHidden = false
    tableView.isHidden = false
  }
  
  func handleErrorReceived() {
    headerContainerView.isHidden = true
    tableView.isHidden = true
  }
  
  func reloadData() {
    dataSource.update(with: viewModel)
    emptyStateView.isHidden = viewModel.hasData
  }
  
  func handleRefreshButtonTapped() {
    viewModel.onViewIsReady()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupHeaderContainerView()

    if viewModel.hasTitle {
      setupTitleLabel()
    }
    
    setupSearchTextField()
    setupTableView()
    setupEmptyStateView()
    setupEmptyStateErrorView()
    setupActivityIndicatorView()
  }

  private func setupHeaderContainerView() {
    addSubview(headerContainerView)
    headerContainerView.backgroundColor = .baseWhite
    headerContainerView.addShadow(offset: CGSize(width: 0, height: 4), radius: 24, color: .zeroBlack, opacity: 0)
    headerContainerView.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview()
    }
  }
  
  private func setupTitleLabel() {
    headerContainerView.addSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
    titleLabel.text = viewModel.title
    titleLabel.snp.makeConstraints { make in
      make.top.leading.trailing.equalToSuperview().inset(16)
    }
  }
  
  private func setupSearchTextField() {
    headerContainerView.addSubview(searchTextField)
    searchTextField.placeholder = viewModel.placeholder
    searchTextField.autocorrectionType = .no
    searchTextField.returnKeyType = .search
    searchTextField.hasClearButton = true
    searchTextField.delegate = self
    searchTextField.onChange = { [weak self] text in
      self?.viewModel.updateSearchText(text: text)
    }
    searchTextField.snp.makeConstraints { make in
      if viewModel.hasTitle {
        make.top.equalTo(titleLabel.snp.bottom).offset(16)
      } else {
        make.top.equalToSuperview().inset(16)
      }
      make.leading.trailing.bottom.equalToSuperview().inset(16)
    }
  }
  
  private func setupTableView() {
    addSubview(tableView)
    tableView.backgroundColor = .clear
    tableView.showsVerticalScrollIndicator = false
    tableView.separatorStyle = .none
    tableView.keyboardDismissMode = .onDrag
    tableView.snp.makeConstraints { make in
      make.top.equalTo(headerContainerView.snp.bottom)
      make.leading.trailing.bottom.equalToSuperview()
    }
    
    tableView.register(ItemCell.self, forCellReuseIdentifier: ItemCell.reuseIdentifier)
    dataSource.setup(tableView: tableView, viewModel: viewModel)
    dataSource.onDidScroll = { [weak self] scrollView in
      self?.handleScrollViewDidScroll(scrollView: scrollView)
    }
  }
  
  private func setupEmptyStateView() {
    addSubview(emptyStateView)
    emptyStateView.configure(with: viewModel.emptyStateViewModel)
    emptyStateView.isHidden = viewModel.hasData
    emptyStateView.snp.makeConstraints { make in
      make.top.equalTo(searchTextField.snp.bottom).offset(32)
      make.leading.trailing.equalToSuperview().inset(16)
    }
  }

  // MARK: - Private methods

  private func handleScrollViewDidScroll(scrollView: UIScrollView) {
    UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
      self.headerContainerView.layer.shadowOpacity = scrollView.contentOffset.y > 0 ? 0.1 : 0
    }, completion: nil)
  }
}

// MARK: - TextFieldDelegate

extension SearchableTableView: TextFieldDelegate {
  func textFieldShouldReturn(_ textField: TextField) -> Bool {
    endEditing(true)
    return true
  }
}
