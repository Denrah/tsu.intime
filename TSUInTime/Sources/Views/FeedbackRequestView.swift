//
//  FeedbackRequestView.swift
//  TSUInTime
//

import UIKit

class FeedbackRequestView: UIView {
  // MARK: - Properties
  
  var onDidLayout: ((_ frame: CGRect) -> Void)?
  var onDidTapClose: (() -> Void)?
  var onDidTapLike: (() -> Void)?
  var onDidTapDislike: (() -> Void)?
  
  private let headerStackView = UIStackView()
  private let iconImageView = UIImageView()
  private let labelsStackView = UIStackView()
  private let titleLabel = Label(textStyle: .bodyBold)
  private let subtitleLabel = Label(textStyle: .body)
  private let closeButton = UIButton(type: .system)
  private let likeButton = CommonButton(style: .small)
  private let dislikeButton = CommonButton(style: .smallClear)

  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Overrides
  
  override func layoutSubviews() {
    super.layoutSubviews()
    onDidLayout?(frame)
  }
  
  // MARK: - Actions
  
  @objc private func handleCloseTap() {
    onDidTapClose?()
  }
  
  @objc private func handleLikeTap() {
    onDidTapLike?()
  }
  
  @objc private func handleDislikeTap() {
    onDidTapDislike?()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupHeaderStackView()
    setupIconImageView()
    setupLabelsStackView()
    setupTitleLabel()
    setupSubtitleLabel()
    setupCloseButton()
    setupLikeButton()
    setupDislikeButton()
  }
  
  private func setupContainer() {
    backgroundColor = .baseWhite
    layer.cornerRadius = 8
    addShadow(offset: CGSize(width: 0, height: 6),
              radius: 24, color: .zeroBlack, opacity: 0.08)
  }
  
  private func setupHeaderStackView() {
    addSubview(headerStackView)
    headerStackView.axis = .horizontal
    headerStackView.spacing = 16
    headerStackView.alignment = .top
    headerStackView.snp.makeConstraints { make in
      make.top.leading.equalToSuperview().inset(16)
    }
  }
  
  private func setupIconImageView() {
    headerStackView.addArrangedSubview(iconImageView)
    iconImageView.image = R.image.feedbackStarImage()
    iconImageView.snp.makeConstraints { make in
      make.size.equalTo(48)
    }
  }
  
  private func setupLabelsStackView() {
    headerStackView.addArrangedSubview(labelsStackView)
    labelsStackView.axis = .vertical
    labelsStackView.spacing = 0
  }
  
  private func setupTitleLabel() {
    labelsStackView.addArrangedSubview(titleLabel)
    titleLabel.numberOfLines = 0
    titleLabel.text = R.string.menu.feedbackRequestTitle()
  }
  
  private func setupSubtitleLabel() {
    labelsStackView.addArrangedSubview(subtitleLabel)
    subtitleLabel.numberOfLines = 0
    subtitleLabel.text = R.string.menu.feedbackRequestSubtitle()
  }
  
  private func setupCloseButton() {
    addSubview(closeButton)
    closeButton.setImage(R.image.crossWithBackgroundIcon()?.withRenderingMode(.alwaysOriginal),
                         for: .normal)
    closeButton.addTarget(self, action: #selector(handleCloseTap), for: .touchUpInside)
    closeButton.snp.makeConstraints { make in
      make.size.equalTo(40)
      make.top.trailing.equalToSuperview().inset(4)
      make.leading.equalTo(headerStackView.snp.trailing).offset(4)
    }
  }
  
  private func setupLikeButton() {
    addSubview(likeButton)
    likeButton.setTitle(R.string.menu.feedbackRequestLikeButtonTitle(), for: .normal)
    likeButton.addTarget(self, action: #selector(handleLikeTap), for: .touchUpInside)
    likeButton.snp.makeConstraints { make in
      make.top.equalTo(headerStackView.snp.bottom).offset(16)
      make.leading.bottom.equalToSuperview().inset(16)
    }
  }
  
  private func setupDislikeButton() {
    addSubview(dislikeButton)
    dislikeButton.setTitle(R.string.menu.feedbackRequestDislikeButtonTitle(), for: .normal)
    dislikeButton.addTarget(self, action: #selector(handleDislikeTap), for: .touchUpInside)
    dislikeButton.snp.makeConstraints { make in
      make.top.equalTo(headerStackView.snp.bottom).offset(16)
      make.leading.equalTo(likeButton.snp.trailing).offset(16)
      make.trailing.bottom.equalToSuperview().inset(16)
      make.width.equalTo(likeButton.snp.width)
    }
  }
}
