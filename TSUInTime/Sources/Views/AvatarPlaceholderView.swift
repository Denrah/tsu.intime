//
//  AvatarPlaceholderView.swift
//  TSUInTime
//

import UIKit

private extension Constants {
  static let placeholderColors: [(background: UIColor, foreground: UIColor)] = [
    (background: .accentLight, foreground: .accent),
    (background: .accentYellowLight, foreground: .accentYellow),
    (background: .accentGreenLight, foreground: .accentGreen),
    (background: .accentRedLight, foreground: .accentRed)
  ]
}

class AvatarPlaceholderView: UIView {
  // MARK: - Properties
  
  var maxLettersCount = 3 {
    didSet {
      update()
    }
  }
  
  var baseText: String? {
    didSet {
      update()
    }
  }
  
  private let titleLabel = Label(textStyle: .bodyBold)
  private let placeholderImageView = UIImageView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupTitleLabel()
    setupPlaceholderImageView()
    
    backgroundColor = .accentLight
    titleLabel.textColor = .accent
  }
  
  private func setupContainer() {
    layer.cornerRadius = 28
    snp.makeConstraints { make in
      make.size.equalTo(56)
    }
  }
  
  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  private func setupPlaceholderImageView() {
    addSubview(placeholderImageView)
    placeholderImageView.image = R.image.tsuLogoPlaceholder()?.withRenderingMode(.alwaysTemplate)
    placeholderImageView.contentMode = .scaleAspectFit
    placeholderImageView.snp.makeConstraints { make in
      make.size.equalTo(32)
      make.center.equalToSuperview()
    }
  }
  
  // MARK: - Private methods
  
  private func update() {
    guard let baseText = baseText?.replacingOccurrences(of: "-", with: " ") else { return }
    let textParts = baseText.split(separator: " ", omittingEmptySubsequences: true)
    
    if textParts.count > maxLettersCount {
      titleLabel.isHidden = true
      placeholderImageView.isHidden = false
    } else {
      titleLabel.isHidden = false
      placeholderImageView.isHidden = true
      titleLabel.text = textParts.reduce("") { $0 + $1.uppercased().prefix(1) }
    }
    
    let index = abs(baseText.hash) % Constants.placeholderColors.count
    let color = Constants.placeholderColors.element(at: index)
    backgroundColor = color?.background
    placeholderImageView.tintColor = color?.foreground
    titleLabel.textColor = color?.foreground
  }
}
