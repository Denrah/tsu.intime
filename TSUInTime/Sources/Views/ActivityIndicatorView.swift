//
//  ActivityIndicatorView.swift
//  TSUInTime
//

import UIKit
import Lottie

enum ActivityIndicatorViewStyle {
  case accent, white

  var animation: Animation? {
    switch self {
    case .accent:
      return Animations.clockActivity
    case .white:
      return Animations.clockActivityWhite
    }
  }
}

class ActivityIndicatorView: UIView {
  var hideWhenStopped: Bool = true

  override var intrinsicContentSize: CGSize {
    CGSize(width: 40, height: 40)
  }
  
  private let animationView = AnimationView()
  
  init(style: ActivityIndicatorViewStyle = .accent) {
    super.init(frame: .zero)
    setup()
    animationView.animation = style.animation
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  func startAnimating() {
    animationView.play()
    isHidden = false
  }
  
  func stopAnimating() {
    animationView.stop()
    if hideWhenStopped {
      isHidden = true
    }
  }
  
  private func setup() {
    isHidden = true
    
    addSubview(animationView)
    animationView.contentMode = .scaleAspectFit
    animationView.loopMode = .loop
    animationView.backgroundBehavior = .pauseAndRestore
    animationView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
}
