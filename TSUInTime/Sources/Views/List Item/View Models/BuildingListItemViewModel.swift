//
//  BuildingListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol BuildingListItemViewModelDelegate: AnyObject {
  func buildingListItemViewModel(_ viewModel: BuildingListItemViewModel, didSelect building: Building)
}

class BuildingListItemViewModel: SimpleListItemViewModel {
  weak var delegate: BuildingListItemViewModelDelegate?
  
  var title: String {
    building.name
  }
  
  var highlightedText: String?
  
  private let building: Building
  
  init(building: Building) {
    self.building = building
  }
  
  func select() {
    delegate?.buildingListItemViewModel(self, didSelect: building)
  }
  
}
