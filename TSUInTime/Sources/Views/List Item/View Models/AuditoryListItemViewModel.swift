//
//  AuditoryListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol AuditoryListItemViewModelDelegate: AnyObject {
  func auditoryListItemViewModel(_ viewModel: AuditoryListItemViewModel, didSelect auditory: Auditory)
}

class AuditoryListItemViewModel: SimpleListItemViewModel {
  weak var delegate: AuditoryListItemViewModelDelegate?
  
  var title: String {
    auditory.name
  }
  
  var highlightedText: String?
  
  private let auditory: Auditory
  
  init(auditory: Auditory) {
    self.auditory = auditory
  }
  
  func select() {
    delegate?.auditoryListItemViewModel(self, didSelect: auditory)
  }
  
}
