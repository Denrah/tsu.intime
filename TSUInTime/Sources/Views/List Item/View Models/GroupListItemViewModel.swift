//
//  GroupListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol GroupListItemViewModelDelegate: AnyObject {
  func groupListItemViewModel(_ viewModel: GroupListItemViewModel, didSelect group: Group)
}

class GroupListItemViewModel: SimpleListItemViewModel {
  weak var delegate: GroupListItemViewModelDelegate?
  
  var title: String {
    group.name
  }
  
  var highlightedText: String?
  
  private let group: Group
  
  init(group: Group) {
    self.group = group
  }
  
  func select() {
    delegate?.groupListItemViewModel(self, didSelect: group)
  }
}
