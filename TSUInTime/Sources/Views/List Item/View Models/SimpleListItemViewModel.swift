//
//  SimpleListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol SimpleListItemViewModel: AnyObject, TableCellViewModel {
  var title: String { get }
  var highlightedText: String? { get set }
}

// MARK: - TableCellViewModel

extension SimpleListItemViewModel {
  var tableReuseIdentifier: String {
    SimpleListItemCell.reuseIdentifier
  }
}
