//
//  FacultyListItemViewModel.swift
//  TSUInTime
//

import Foundation

protocol FacultyListItemViewModelDelegate: AnyObject {
  func facultyListItemViewModel(_ viewModel: FacultyListItemViewModel, didSelect faculty: Faculty)
}

class FacultyListItemViewModel {
  weak var delegate: FacultyListItemViewModelDelegate?
  
  var name: String {
    faculty.name
  }
  
  var avatarImageURL: URL? {
    return faculty.avatarURL
  }
  
  var hexColor: String {
    return faculty.hexColor
  }
  
  var highlightedText: String?
  
  private let faculty: Faculty
  
  init(faculty: Faculty) {
    self.faculty = faculty
  }
  
  func select() {
    delegate?.facultyListItemViewModel(self, didSelect: faculty)
  }
}

// MARK: - TableCellViewModel

extension FacultyListItemViewModel: TableCellViewModel {
  var tableReuseIdentifier: String {
    FacultyListItemCell.reuseIdentifier
  }
}
