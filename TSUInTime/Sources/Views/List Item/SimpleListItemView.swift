//
//  SimpleListItemView.swift
//  TSUInTime
//

import UIKit

typealias SimpleListItemCell = TableCellContainer<SimpleListItemView>

class SimpleListItemView: UIView, Configurable {
  // MARK: - Properties
  
  private let titleLabel = Label(textStyle: .body)
  private let disclosureIndicatorImageView = UIImageView()
  private let dividerView = UIView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: SimpleListItemViewModel) {
    let attributedText = NSMutableAttributedString(string: viewModel.title)
    if let highlightedText = viewModel.highlightedText {
      let range = (viewModel.title.lowercased() as NSString).range(of: highlightedText.lowercased())
      attributedText.setAttributes([.font: UIFont.bodyBold ?? .boldSystemFont(ofSize: 16)], range: range)
    }
    titleLabel.attributedText = attributedText
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupTitleLabel()
    setupDisclosureIndicatorImageView()
    setupDividerView()
  }
  
  private func setupTitleLabel() {
    addSubview(titleLabel)
    titleLabel.textColor = .baseBlack
    titleLabel.numberOfLines = 0
    titleLabel.snp.makeConstraints { make in
      make.leading.top.bottom.equalToSuperview().inset(16)
    }
  }
  
  private func setupDisclosureIndicatorImageView() {
    addSubview(disclosureIndicatorImageView)
    disclosureIndicatorImageView.contentMode = .scaleAspectFit
    disclosureIndicatorImageView.image = R.image.disclosureIndicator()?.withTintColor(.accent)
    disclosureIndicatorImageView.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.leading.equalTo(titleLabel.snp.trailing).offset(8)
      make.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }
  
  private func setupDividerView() {
    addSubview(dividerView)
    dividerView.backgroundColor = .shade1
    dividerView.snp.makeConstraints { make in
      make.height.equalTo(1)
      make.trailing.bottom.equalToSuperview()
      make.leading.equalToSuperview().inset(16)
    }
  }
}
