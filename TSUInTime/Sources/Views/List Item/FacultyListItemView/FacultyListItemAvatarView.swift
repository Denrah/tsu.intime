//
//  FacultyListItemAvatarView.swift
//  TSUInTime
//

import UIKit
import Kingfisher

class FacultyListItemAvatarView: UIView {
  // MARK: - Properties
  
  override var intrinsicContentSize: CGSize {
    return CGSize(width: 56, height: 56)
  }
  
  private let imageView = UIImageView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Configure
  
  func configure(imageURL: URL?, color: UIColor?) {
    imageView.setImage(with: imageURL, placeholder: R.image.tsuLogoPlaceholder(),
                       options: [.imageModifier(RenderingModeImageModifier(renderingMode: .alwaysTemplate))])
    
    if let color = color {
      backgroundColor = color.withAlphaComponent(0.1)
      imageView.tintColor = color
    }
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupImageView()
  }
  
  private func setupContainer() {
    layer.cornerRadius = 28
  }
  
  private func setupImageView() {
    addSubview(imageView)
    imageView.contentMode = .scaleAspectFit
    imageView.snp.makeConstraints { make in
      make.size.equalTo(32)
      make.center.equalToSuperview()
    }
  }
}
