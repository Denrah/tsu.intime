//
//  FacultyListItemView.swift
//  TSUInTime
//

import UIKit

typealias FacultyListItemCell = TableCellContainer<FacultyListItemView>

class FacultyListItemView: UIView, Configurable {
  // MARK: - Properties
  
  private let avatarView = FacultyListItemAvatarView()
  private let titleLabel = Label(textStyle: .body)
  private let disclosureIndicatorImageView = UIImageView()
  private let dividerView = UIView()
  
  // MARK: - Init
  
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  // MARK: - Configure
  
  func configure(with viewModel: FacultyListItemViewModel) {
    let attributedText = NSMutableAttributedString(string: viewModel.name)
    if let highlightedText = viewModel.highlightedText {
      let range = (viewModel.name.lowercased() as NSString).range(of: highlightedText.lowercased())
      attributedText.setAttributes([.font: UIFont.bodyBold ?? .boldSystemFont(ofSize: 16)], range: range)
    }
    titleLabel.attributedText = attributedText
    avatarView.configure(imageURL: viewModel.avatarImageURL, color: UIColor(hex: viewModel.hexColor))
  }
  
  // MARK: - Setup
  
  private func setup() {
    setupContainer()
    setupAvatarView()
    setupTilteLabel()
    setupDisclosureIndicatorImageView()
    setupDividerView()
  }
  
  private func setupContainer() {
    snp.makeConstraints { make in
      make.height.greaterThanOrEqualTo(88)
    }
  }
  
  private func setupAvatarView() {
    addSubview(avatarView)
    avatarView.snp.makeConstraints { make in
      make.size.equalTo(56)
      make.leading.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }
  
  private func setupTilteLabel() {
    addSubview(titleLabel)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .baseBlack
    titleLabel.snp.makeConstraints { make in
      make.leading.equalTo(avatarView.snp.trailing).offset(16)
      make.top.bottom.equalToSuperview().inset(16)
    }
  }
  
  private func setupDisclosureIndicatorImageView() {
    addSubview(disclosureIndicatorImageView)
    disclosureIndicatorImageView.contentMode = .scaleAspectFit
    disclosureIndicatorImageView.image = R.image.disclosureIndicator()?.withTintColor(.accent)
    disclosureIndicatorImageView.snp.makeConstraints { make in
      make.size.equalTo(24)
      make.leading.equalTo(titleLabel.snp.trailing).offset(8)
      make.trailing.equalToSuperview().inset(16)
      make.centerY.equalToSuperview()
    }
  }
  
  private func setupDividerView() {
    addSubview(dividerView)
    dividerView.backgroundColor = .shade1
    dividerView.snp.makeConstraints { make in
      make.height.equalTo(1)
      make.leading.equalTo(avatarView.snp.trailing).offset(16)
      make.trailing.bottom.equalToSuperview()
    }
  }
}
