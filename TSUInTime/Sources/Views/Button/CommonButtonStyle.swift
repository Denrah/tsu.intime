//
//  CommonButtonStyle.swift
//  TSUInTime
//

import UIKit

enum CommonButtonStyle {
  case `default`, clear, small, smallClear

  var backgroudColor: UIColor {
    switch self {
    case .default, .small:
      return .accent
    case .clear, .smallClear:
      return .clear
    }
  }

  var highlightedBackgroundColor: UIColor {
    switch self {
    case .default, .small:
      return .accentFaded
    case .clear, .smallClear:
      return .clear
    }
  }

  var disabledBackgroundColor: UIColor {
    switch self {
    case .default, .small:
      return .shade2
    case .clear, .smallClear:
      return .clear
    }
  }

  var textColor: UIColor {
    switch self {
    case .default, .small:
      return .baseWhite
    case .clear, .smallClear:
      return .accent
    }
  }

  var highlightedTextColor: UIColor {
    switch self {
    case .default, .small:
      return .baseWhite
    case .clear, .smallClear:
      return .accentFaded
    }
  }

  var disabledTextColor: UIColor {
    switch self {
    case .default, .small:
      return .baseWhite
    case .clear, .smallClear:
      return .shade2
    }
  }
  
  var height: CGFloat {
    switch self {
    case .default, .clear:
      return 48
    case .small, .smallClear:
      return 32
    }
  }
  
  var font: UIFont? {
    switch self {
    case .default, .clear:
      return .bodyBold
    case .small, .smallClear:
      return .footnoteBold
    }
  }
}
