//
//  Animations.swift
//  TSUInTime
//

import Lottie

struct Animations {
  static var clockActivity: Animation? {
    guard R.file.clockActivityJson() != nil else { return nil }
    return Animation.named("clock-activity")
  }

  static var clockActivityWhite: Animation? {
    guard R.file.clockActivityJson() != nil else { return nil }
    return Animation.named("clock-activity-white")
  }
}
